<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('artwork/Artwork_model');
        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();
    }

    /**
     * Display Admin Dashboard Page
     */
    public function index()
    {
        if ($this->ion_auth->in_group(2)) {
            $data['total_artwork_fine_art'] = $this->Artwork_model->getArtworksByCategoryForJudges($categoryID = 1, $count = true);
            $data['total_artwork_digital_art'] = $this->Artwork_model->getArtworksByCategoryForJudges($categoryID = 2, $count = true);

            $this->load->model('shortlist/Shortlist_model');
            $data['total_artwork_shortlist_fine_art'] = $this->Shortlist_model->getShortListing($categoryID = 1, $count = true);
            $data['total_artwork_shortlist_digital_art'] = $this->Shortlist_model->getShortListing($categoryID = 2, $count = true);

            $this->load->model('finalist/Finalist_model');
            $data['total_artwork_finalist_fine_art'] = $this->Finalist_model->getFinalListing($categoryID = 1, $count = true);
            $data['total_artwork_finalist_digital_art'] = $this->Finalist_model->getFinalListing($categoryID = 2, $count = true);
        }

        $data['totalNumberOfArtworks'] = $this->Artwork_model->count();

        $data['totalNumberOfFineArt'] = $this->Artwork_model->where('artwork_category_id', '1')->count();

        $data['totalNumberOfDigitalArt'] = $this->Artwork_model->where('artwork_category_id', '2')->count();

        $data['latestArtSubmissions'] = $this->Artwork_model->get(5, 0);

        $this->template->title('Admin Dashboard');

        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('dashboard', $data);

    }

}