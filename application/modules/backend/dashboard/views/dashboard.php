<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</div>

<div class="body">
    <!-- Total Number of Artwork Submission -->
    <div class="row dashboard">
        <div class="col-md-4 box col-xs-6">
            <div class="statistic panel panel-default">
                <div class="panel-body bg-light-pink">
                    <?php if($this->ion_auth->in_group(1)) { ?>
                        <a href="submissions">
                    <?php } else { ?>
                        <a href="#">
                    <?php } ?>
                        <div class="col-xs-12 col-sm-4 box-left bg-pink text-center">
                            <i class="zmdi zmdi-cloud-upload zmdi-hc-4x"></i>
                        </div>
                        <div class="col-xs-12 col-sm-8 box-right">
                            <div class="text-top counter-up"><?php echo $totalNumberOfArtworks; ?></div>
                            <div class="text-bottom">Total Artwork Submission</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <!-- Total Number Of Fine Art Submission -->
        <div class="col-md-4 box col-xs-6">
            <div class="statistic panel panel-default">
                <div class="panel-body bg-light-green">
                    <?php if($this->ion_auth->in_group(1)) { ?>
                        <a href="submissions/filter?artwork_category_id=1&submission_date_range=&participant_college_id=0&participant_name=&participant_email=&participant_nric_no=">
                    <?php } else { ?>
                        <a href="submissions/fine_art">
                    <?php } ?>
                        <div class="col-xs-12 col-sm-4 box-left bg-green text-center">
                            <i class="zmdi zmdi-book-image zmdi-hc-4x"></i>
                        </div>
                        <div class="col-xs-12 col-sm-8 box-right">
                            <div class="text-top counter-up"><?php echo $totalNumberOfFineArt; ?></div>
                            <div class="text-bottom">Fine Art Submission</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <!-- Total Number Of Digital Art Submission -->
        <div class="col-md-4 box col-xs-6">
            <div class="statistic panel panel-default">
                <div class="panel-body bg-light-green">
                    <?php if($this->ion_auth->in_group(1)) { ?>
                        <a href="submissions/filter?artwork_category_id=2&submission_date_range=&participant_college_id=0&participant_name=&participant_email=&participant_nric_no=">
                    <?php } else { ?>
                        <a href="submissions/digital_art">
                    <?php } ?>
                        <div class="col-xs-12 col-sm-4 box-left bg-green text-center">
                            <i class="zmdi zmdi-book-image zmdi-hc-4x"></i>
                        </div>
                        <div class="col-xs-12 col-sm-8 box-right">
                            <div class="text-top counter-up"><?php echo $totalNumberOfDigitalArt; ?></div>
                            <div class="text-bottom">Digital Art Submission</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 m-t-20">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div><h4>Latest Submissions</h4></div>
                    <br>
                    <div class="table-responsive clear">
                        <table class="table list no-margin m-b-0">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Category</th>
                                <th>Artwork Name</th>
                                <th>Artwork Image</th>
                                <th>Name</th>
                                <?php if($this->ion_auth->in_group(1)) { ?>
                                    <th>NRIC No.</th>
                                    <th>Contact No</th>
                                <?php } ?>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($latestArtSubmissions)) { ?>
                                <?php if (isset($latestArtSubmissions[0]) && !empty($latestArtSubmissions)) { ?>
                                    <?php foreach ($latestArtSubmissions as $key => $latestArtSubmission) { ?>
                                        <tr>
                                            <td><?php echo $latestArtSubmission['id']; ?></td>
                                            <td><?php echo $latestArtSubmission['artwork_category_name']; ?></td>
                                            <td><?php echo $latestArtSubmission['artwork_name']; ?></td>
                                            <td>
                                                <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $latestArtSubmission['id']; ?>">
                                                    <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $latestArtSubmission['artwork_full_image']; ?>"/>
                                            </td>
                                            <td><?php echo $latestArtSubmission['participant_name']; ?></td>
                                            <?php if($this->ion_auth->in_group(1)) { ?>
                                                <td><?php echo $latestArtSubmission['participant_nric_no']; ?></td>
                                                <td><?php echo $latestArtSubmission['participant_contact_no']; ?></td>
                                            <?php } ?>
                                            <td><?php echo $latestArtSubmission['created_at']; ?></td>
                                            <td>
                                                <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $latestArtSubmission['id']; ?>">
                                                    <div class="approve label label-info">View Details</div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td><?php echo $latestArtSubmissions['artworks_id']; ?></td>
                                        <td><?php echo $latestArtSubmissions['name']; ?></td>
                                        <td><?php echo $latestArtSubmissions['artwork_name']; ?></td>
                                        <td>
                                            <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $latestArtSubmissions['id']; ?>">
                                                <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $latestArtSubmissions['artwork_full_image']; ?>"/>
                                            </a>
                                        </td>
                                        <td><?php echo $latestArtSubmissions['participant_name']; ?></td>
                                        <td><?php echo $latestArtSubmissions['participant_nric_no']; ?></td>
                                        <td><?php echo $latestArtSubmissions['participant_contact_no']; ?></td>
                                        <td><?php echo $latestArtSubmissions['created_at']; ?></td>
                                        <td>
                                            <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $latestArtSubmissions['id']; ?>">
                                                <div class="label label-info">View</div>
                                            </a>
                                        </td>
                                    </tr>
                                <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>

        <!-- Modal -->
        <div class="modal fade artworkDetailModal" id="artworkDetailModal" tabindex="-1" role="dialog" aria-labelledby="artworkDetailModal" aria-hidden="true">
            <div class="modal-dialog">
                <a data-dismiss="modal" aria-label="Close" id="close-popup" style="display: inline;"></a>
                <div class="modal-content">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>

    </div>
</div>
