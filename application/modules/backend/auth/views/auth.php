<section id="login">
	<div class="container-fluid-without-padding">
		<div class="wrapper animated fadeInDown">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					<img style="200px" src="<?php echo asset('images/logo_nai.png'); ?>">
				</div>
				<div class="panel-body">
					<?php echo form_open(base_url().'admin/postLogin' ); ?>
						<?php if ($this->session->flashdata('error')) { ?>
							<div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Failed!</strong> <?php echo $this->session->flashdata('error'); ?>
							</div>
						<?php } ?>

						<!-- Username Input Text -->
						<div class="form-group m-b-20">
							<?php echo form_input(array('name' => 'email', 'id' => 'username', 'class' => 'form-control',
							    'placeholder' => 'Username' )); ?>

							<!-- Validation Error Message -->
							<?php if (form_error('email')) { ?>
								<div class="error p-t-5">
									<?php echo form_error('email'); ?>
								</div>
							<?php } ?>
						</div>

					<!-- Password Input Text -->
						<div class="form-group">
							<?php echo form_password(array('name' => 'password', 'id' => 'password', 'class' => 'form-control',
								'placeholder' => 'Password' )); ?>

							<!-- Validation Error Message -->
							<?php if (form_error('password')) { ?>
								<div class="error p-t-5">
									<?php echo form_error('password'); ?>
								</div>
							<?php } ?>
						</div>

						<!-- Remember Me Checkbox -->
						<div class="form-group">
							<div class="checkbox">
								<label class="m-t-5">
									<?php echo form_checkbox('remember'); ?> Remember Me
								</label>
							</div>
						</div>

						<!-- Login Button -->
						<div class="form-group">
							<div class="text-right">
								<?php echo form_submit('login', 'Login', "style='background: #000; color: #fff' class='btn'"); ?>
							</div>
						</div>

					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>