<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller
{
	/**
	 * Login constructor.
	 * Load ion auth library
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form'));
	}

	/**
	 * Display Admin Dashboard Login Page.
	 *
	 */
	public function getLogin()
	{
		// If user already logged in, redirect user to admin dashboard
		$this->userLoggedIn();

		$this->template->title('Nandos Art Initiative');
		$this->template->set_layout('login')->build('auth');
	}

	/**
	 * Authenticate User using ion auth library
	 *
	 * @link http://benedmunds.com/ion_auth/
	 */
    public function postLogin()
    {
		if($this->input->post())
		{
			// Set Input Rule and validate
			$this->load->library('form_validation');
/*			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');*/
			$this->form_validation->set_rules('password', 'Password', 'required');

			// If validation failed return back login page with error
			if ($this->form_validation->run() === false) {
				$this->template->title('Nandos Art Initiative');
				return $this->template->set_layout('login')->build('auth');
			}
			// If validation passed then authenticate user
			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), (bool) $this->input->post('remember')))
			{
				return redirect(base_url() . 'admin/dashboard', 'refresh');
			}

			$this->session->set_flashdata('error',$this->ion_auth->errors());
			return redirect(base_url() . 'admin/login', 'refresh');

		}
    }

	public function logout()
	{
		$this->ion_auth->logout();
		redirect(base_url() . 'admin/login', 'refresh');
	}
}