<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finalist_model extends MY_Model
{
    // Specify Table Name
    public $table = 'finalists';

    // Specify Primary Key
    public $primary_key = 'id';

    // Specify if timestamps need to update or create.
    public $timestamps = true;

    // Return query result as array
    public $return_as = 'array';

    /**
     * Shortlist model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     *
     * Get Artwork Shortlist by logged in user
     * @param $categoryID
     * @param bool|false $count
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getFinalListing($categoryID, $count = false, $limit = 10, $offset = 0)
    {
        $loggedInUserID = (int)$this->ion_auth->user()->row()->id;

        if ($count) {
            $query = $this->db->query("SELECT COUNT(*) AS `numrows` FROM `finalists` WHERE artwork_category_id =" . $categoryID . " AND user_id = " . $loggedInUserID);
            return array_shift($query->result())->numrows;
        }

        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $this->db->select('f.id as finalist_id, f.artwork_id as finalist_artwork_id, f.user_id as finalist_user_id,
            f.artwork_category_id as shortlist_artwork_category_id, a.*, ac.id as artwork_category_id, ac.name as artwork_category_name')
            ->from('finalists as f');
        $this->db->join('artworks as a', 'a.id = f.artwork_id');
        $this->db->join('artwork_categories as ac', 'ac.id = f.artwork_category_id');
        $this->db->where('a.artwork_category_id', $categoryID);
        $this->db->where('f.user_id', $loggedInUserID);

        $query = $this->db->order_by('a.id', 'desc')->limit($limit, $offset)->get();

        return $query->result_array();
    }

    public function countVotes($categoryID, $count = false, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $this->db->select('COUNT(f.artwork_id) as total_votes, f.id as finalist_id, f.artwork_id as finalist_artwork_id, f.user_id as finalist_user_id,
            f.artwork_category_id as finalist_artwork_category_id, a.*, ac.id as artwork_category_id, ac.name as artwork_category_name, c.name as college_name')
            ->from('finalists as f');
        $this->db->join('artwork_categories as ac', 'ac.id = f.artwork_category_id', 'left outer');
        $this->db->join('artworks as a', 'a.id = f.artwork_id', 'left outer');
        $this->db->join('colleges as c', 'c.id = a.participant_college_id', 'left outer');
        $this->db->group_by('f.artwork_id');

        if ($count) {
            $query = $this->db->where('f.artwork_category_id', $categoryID)->order_by('total_votes', 'desc')->get();
            return $query->num_rows();
        }

        $query = $this->db->where('f.artwork_category_id', $categoryID)->limit($limit, $offset)->order_by('total_votes', 'desc')->get();

        $results = $query->result_array();

        foreach ($results as $key => $artwork) {
            $this->db->select('u.first_name, u.last_name');
            $this->db->join('users as u', 'u.id = f.user_id' );
            $this->db->from('finalists as f');
            $this->db->where('artwork_id', $artwork['id']);
            $query = $this->db->get();

            $results[$key]['votes_by'] = $query->result_array();
        }
        return $results;
    }
}