<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <?php if ($this->uri->segment(4) == 'shortlists') { ?>
            <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
            <li><a href="<?php echo base_url() . 'admin/submissions/fine_art'; ?>">Fine Art Submissions</a></li>
            <li class="active">Shortlists</li>
        <?php } else if ($this->uri->segment(4) == 'finalists') { ?>
            <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
            <li><a href="<?php echo base_url() . 'admin/submissions/fine_art'; ?>">Fine Art Submissions</a></li>
            <li class="active">Finalists</li>
        <?php } else { ?>
            <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
            <li class="active">Fine Art Submissions</li>
        <?php } ?>
    </ol>
</div>

<div class="body">
    <div class="row submission">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-xs-12 table-heading">
                        <div class="col-xs-12 text no-padding">
                            <?php if ($this->uri->segment(4) == 'shortlists') { ?>
                                <div class="col-xs-8">
                                  <h4 class="pull-left">Shortlisted Fine Art Submissions</h4>
                                  <a href="#" class="remove-shortlist-multiple-artwork reject btn btn-info pull-left">Remove From Shortlist</a>
                                  <a href="#" class="finalist-multiple-artworks reject btn btn-primary pull-left">Add Into Finalist</a>
                                </div>

                                <div class="col-xs-4">
                                    <p class="change-limit-text"><b>Please select no.  of artworks to show per page.</b><br>*If the loading speed is slow, please select to display 5 artworks per page.</p>
                                    <a href="#" class="data_per_page btn bg-green pull-right">View</a>

                                    <select class="form-control" id="change-limit" data-url="<?php echo base_url(); ?>admin/submissions/fine_art/shortlists">
                                        <option value="5" <?php echo ($get_limit == '5' ? 'selected' : ''); ?>>5 artworks</option>
                                        <option value="10" <?php echo ($get_limit == '10' ? 'selected' : ''); ?>>10 artworks</option>
                                        <option value="25" <?php echo ($get_limit == '25' ? 'selected' : ''); ?>>25 artworks</option>
                                        <option value="50" <?php echo ($get_limit == '50' ? 'selected' : ''); ?>>50 artworks</option>
                                  </select>

                                </div>
                            <?php } else if ($this->uri->segment(4) == 'finalists') { ?>
                              <div class="col-xs-8">
                                <h4 class="pull-left">Finalists Fine Art Submissions</h4>
                                <a href="#" class="remove-finalist-multiple-artwork reject btn btn-primary pull-left">Remove From Finalist</a>
                              </div>

                                <div class="col-xs-4">
                                    <p class="change-limit-text"><b>Please select no.  of artworks to show per page.</b><br>*If the loading speed is slow, please select to display 5 artworks per page.</p>
                                    <a href="#" class="data_per_page btn bg-green pull-right">View</a>

                                    <select class="form-control" id="change-limit" data-url="<?php echo base_url(); ?>admin/submissions/fine_art/finalists">
                                        <option value="5" <?php echo ($get_limit == '5' ? 'selected' : ''); ?>>5 artworks</option>
                                        <option value="10" <?php echo ($get_limit == '10' ? 'selected' : ''); ?>>10 artworks</option>
                                        <option value="20" <?php echo ($get_limit == '20' ? 'selected' : ''); ?>>20 artworks</option>
                                  </select>
                                </div>
                            <?php } else { ?>
                              <div class="col-xs-8">
                                <h4 class="pull-left">Fine Art Submissions</h4>
                                <a href="#" class="shortlist-multiple-artworks reject btn btn-primary pull-left">Add Into Shortlist</a>
                              </div>

                                <div class="col-xs-4">
                                    <p class="change-limit-text"><b>Please select no.  of artworks to show per page.</b><br>*If the loading speed is slow, please select to display 5 artworks per page.</p>
                                    <a href="#" class="data_per_page btn bg-green pull-right">View</a>

                                    <select class="form-control" id="change-limit" data-url="<?php echo base_url(); ?>admin/submissions/fine_art">
                                        <option value="5" <?php echo ($get_limit == '5' ? 'selected' : ''); ?>>5 artworks</option>
                                        <option value="10" <?php echo ($get_limit == '10' ? 'selected' : ''); ?>>10 artworks</option>
                                        <option value="25" <?php echo ($get_limit == '25' ? 'selected' : ''); ?>>25 artworks</option>
                                        <option value="50" <?php echo ($get_limit == '50' ? 'selected' : ''); ?>>50 artworks</option>
                                  </select>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="table-responsive clear">
                        <table class="table list no-margin m-b-0">
                            <thead>
                            <tr>
                                <?php if($this->ion_auth->in_group(2)) { ?>
                                    <th><input name="select-all" type="checkbox" value="all"></th>
                                <?php } ?>
                                <th>No.</th>
                                <th>Category</th>
                                <th>Artwork Name</th>
                                <th>Artwork Image</th>
                                <th>Name</th>
                                <?php if($this->ion_auth->in_group(1)) { ?>
                                    <th>NRIC No.</th>
                                    <th>Email</th>
                                    <th>Contact No</th>
                                <?php } ?>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($artworks)) { ?>
                                <?php if (isset($artworks[0]) && !empty($artworks)) { ?>
                                    <?php foreach ($artworks as $key => $artwork) { ?>
                                        <tr>
                                            <?php if($this->ion_auth->in_group(2)) { ?>
                                                <th><input name="select" type="checkbox" data-name="<?php echo $artwork['artwork_name']; ?>" value="<?php echo $artwork['id']; ?>"></th>
                                            <?php } ?>
                                            <td><?php echo $artwork['id']; ?></td>
                                            <td><?php echo $artwork['artwork_category_name']; ?></td>
                                            <td><?php echo $artwork['artwork_name']; ?></td>
                                            <td>
                                                <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/fine_art/view/' . $artwork['id']; ?>">
                                                    <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $artwork['artwork_full_image']; ?>"/>
                                                </a>
                                            </td>
                                            <td><?php echo $artwork['participant_name']; ?></td>
                                            <?php if($this->ion_auth->in_group(1)) { ?>
                                                <td><?php echo $artwork['participant_nric_no']; ?></td>
                                                <td><?php echo $artwork['participant_email']; ?></td>
                                                <td><?php echo $artwork['participant_contact_no']; ?></td>
                                            <?php } ?>
                                            <td><?php echo $artwork['created_at']; ?></td>
                                            <td>
                                                <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/fine_art/view/' . $artwork['id']; ?>">
                                                    <div class="approve label label-info">View Details</div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <?php if($this->ion_auth->in_group(2)) { ?>
                                            <th><input name="select" type="checkbox" data-name="<?php echo $artworks['artwork_name']; ?>" value="<?php echo $artworks['id']; ?>"></th>
                                        <?php } ?>
                                        <td><?php echo $artworks['id']; ?></td>
                                        <td><?php echo $artworks['artwork_category_name']; ?></td>
                                        <td><?php echo $artworks['artwork_name']; ?></td>
                                        <td>
                                            <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/fine_art/view/' . $artworks['id']; ?>">
                                                <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $artworks['artwork_full_image']; ?>"/>
                                            </a>
                                        </td>
                                        <td><?php echo $artworks['participant_name']; ?></td>
                                        <?php if($this->ion_auth->in_group(1)) { ?>
                                            <td><?php echo $artworks['participant_nric_no']; ?></td>
                                            <td><?php echo $artworks['participant_email']; ?></td>
                                            <td><?php echo $artworks['participant_contact_no']; ?></td>
                                        <?php } ?>
                                        <td><?php echo $artworks['created_at']; ?></td>
                                        <td>
                                            <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/fine_art/view/' . $artworks['id']; ?>">
                                                <div class="label label-info">View</div>
                                            </a>
                                        </td>
                                    </tr>
                                <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- Render Pagination -->
                    <div class="pagination-container">
                        <div class="pagination pull-left">
                            <?php echo $pagination; ?>
                        </div>
                        <div class="pagination-text pull-left">
                            <?php if (isset($pagination_message)) {
                                echo $pagination_message;
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade artworkDetailModal" id="artworkDetailModal" tabindex="-1" role="dialog" aria-labelledby="artworkDetailModal" aria-hidden="true">
            <div class="modal-dialog">
                <a data-dismiss="modal" aria-label="Close" id="close-popup" style="display: inline;"></a>
                <div class="modal-content">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    </div>
</div>
