<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
        <li class="active">Users</li>
    </ol>
</div>

<div class="body">
    <div class="row user">
        <div class="col-xs-12 m-b-20">
            <a href="<?php echo base_url() . 'admin/users/create'; ?>"><div class="btn btn-primary">Add New User</div></a>
        </div>

        <?php if ($this->session->flashdata('success')) { ?>
            <div class="col-xs-12">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                </div>
            </div>
        <?php } ?>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- Showing current row from total row. Example: Showing 1 to 15 of 19 -->
                    <div class="col-xs-12">
                        <h4>List Of Users</h4>
                    </div>

                    <div class="table-responsive clear">
                        <table class="table list no-margin m-b-0">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Full Name</th>
                                <th>Username</th>
                                <th>Roles</th>
                                <th>Last Login</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($users[0])) {
                                    foreach ($users as $key => $user) { ?>
                                        <tr>
                                            <td><?php echo $user['id']; ?></td>
                                            <td><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></td>
                                            <td><?php echo $user['username']; ?></td>
                                            <td>
                                                <?php foreach ($user['groups'] as $key1 => $group) { ?>
                                                    <div class="label label-info"><?php echo ucfirst($group['name']) ?></div>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $user['last_login']; ?></td>
                                            <td><?php echo $user['created_on']; ?></td>
                                            <td>
                                                <a class="update-user" href="<?php echo base_url() . 'admin/users/edit/' . $user['id']; ?>">
                                                    <div class="label label-primary">Update</div>
                                                </a>
                                                <a class="delete-user " data-id="<?php echo $user['id']; ?>"
                                                   data-name="<?php echo $user['username']; ?>">
                                                    <div class="label label-danger">Delete</div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td><?php echo $users['id']; ?></td>
                                        <td><?php echo $users['first_name'] . ' ' . $users['last_name']; ?></td>
                                        <td><?php echo $users['username']; ?></td>
                                        <td><?php echo $users['email']; ?></td>
                                        <td>
                                            <?php foreach ($users['groups'] as $key1 => $group) { ?>
                                                <div class="label label-info"><?php echo ucfirst($group['name']) ?></div>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $users['last_login']; ?></td>
                                        <td><?php echo $users['created_on']; ?></td>
                                        <td>
                                            <a class="update-user" href="<?php echo base_url() . 'admin/users/edit/' . $users['id']; ?>">
                                                <div class="label label-primary">Update</div>
                                            </a>
                                            <a class="delete-user " data-id="<?php echo $users['id']; ?>"
                                               data-name="<?php echo $users['username']; ?>">
                                                <div class="label label-danger">Delete</div>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>