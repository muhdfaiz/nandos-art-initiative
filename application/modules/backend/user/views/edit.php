<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
        <li><a href="<?php echo base_url() . 'admin/users'; ?>">Users</a></li>
        <li class="active">Update User</li>
    </ol>
</div>

<div class="body">
    <div class="row user">

        <?php if ($this->session->flashdata('error')) { ?>
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('error'); ?>
                </div>
            </div>
        <?php } ?>

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading m-b-15"><h3 class="panel-title">Update User</h3></div>
                <div class="panel-body">
                    <form method="POST" action="<?php echo base_url() . 'admin/users/update/' . $user['id']; ?>" accept-charset="UTF-8" class="form-horizontal" role="form">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" name="id" value="<?php echo $user['id']; ?>">

                        <!-- First Name Input -->
                        <div class="form-group">
                            <label for="first_name" class="col-md-2 control-label">First Name</label>
                            <div class="col-md-10">
                                <input class="form-control" placeholder="First Name" name="first_name" type="text"
                                       value="<?php echo $user['first_name']; ?>" id="first_name">
                                <div class="error p-t-5">
                                    <?php echo form_error('first_name'); ?>
                                </div>
                            </div>
                        </div>

                        <!-- Last Name Input -->
                        <div class="form-group">
                            <label for="last_name" class="col-md-2 control-label">Last Name</label>
                            <div class="col-md-10">
                                <input class="form-control" placeholder="Last Name" name="last_name" type="text"
                                       value="<?php echo $user['last_name']; ?>" id="last_name">
                                <div class="error p-t-5">
                                    <?php echo form_error('last_name'); ?>
                                </div>
                            </div>

                        </div>

                        <!-- Username Input -->
                        <div class="form-group">
                            <label for="username" class="col-md-2 control-label">Username</label>
                            <div class="col-md-10">
                                <input class="form-control" placeholder="Username" name="username" type="text"
                                       value="<?php echo $user['username']; ?>" id="username">
                                <div class="error p-t-5">
                                    <?php echo form_error('username'); ?>
                                </div>
                            </div>
                        </div>

                        <!-- Roles -->
                        <div class="form-group">
                            <label for="roles" class="col-md-2 control-label">Group</label>
                            <div class="col-md-10">
                                <?php $roles = array('' => '-- Please Select --', '1' => 'Admin', '2' => 'Judges') ; ?>

                                <?php if (isset($user['groups'][0]['group_id']) && !empty($user['groups'][0]['group_id'])) {
                                    echo form_dropdown('roles', $roles, $user['groups'][0]['group_id'], 'id="roles" class="form-control"');
                                } else {
                                    echo form_dropdown('roles', $roles, set_value('roles'), 'id="roles" class="form-control"');
                                } ?>

                                <div class="error p-t-5">
                                    <?php echo form_error('roles'); ?>
                                </div>
                            </div>
                        </div>

                        <!-- Password Input -->
                        <div class="form-group">
                            <label for="password" class="col-md-2 control-label">New Password</label>
                            <div class="col-md-10">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="" id="password">
                                <div class="error p-t-5">
                                    <?php echo form_error('password'); ?>
                                </div>
                            </div>
                        </div>

                        <!-- Confirm Password Input -->
                        <div class="form-group">
                            <label for="confirm_password" class="col-md-2 control-label">Confirm New Password</label>
                            <div class="col-md-10">
                                <input class="form-control" placeholder="Confirm Password" name="confirm_password" type="password" value="" id="confirm_password">
                                <div class="error p-t-5">
                                    <?php echo form_error('confirm_password'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group pull-right">
                            <div class="col-xs-12">
                                <input class="btn btn-primary" type="submit" value="Update">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>