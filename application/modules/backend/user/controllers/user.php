<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller
{
    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();
        $this->load->model('user_model');
        $this->load->library('form_validation');

    }

    /**
     * Display Admin Dashboard Page
     */
    public function index()
    {
        $users = $this->user_model->with('groups')->get_all();

        $this->template->title('Admin Dashboard');

        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('index', array('users' => $users));
    }

    /**
     * Display Form to crate users
     */
    public function create()
    {
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('create');
    }

    /**
     * Store New User in database
     */
    public function store()
    {
        if (!$this->validateCreateUserInput()) {
            $this->form_validation->set_message('roles', 'The Roles field is required.');
            $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
                ->set_layout('admin')->build('create');
            return;
        }

        // Retrieve all post data
        $postData = $this->input->post();

        // Store user data
        $userID = $this->ion_auth->register($postData['username'], $postData['password'], '', $postData, array($postData['roles']));

        if ($userID) {
            $this->session->set_flashdata('success', 'Successfully Add New User');
            return redirect(base_url() . 'admin/users', 'refresh');
        }
    }

    /**
     * Display Form to Update User
     *
     * @param $userID
     */
    public function edit($userID)
    {
        $data['user'] = $this->user_model->with('groups')->get($userID);

        if (!$data['user']) {
            return redirect(base_url() . 'admin/dashboard', 'refresh');
        }

        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('edit', $data);
    }

    /**
     * Update user Info
     *
     * @param $userID
     */
    public function update($userID)
    {
        if (!$this->validateUpdateUserInput()) {
            $data['user'] = $this->user_model->with('groups')->get($userID);
            $this->form_validation->set_message('roles', 'The Roles field is required.');
            $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
                ->set_layout('admin')->build('edit', $data);
            return;
        }
        // Get user group
        $userGroups = $this->ion_auth->get_users_groups($userID)->result();

        // Retrieve all post data
        $postData = $this->input->post();

        foreach($userGroups as $key => $userGroup) {
            if ($userGroup->id != $postData['roles']) {
                // Remove user from all groups
                $this->ion_auth->remove_from_group(NULL, $userID);
                // Add user to group
                $this->ion_auth->add_to_group($postData['roles'], $userID);
            }
        }

        $updateStatus = $this->ion_auth->update($userID, $postData);

        if ($updateStatus) {
            $this->session->set_flashdata('success', 'Successfully Updated User');
            return redirect(base_url() . 'admin/users', 'refresh');
        }
    }


    /**
     * Delete User by User ID
     *
     * @param $userID
     * @return bool
     */
    public function destroy($userID)
    {
        $delete = $this->user_model->delete($userID);

        echo $delete;
    }

    /**
     * Validate input data during create new user
     *
     * @return boolean
     */
    private function validateCreateUserInput()
    {
        $this->form_validation->set_message('numeric', 'The Roles field is required.');
        // Form Validation
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|alphanumeric|is_unique[users.username]');
        $this->form_validation->set_rules('roles', 'Roles', 'required|numeric');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[11]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

        return $this->form_validation->run();
    }

    /**
     * Validate input data during update user
     *
     * @return boolean
     */
    private function validateUpdateUserInput()
    {
        $this->form_validation->set_message('numeric', 'The Roles field is required.');
        // Form Validation
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|alphanumeric');
        $this->form_validation->set_rules('roles', 'Roles', 'required|numeric');
        $this->form_validation->set_rules('password', 'Password', 'min_length[6]|max_length[11]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'matches[password]');

        return $this->form_validation->run();
    }

}