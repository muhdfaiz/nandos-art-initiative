<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_model extends MY_Model
{
    public $table = 'groups'; // you MUST mention the table name

    public $primary_key = 'id'; // you MUST mention the primary key
    /**
     * User_model constructor.
     */
    public function __construct()
    {
        $this->has_many_pivot['users'] = array('User_model','id','id'); //where the second parameter is the foreign
        // primary key of posts table, and the third parameter is the local primary key.
        //$this->has_many['users'] = array('foreign_model'=>'User_model','foreign_table'=>'users','foreign_key'=>'user_id','local_key'=>'id');
    }

    /**
     * Get All Users with Group
     */
    public function getUsersWithGroup()
    {
        $users = $this->db->select('id, first_name, last_name, username, email, last_login, created_on')->from('users')
            ->join('users');
        d($users->result());
    }
}