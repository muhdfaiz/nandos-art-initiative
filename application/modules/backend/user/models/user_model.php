<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{
    // Specify Table Name
    public $table = 'users'; // you MUST mention the table name

    // Specify Primary Key
    public $primary_key = 'id'; // you MUST mention the primary key

    // Specify if timestamps need to update or create.
    public $timestamps = FALSE;

    // Return query result as array
    public $return_as = 'array';

    /**
     * User_model constructor.
     */
    public function __construct()
    {
        // Set Has Many Relationship -> User has many groups
        $this->has_many_pivot['groups'] = array('Group_model','id','id');
/*        $this->has_many['shortlist'] = array('shortlist/Shortlist_model', 'id', 'judge_id');*/

        parent::__construct();
    }
}