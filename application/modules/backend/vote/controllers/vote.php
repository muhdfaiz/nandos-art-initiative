<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vote extends MY_Controller
{
    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->model('finalist/Finalist_model');
    }

    public function getFineArtVotes($offset = 0)
    {
        $data['artworks'] = $this->Finalist_model->countVotes($categoryID = 1, $count = false, $limit = 20, $offset);

        $data['total_artwork'] = $this->Finalist_model->countVotes($categoryID = 1, $count = true);

        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/fine_art/votes", $data['total_artwork'],
            $limit = 20, $urlSegement = 5);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('fine_art', $data);
    }


    public function getDigitalArtVotes($offset = 0)
    {
        $data['artworks'] = $this->Finalist_model->countVotes($categoryID = 2, $count = false, $limit = 20, $offset);
        $data['total_artwork'] = $this->Finalist_model->countVotes($categoryID = 2, $count = true);

        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/digital_art/votes", $data['total_artwork'],
            $limit = 20, $urlSegement = 5);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('digital_art', $data);
    }

    public function export()
    {
        $getData = $this->input->get();

        if ($getData['artwork_category_id'] == 1) {
            $categoryName = 'Fine_Art';
        } else {
            $categoryName = 'Digital_Art';
        }

        header("Content-Disposition: attachment; filename=" . $categoryName . "_Votes_Result-" . date("d-m-Y") . ".csv");
        header("Content-Type: application/vnd.ms-excel");

        $totalArtwork = $this->Finalist_model->countVotes($categoryID = 1, $count = true);

        // Open output stream
        $handle = fopen('php://output', 'w');

        // Add CSV headers
        fputcsv($handle, array(
            'Artwork ID',
            'Total Votes',
            'Votes By',
            'Artwork Name',
            'Artwork Creative Rational',
            'Artwork Full Image URL',
            'Artwork Highlight Image URL',
            'Participant Name',
            'Participant Gender',
            'Participant NRIC No',
            "Participant Email",
            "Participant Contact No",
            "Participant is Student",
            "Participant College Name",
            "Participant Home Address",
            "Submission Date",
        ));

        if(intval($totalArtwork < 500)) {
            $max = 1;
        } else {
            $max = intval($totalArtwork / 500);
        }

        for ($i = 0; $i < $max; $i++) {
            $artworks = $this->Finalist_model->countVotes($getData['artwork_category_id'], $count = false, $limit = 500, $offset = $i * 500);

            foreach ($artworks as $key => $artwork) {
                $participantGender = ($artwork['participant_gender_id'] == '1') ? 'Male' : 'Female';

                if (!empty($artwork['artwork_full_image'])) {
                    $artworkFullImage = base_url() . 'uploads/' . $artwork['artwork_full_image'];
                } else {
                    $artworkFullImage = 'N/A';
                }

                if (!empty($artwork['artwork_highlight_image'])) {
                    $artworkHighlightImage = base_url() . 'uploads/' . $artwork['artwork_highlight_image'];
                } else {
                    $artworkHighlightImage = 'N/A';
                }

                if ($artwork['participant_is_student']) {
                    $participantIsStudent = 'true';
                    if ($artwork['college_name'] == 'Other') {
                        $collegeName = $artwork['participant_college_name'];
                    } else {
                        $collegeName = $artwork['college_name'];
                    }
                } else {
                    $collegeName = 'N/A';
                    $participantIsStudent = 'false';
                }

                if (!empty($artwork['votes_by'])) {
                    $votesBy = implode(', ', array_map(function ($judeName) {
                        return $judeName['first_name'] . ' ' . $judeName['last_name'];
                        }, $artwork['votes_by']));
                } else {
                    $votesBy = 'N/A';
                }

                fputcsv($handle, array(
                    $artwork['id'],
                    count($artwork['votes_by']),
                    $votesBy,
                    $artwork['artwork_name'],
                    $artwork['artwork_creative_rationale'],
                    $artworkFullImage,
                    $artworkHighlightImage,
                    $artwork['participant_name'],
                    $participantGender,
                    $artwork['participant_nric_no'],
                    $artwork['participant_email'],
                    (string) $artwork['participant_contact_no'],
                    $participantIsStudent,
                    $collegeName,
                    $artwork['participant_home_address'],
                    $artwork['created_at']
                ));
            }
        };

        fclose($handle);
    }

    /**
     * Set Bootstrap Pagination
     *
     * @param $baseURL
     * @param $totalArtwork
     * @param $perPage
     * @param $uriSegment
     * @return mixed
     */
    private function setPagination($baseURL, $totalArtwork, $perPage, $uriSegment)
    {
        $this->load->config('pagination');
        $this->config->config['pagination']['total_rows'] = $totalArtwork;
        $choice = $totalArtwork / $perPage;
        $this->config->config['pagination']["num_links"] = floor($choice);
        $this->config->config['pagination']["per_page"] = $perPage;
        $this->config->config['pagination']['base_url'] = $baseURL;
        $this->config->config['pagination']['use_page_numbers'] = true;
        $this->config->config['pagination']['uri_segment'] = $uriSegment;
        //$this->config->config['pagination']['first_url'] = $this->config->config['pagination']['base_url'].'?'.http_build_query($_GET);

        if (count($_GET) > 0) $this->config->config['pagination']['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($this->config->config['pagination']);

        return $this->pagination->create_links();
    }
}