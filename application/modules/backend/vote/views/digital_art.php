<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() . 'admin/dashboard'; ?>">Home</a></li>
        <li class="active">Digital Art Votes Result</li>

    </ol>
</div>

<div class="body">
    <div class="row submission">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-xs-12 table-heading">
                        <div class="col-xs-12 col-sm-7 text no-padding">
                            <h4 class="pull-left">Digital Art Votes Result</h4>
                            <?php if($this->ion_auth->in_group(1)) { ?>
                                <a id="export-votes-result" data-category-id="2" href="#" class="reject btn btn-primary pull-left">Export to CSV</a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="table-responsive clear">
                        <table class="table list no-margin m-b-0">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Total Votes</th>
                                <th>Votes By</th>
                                <th>Artwork Name</th>
                                <th>Artwork Image</th>
                                <th>Name</th>
                                <th>NRIC No.</th>
                                <th>Email</th>
                                <th>Contact No</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($artworks)) { ?>
                                <?php if (isset($artworks[0]) && !empty($artworks)) { ?>
                                    <?php foreach ($artworks as $key => $artwork) { ?>
                                        <tr>
                                            <td><?php echo $artwork['id']; ?></td>
                                            <td style="text-align: center"><span style="float: none" class="badge"><?php echo $artwork['total_votes']; ?></span></td>
                                            <td>
                                                <ul class="p-l-15">
                                                    <?php foreach($artwork['votes_by'] as $key => $votedUser) { ?>
                                                        <li><?php echo $votedUser['first_name'] . ' ' . $votedUser['last_name']; ?></li>
                                                    <?php } ?>
                                                </ul>
                                            </td>
                                            <td><?php echo $artwork['artwork_name']; ?></td>
                                            <td>
                                                <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $artwork['id']; ?>">
                                                    <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $artwork['artwork_full_image']; ?>"/>
                                                </a>
                                            </td>
                                            <td><?php echo $artwork['participant_name']; ?></td>
                                            <td><?php echo $artwork['participant_nric_no']; ?></td>
                                            <td><?php echo $artwork['participant_email']; ?></td>
                                            <td><?php echo $artwork['participant_contact_no']; ?></td>
                                            <td>
                                                <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/fine_art/view/' . $artwork['id']; ?>">
                                                    <div class="approve label label-info">View Details</div>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td><?php echo $artworks['id']; ?></td>
                                        <td style="text-align: center"><span style="float: none" class="badge"><?php echo $artworks['total_votes']; ?></span></td>
                                        <td>
                                            <ul class="p-l-15">
                                                <?php foreach($artworks['votes_by'] as $key => $votedUser) { ?>
                                                    <li><?php echo $votedUser['first_name'] . ' ' . $votedUser['last_name']; ?></li>
                                                <?php } ?>
                                            </ul>
                                        </td>
                                        <td><?php echo $artworks['artwork_name']; ?></td>
                                        <td>
                                            <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $artworks['id']; ?>">
                                                <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $artworks['artwork_full_image']; ?>"/>
                                            </a>
                                        </td>
                                        <td><?php echo $artworks['participant_name']; ?></td>
                                        <td><?php echo $artworks['participant_nric_no']; ?></td>
                                        <!--<td><?php /*echo $artworks['participant_email']; */?></td>-->
                                        <td><?php echo $artworks['participant_contact_no']; ?></td>
                                        <td>
                                            <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/fine_art/view/' . $artworks['id']; ?>">
                                                <div class="label label-info">View Details</div>
                                            </a>
                                        </td>
                                    </tr>
                                <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- Render Pagination -->
                    <div class="pagination-container">
                        <div class="pagination pull-left">
                            <?php echo $pagination; ?>
                        </div>
                        <div class="pagination-text pull-left">
                            <?php if (isset($pagination_message)) {
                                echo $pagination_message;
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade artworkDetailModal" id="artworkDetailModal" tabindex="-1" role="dialog" aria-labelledby="artworkDetailModal" aria-hidden="true">
            <div class="modal-dialog">
                <a data-dismiss="modal" aria-label="Close" id="close-popup" style="display: inline;"></a>
                <div class="modal-content">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    </div>
</div>