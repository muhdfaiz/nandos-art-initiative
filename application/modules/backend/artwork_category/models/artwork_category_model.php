<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artwork_category_model extends MY_Model
{
    // Specify Table Name
    public $table = 'artwork_categories';

    // Specify Primary Key
    public $primary_key = 'id';

    // Specify if timestamps need to update or create.
    public $timestamps = FALSE;

    // Return query result as array
    public $return_as = 'array';

    /**
     * Artwork Category model constructor.
     */
    public function __construct()
    {
        $this->has_many['artworks'] = array('foreign_model'=>'Artwork_model','foreign_table'=>'artworks','foreign_key'=>'artwork_category_id','local_key'=>'id');
        parent::__construct();
    }

}