<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Digital_art extends MY_Controller
{
    /**
     * @var intSpecify Category ID
     */
    private $categoryID = 2;

    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();
        $this->load->model('artwork/Artwork_model');
        $this->load->model('college/college_model');
        $this->load->model('artwork_category/artwork_category_model');
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('ion_auth');
    }

    /**
     * Display List of All Artworks for Digital Artwork and
     * exclude artworks that have been shortlist by logged in user.
     *
     * @param int $offset
     */
    public function index($offset = 0)
    {
        //get artwork limit by get
        (isset($_GET['limit']) ? $get_limit = $_GET['limit'] : $get_limit = 10);

        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();

        $data['total_artwork'] = $this->Artwork_model->getArtworksByCategoryForJudges($this->categoryID, $count = true);
        $data['total_artwork_fine_art'] = $this->Artwork_model->getArtworksByCategoryForJudges($categoryID = 1, $count = true);
        $data['total_artwork_digital_art'] = $data['total_artwork'];

        $this->load->model('shortlist/Shortlist_model');

        $data['total_artwork_shortlist_fine_art'] = $this->Shortlist_model->getShortListing($categoryID = 1, $count = true);
        $data['total_artwork_shortlist_digital_art'] = $this->Shortlist_model->getShortListing($categoryID = 2, $count = true);

        $this->load->model('finalist/Finalist_model');
        $data['total_artwork_finalist_fine_art'] = $this->Finalist_model->getFinalListing($categoryID = 1, $count = true);
        $data['total_artwork_finalist_digital_art'] = $this->Finalist_model->getFinalListing($categoryID = 2, $count = true);

        $data['artworks'] = $this->Artwork_model->getArtworksByCategoryForJudges($this->categoryID, $count = false, $limit = $get_limit, $offset);

        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/digital_art", $data['total_artwork'],
            $limit = $get_limit, $urlSegement = 4);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $data['get_limit'] = $get_limit;
        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('digital_art/index', $data);
    }

    /**
     * View Details of Artwork
     *
     * @param $artworkID
     */
    public function view($artworkID)
    {
        $data['artwork'] = $this->Artwork_model->getByID($artworkID);

        $this->load->model('shortlist/Shortlist_model');
        $data['shortlisted'] = $this->Shortlist_model->where('user_id', $this->ion_auth->user()->row()->id)
            ->where('artwork_id', $artworkID)->get();

        $this->load->model('finalist/Finalist_model');
        $data['finalist'] = $this->Finalist_model->where('user_id', $this->ion_auth->user()->row()->id)
            ->where('artwork_id', $artworkID)->get();

        $this->load->view('artwork/view', $data);
    }

    /**
     * Get All Artworks shortlist by Judge
     *
     * @param int $offset
     */
    public function getShortListing($offset = 0)
    {
        //get artwork limit by get
        (isset($_GET['limit']) ? $get_limit = $_GET['limit'] : $get_limit = 10);

        $this->load->model('shortlist/Shortlist_model');

        $data['total_artwork'] = $this->Shortlist_model->getShortListing($this->categoryID, $count = true);

        $data['total_artwork_fine_art'] = $this->Artwork_model->getArtworksByCategoryForJudges($categoryID = 1, $count = true);
        $data['total_artwork_digital_art'] = $this->Artwork_model->getArtworksByCategoryForJudges($this->categoryID, $count = true);

        $data['total_artwork_shortlist_fine_art'] = $this->Shortlist_model->getShortListing($categoryID = 1, $count = true);
        $data['total_artwork_shortlist_digital_art'] = $data['total_artwork'];

        $this->load->model('finalist/Finalist_model');
        $data['total_artwork_finalist_fine_art'] = $this->Finalist_model->getFinalListing($categoryID = 1, $count = true);
        $data['total_artwork_finalist_digital_art'] = $this->Finalist_model->getFinalListing($this->categoryID, $count = true);

        $data['artworks'] = $this->Shortlist_model->getShortListing($this->categoryID, $count = false, $limit = $get_limit, $offset);

        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/digital_art/shortlists", $data['total_artwork'],
            $limit = $get_limit, $urlSegement = 5);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $data['get_limit'] = $get_limit;
        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('digital_art/index', $data);
    }

    /**
     * Shortlisting Digital Artwork
     */
    public function storeShortlisting()
    {
        $artworksID = $this->input->post('artworks_id');

        $this->load->model('shortlist/Shortlist_model');

        $loggedInUserID = $this->ion_auth->user()->row()->id;

        if ($artworksID) {
            foreach ($artworksID as $key => $artworkID) {
                $previousShortlist = $this->Shortlist_model->where('artwork_id', $artworkID)
                    ->where('user_id', $loggedInUserID)->get();

                if (!$previousShortlist) {
                    $result = $this->Shortlist_model->insert(array(
                        'artwork_id' => $artworkID,
                        'artwork_category_id' => '2',
                        'user_id' => $this->ion_auth->user()->row()->id
                    ));

                    if (!$result) {
                        header('Content-Type: application/json');
                        echo false;
                        return;
                    }
                }
            }
            header('Content-Type: application/json');
            echo true;
            return;
        }

    }

    /**
     * Get All Artworks finalist by Judge
     *
     * @param int $offset
     */
    public function getFinalListing($offset = 0)
    {
        //get artwork limit by get
        (isset($_GET['limit']) ? $get_limit = $_GET['limit'] : $get_limit = 10);

        $this->load->model('finalist/Finalist_model');

        $data['total_artwork'] = $this->Finalist_model->getFinalListing($this->categoryID, $count = true);

        $data['total_artwork_fine_art'] =  $this->Artwork_model->getArtworksByCategoryForJudges($categoryID = 1, $count = true);
        $data['total_artwork_digital_art'] = $this->Artwork_model->getArtworksByCategoryForJudges($categoryID = 2, $count = true);

        $this->load->model('shortlist/Shortlist_model');

        $data['total_artwork_shortlist_fine_art'] = $this->Shortlist_model->getShortListing($categoryID = 1, $count = true);
        $data['total_artwork_shortlist_digital_art'] = $this->Shortlist_model->getShortListing($categoryID = 2, $count = true);

        $this->load->model('finalist/Finalist_model');
        $data['total_artwork_finalist_fine_art'] = $this->Finalist_model->getFinalListing($categoryID = 1, $count = true);
        $data['total_artwork_finalist_digital_art'] = $data['total_artwork'];

        $data['artworks'] = $this->Finalist_model->getFinalListing($this->categoryID, $count = false, $limit = $get_limit, $offset);

        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/digital_art/finalists", $data['total_artwork'],
            $limit = $get_limit, $urlSegement = 5);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $data['get_limit'] = $get_limit;
        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('digital_art/index', $data);
    }

    /**
     * Delete Shortlisting
     */
    public function deleteShortListing()
    {
        $artworksID = $this->input->post('artworks_id');

        $userID =  $this->ion_auth->user()->row()->id;
        if ($artworksID) {
            $this->db->where_in('artwork_id', $artworksID)->where('user_id', $userID);
            $deleted = $this->db->delete('shortlists');

            header('Content-Type: application/json');

            if ($deleted) {
                echo true;
                return;
            }
            echo false;
            return;
        }

        header('Content-Type: application/json');
        echo false;
        return;
    }

    /**
     * Shortlisting Fine Artwork
     */
    public function storeFinalListing()
    {
        $artworksID = $this->input->post('artworks_id');

        $this->load->model('finalist/Finalist_model');

        $loggedInUserID = $this->ion_auth->user()->row()->id;

        $countPreviousFinalist = $this->Finalist_model->where('artwork_category_id', $this->categoryID)
            ->where('user_id', $loggedInUserID)->count();

        if ($countPreviousFinalist >= 20 || $countPreviousFinalist + count($artworksID) > 20) {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('error' => 'You can only add a maximum number of 20 artworks into finalist.')));
            return;
        }

        if ($artworksID) {
            foreach ($artworksID as $key => $artworkID) {
                $previousShortlist = $this->Finalist_model->where('artwork_id', $artworkID)
                    ->where('user_id', $loggedInUserID)->get();

                if (!$previousShortlist) {
                    $result = $this->Finalist_model->insert(array(
                        'artwork_id' => $artworkID,
                        'artwork_category_id' => $this->categoryID,
                        'user_id' => $this->ion_auth->user()->row()->id
                    ));

                    if (!$result) {
                        header('Content-Type: application/json');
                        echo false;
                        return;
                    }
                }
            }
            header('Content-Type: application/json');
            echo true;
            return;
        }
    }

    /**
     * Delete Shortlisting
     */
    public function deleteFinalListing()
    {
        $artworksID = $this->input->post('artworks_id');

        $userID =  $this->ion_auth->user()->row()->id;

        if ($artworksID) {
            $this->db->where_in('artwork_id', $artworksID)->where('user_id', $userID);
            $deleted = $this->db->delete('finalists');

            header('Content-Type: application/json');

            if ($deleted) {
                echo true;
                return;
            }
            echo false;
            return;
        }

        header('Content-Type: application/json');
        echo false;
        return;
    }


    /**
     * Set Bootstrap Pagination
     *
     * @param $baseURL
     * @param $totalArtwork
     * @param $perPage
     * @param $uriSegment
     * @return mixed
     */
    private function setPagination($baseURL, $totalArtwork, $perPage, $uriSegment)
    {
        $this->load->config('pagination');
        $this->config->config['pagination']['total_rows'] = $totalArtwork;
        $choice = $totalArtwork / $perPage;
        $this->config->config['pagination']["num_links"] = 9;
        $this->config->config['pagination']["per_page"] = $perPage;
        $this->config->config['pagination']['base_url'] = $baseURL;
        $this->config->config['pagination']['use_page_numbers'] = true;
        $this->config->config['pagination']['uri_segment'] = $uriSegment;
        //$this->config->config['pagination']['first_url'] = $this->config->config['pagination']['base_url'].'?'.http_build_query($_GET);

        if (count($_GET) > 0) $this->config->config['pagination']['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($this->config->config['pagination']);

        return $this->pagination->create_links();
    }


}
