<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo 'admin/dashboard'; ?>">Home</a></li>
        <li class="active">Submissions</li>
    </ol>
</div>

<div class="body">
    <div class="row submission">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if($this->ion_auth->in_group(1)) { ?>
                        <div class="filter">
                            <form method="GET" action="<?php echo base_url() . 'admin/submissions/filter'; ?>" accept-charset="UTF-8">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <!-- Submission Type Filter Select Box -->
                                            <div class="form-group">
                                                <label for="artwork_category_id" class="control-label">Artwork Category</label>
                                                <?php echo form_dropdown('artwork_category_id', array('0' => '-- Please Select --') + $artwork_categories, $this->input->get('artwork_category_id'), 'id="artwork_category_id" class="form-control"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-6">
                                            <!-- Submission Date (Start) Filter Input -->
                                            <div class="form-group">
                                                <label for="submission_date_range" class="control-label">Submission Date (Range)</label>

                                                <div class="input-group date" id="submissionDateTimeRange">
                                                    <input placeholder="Submission Date Range" id="submission_date_range" value="<?php echo $this->input->get('submission_date_range'); ?>" class="form-control" name="submission_date_range" type="text">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-md-4">
                                            <!-- State Filter Input -->
                                            <div class="form-group">
                                                <label for="state" class="control-label">College</label>
                                                <?php echo form_dropdown('participant_college_id', array('0' => '-- Please Select --') + $colleges, $this->input->get('participant_college_id'), 'id="participant_college_id" class="form-control"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-md-4">
                                            <!-- Name Filter Input -->
                                            <div class="form-group">
                                                <label for="participant_name" class="control-label">Name</label>
                                                <input class="form-control ui-autocomplete-input" placeholder="Name" name="participant_name" value="<?php echo $this->input->get('participant_name'); ?>" type="text" id="participant_name">
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-md-4">
                                            <!-- Email Filter Input -->
                                            <div class="form-group">
                                                <label for="participant_email" class="control-label">Email</label>
                                                <input placeholder="Email Address" class="form-control ui-autocomplete-input" value="<?php echo $this->input->get('participant_email'); ?>"
                                                       name="participant_email" type="text" id="participant_email" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-md-4">
                                            <!-- Status Filter Input -->
                                            <div class="form-group">
                                                <label for="participant_nric_no" class="control-label">NRIC No</label>
                                                <input placeholder="NRIC No" class="form-control ui-autocomplete-input" value="<?php echo $this->input->get('participant_nric_no'); ?>"
                                                       name="participant_nric_no" type="text" id="participant_nric_no" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <button type="submit" id="product-filter-button" class="btn btn-primary pull-right"><i class="zmdi zmdi-search-for"></i>  Filter Submissions</button>
                                            <a href="#" class="p-r-10 clear-filter btn btn-primary pull-right"><i class="zmdi zmdi-refresh"></i>  Clear</a>
                                            <a id="submissions-export" class="p-r-10 export-button btn btn-primary pull-right"><i class="zmdi zmdi-file"></i>  Export To CSV</a>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php } ?>

                    <div class="col-xs-12 table-heading">
                        <div class="col-xs-8 text no-padding">
                            <h4 style="margin-top: 18px;" class="pull-left">List Of All Submissions</h4>
                            <?php if($this->ion_auth->in_group(2)) { ?>
                                <a id="shortlist-submission" href="#" class="reject btn btn-primary pull-left">Shortlist</a>
                            <?php } ?>
                        </div>
                        <div class="col-xs-4">
                            <p class="change-limit-text"><b>Please select no.  of artworks to show per page.</b><br>*If the loading speed is slow, please select to display 5 artworks per page.</p>
                            <a href="#" class="data_per_page btn bg-green pull-right">View</a>
                            
                            <select class="form-control" id="change-limit" data-url="<?php echo base_url(); ?>admin/submissions">
                                <option value="5" <?php echo ($get_limit == '5' ? 'selected' : ''); ?>>5 artworks</option>
                                <option value="10" <?php echo ($get_limit == '10' ? 'selected' : ''); ?>>10 artworks</option>
                                <option value="25" <?php echo ($get_limit == '25' ? 'selected' : ''); ?>>25 artworks</option>
                                <option value="50" <?php echo ($get_limit == '50' ? 'selected' : ''); ?>>50 artworks</option>
                          </select>
                        </div>
                    </div>

                    <div class="table-responsive clear">
                        <table class="table list no-margin m-b-0">
                            <thead>
                            <tr>
                                <?php if($this->ion_auth->in_group(2)) { ?>
                                    <th><input name="select-all" type="checkbox" value="all"></th>
                                <?php } ?>
                                <th>No.</th>
                                <th>Category</th>
                                <th>Artwork Name</th>
                                <th>Artwork Image</th>
                                <th>Name</th>
                                <th>Email</th>
                                <?php if($this->ion_auth->in_group(1)) { ?>
                                    <th>NRIC No.</th>
                                    <th>Contact No</th>
                                <?php } ?>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($artworks)) { ?>
                                    <?php if (isset($artworks[0]) && !empty($artworks)) { ?>
                                        <?php foreach ($artworks as $key => $artwork) { ?>
                                            <tr>
                                                <?php if($this->ion_auth->in_group(2)) { ?>
                                                    <th><input name="select" type="checkbox" value="<?php echo $artwork['id']; ?>"></th>
                                                <?php } ?>
                                                <td><?php echo $artwork['id']; ?></td>
                                                <td><?php echo $artwork['artwork_category_name']; ?></td>
                                                <td><?php echo $artwork['artwork_name']; ?></td>
                                                <td>
                                                    <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $artwork['id']; ?>">
                                                        <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $artwork['artwork_full_image']; ?>"/>
                                                    </a>
                                                </td>
                                                <td><?php echo $artwork['participant_name']; ?></td>
                                                <td><?php echo $artwork['participant_email']; ?></td>
                                                <?php if($this->ion_auth->in_group(1)) { ?>
                                                    <td><?php echo $artwork['participant_nric_no']; ?></td>
                                                    <td><?php echo $artwork['participant_contact_no']; ?></td>
                                                <?php } ?>
                                                <td><?php echo $artwork['created_at']; ?></td>
                                                <td>
                                                    <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $artwork['id']; ?>">
                                                        <div class="approve label label-info">View Details</div>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <?php if($this->ion_auth->in_group(2)) { ?>
                                                    <th><input name="select" type="checkbox" value="<?php echo $artworks['id']; ?>"></th>
                                                <?php } ?>
                                                <td><?php echo $artworks['id']; ?></td>
                                                <td><?php echo $artworks['artwork_category_name']; ?></td>
                                                <td><?php echo $artworks['artwork_name']; ?></td>
                                                <td>
                                                    <a class="artwork-image" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $artworks['id']; ?>">
                                                        <img style="width:100px;" src="<?php echo base_url() . 'uploads-thumbnail/' . $artworks['artwork_full_image']; ?>"/>
                                                    </a>
                                                </td>
                                                <td><?php echo $artworks['participant_name']; ?></td>
                                                <td><?php echo $artworks['participant_nric_no']; ?></td>
                                                <!--<td><?php /*echo $artworks['participant_email']; */?></td>-->
                                                <td><?php echo $artworks['participant_contact_no']; ?></td>
                                                <td><?php echo $artworks['created_at']; ?></td>
                                                <td>
                                                    <a class="viewArtworkButton" data-toggle="modal" data-target="#artworkDetailModal" href="<?php echo base_url() . 'admin/submissions/view/' . $artworks['id']; ?>">
                                                        <div class="label label-info">View</div>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php }
                                    } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- Render Pagination -->
                    <div class="pagination-container">
                        <div class="pagination pull-left">
                            <?php echo $pagination; ?>
                        </div>
                        <div class="pagination-text pull-left">
                            <?php if (isset($pagination_message)) {
                                echo $pagination_message;
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade artworkDetailModal" id="artworkDetailModal" tabindex="-1" role="dialog" aria-labelledby="artworkDetailModal" aria-hidden="true">
            <div class="modal-dialog">
                <a data-dismiss="modal" aria-label="Close" id="close-popup" style="display: inline;"></a>
                <div class="modal-content">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    </div>
</div>
