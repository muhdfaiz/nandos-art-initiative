<div class="submission-detail">
    <div class="artworkImages col-xs-12 col-md-6">
        <div class="ribbon-wrapper"><div class="ribbon-red">Artwork Full</div></div>
        <div class="slider" style="visibility: hidden">
            <ul class="bxslider list-unstyled">
                <li class="artwork-main">
                    <img class="zoom artwork-main" data-zoom-image="<?php echo base_url() . 'uploads/' . $artwork['artwork_full_image']; ?>" src="<?php echo base_url() . 'uploads/' . $artwork['artwork_full_image']; ?>"/>
                </li>

                <?php if (isset($artwork['artwork_highlight_image']) && $artwork['artwork_highlight_image']) { ?>
                    <li class="artwork-main">
                        <img class="zoom artwork-highlight" data-zoom-image="<?php echo base_url() . 'uploads/' . $artwork['artwork_highlight_image']; ?>" src="<?php echo base_url() . 'uploads/' . $artwork['artwork_highlight_image']; ?>" />
                    </li>
                <?php } ?>

            </ul>

            <div id="bx-pager">
                <a data-slide-index="0" href="">
                    <img src="<?php echo base_url() . 'uploads-thumbnail/' . $artwork['artwork_full_image']; ?>"/>
                </a>

                <?php if (isset($artwork['artwork_highlight_image']) && $artwork['artwork_highlight_image']) { ?>
                    <a data-slide-index="1" href="">
                        <img src="<?php echo base_url() . 'uploads-thumbnail/' . $artwork['artwork_highlight_image']; ?>" />
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="spinner" style="display: none;">
            <img src="<?php echo asset('css/backend/images/bx_loader.gif'); ?>" />
        </div>
    </div>

    <div class="col-xs-12 col-md-6 p-l-30">
        <div class="m-b-10 col-xs-12">
            <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                <div class="title">
                    <b>Artwork Name:</b>
                </div>
                <div class="desc">
                    <?php if (isset($artwork['artwork_name']) && $artwork['artwork_name']) {
                        echo $artwork['artwork_name'];
                    } else { ?>
                        N/A
                    <?php } ?>
                </div>
            </div>

            <?php if($this->ion_auth->in_group(1)) { ?>
                <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                    <div class="title">
                        <b>Submitted Date</b>
                    </div>
                    <div class="desc">
                        <?php if (isset($artwork['created_at']) && $artwork['created_at']) {
                            echo $artwork['created_at'];
                        } else {
                            echo 'N/A';
                        } ?>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="m-b-20 col-xs-12">
            <div class="title">
                <b>Artwork Creative Rationale:</b>
            </div>
            <div class="desc">
                <?php if (isset($artwork['artwork_creative_rationale']) && $artwork['artwork_creative_rationale']) {
                    echo $artwork['artwork_creative_rationale'];
                } else { ?>
                    N/A
                <?php } ?>
            </div>
        </div>

        <div class="m-b-10 col-xs-12">
            <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                <div class="title">
                    <b>Participant Name:</b>
                </div>
                <div class="desc">
                    <?php if (isset($artwork['participant_name']) && $artwork['participant_name']) {
                        echo $artwork['participant_name'];
                    } else { ?>
                        N/A
                    <?php } ?>
                </div>
            </div>

            <?php if($this->ion_auth->in_group(1)) { ?>
                <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                    <div class="title">
                        <b>Participant Gender:</b>
                    </div>
                    <div class="desc">
                        <?php if (isset($artwork['participant_gender_id']) && $artwork['participant_gender_id']) {
                            if ($artwork['participant_gender_id'] == '1') {
                                echo 'Male';
                            } else {
                                echo 'Female';
                            }
                        } else { ?>
                            N/A
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>

        <?php if($this->ion_auth->in_group(1)) { ?>
            <div class="m-b-10 col-xs-12">
                <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                    <div class="title">
                        <b>Participant NRIC No:</b>
                    </div>
                    <div class="desc">
                        <?php if (isset($artwork['participant_nric_no']) && $artwork['participant_nric_no']) {
                            echo $artwork['participant_nric_no'];
                        } else { ?>
                            N/A
                        <?php } ?>
                    </div>
                </div>
                <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                    <div class="title">
                        <b>Participant Email:</b>
                    </div>
                    <div class="desc">
                        <?php if (isset($artwork['participant_email']) && $artwork['participant_email']) {
                            echo $artwork['participant_email'];
                        } else { ?>
                            N/A
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="m-b-10 col-xs-12">
                <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                    <div class="title">
                        <b>Participant Contact No:</b>
                    </div>
                    <div class="desc">
                        <?php if (isset($artwork['participant_contact_no']) && $artwork['participant_contact_no']) {
                            echo $artwork['participant_contact_no'];
                        } else { ?>
                            N/A
                        <?php } ?>
                    </div>
                </div>
                <div style="padding-left: 0;" class="p-l-0 col-xs-6">
                    <div class="title">
                        <b>Participant College Name:</b>
                    </div>
                    <div class="desc">
                        <?php if (isset($artwork['college_name']) && $artwork['college_name']) {
                            if ($artwork['college_name'] == 'Other') {
                                echo $artwork['participant_college_name'];
                            } else {
                                echo $artwork['college_name'];
                            };
                        } else {
                            echo 'N/A';
                        } ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="title">
                    <b>Participant Home Address:</b>
                </div>
                <div class="desc">
                    <?php if (isset($artwork['participant_home_address']) && $artwork['participant_home_address']) {
                        echo $artwork['participant_home_address'];
                    } else { ?>
                        N/A
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <?php if($this->ion_auth->in_group(2)) { ?>
            <?php if (isset($shortlisted) && !$shortlisted) { ?>
                <div class="col-xs-12">
                    <a href="#" class="shortlist-single-artwork m-l-0 m-t-20 reject btn btn-primary"
                       data-artwork-id="<?php echo $artwork['id']; ?>"
                       data-artwork-name="<?php echo $artwork['artwork_name']; ?>"
                       data-artwork-category-id="<?php echo $artwork['artwork_category_id']; ?>">Add Into Shortlist</a>
                </div>
            <?php } ?>

            <?php if (isset($shortlisted) && isset($finalist) && $shortlisted && !$finalist) { ?>
                <div class="col-xs-12">
                    <a href="#" class="remove-shortlist-single-artwork m-l-0 m-t-20 reject btn btn-info"
                       data-artwork-id="<?php echo $artwork['id']; ?>"
                       data-artwork-name="<?php echo $artwork['artwork_name']; ?>"
                       data-artwork-category-id="<?php echo $artwork['artwork_category_id']; ?>">Remove From Shortlist</a>
                </div>
                <div class="col-xs-12">
                    <a class="finalist-single-artwork m-l-0 m-t-20 reject btn btn-primary"
                       data-artwork-id="<?php echo $artwork['id']; ?>"
                       data-artwork-name="<?php echo $artwork['artwork_name']; ?>"
                       data-artwork-category-id="<?php echo $artwork['artwork_category_id']; ?>">Add Into Finalist</a>
                </div>
            <?php } ?>

            <?php if (isset($shortlisted) && isset($finalist) && $shortlisted && $finalist) { ?>
                <div class="col-xs-12">
                    <a href="#" class="remove-finalist-single-artwork m-l-0 m-t-20 reject btn btn-info"
                       data-artwork-id="<?php echo $artwork['id']; ?>"
                       data-artwork-name="<?php echo $artwork['artwork_name']; ?>"
                       data-artwork-category-id="<?php echo $artwork['artwork_category_id']; ?>">Remove From Finalist</a>
                </div>
            <?php } ?>
        <?php } ?>

    </div>


</div>