<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artwork extends MY_Controller
{

    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->model('Artwork_model');
        $this->load->helper('form');
        $this->load->model('artwork_category/Artwork_category_model');
    }

    /**
     * Display List of All Artworks
     *
     * @param int $offset
     */
    public function index($offset = 0)
    {
        //get artwork limit by get
        (isset($_GET['limit']) ? $get_limit = $_GET['limit'] : $get_limit = 10);

        $artworkCategories = $this->Artwork_category_model->fields('id, name')->get_all();

        foreach ($artworkCategories as $key => $artworkCategory) {
            $data['artwork_categories'][$artworkCategory['id']] = $artworkCategory['name'];
        }

        $data['total_artwork'] = $this->Artwork_model->count(); // retrieve the total number of submission
        $data['artworks'] = $this->Artwork_model->get($get_limit, $offset);
        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/", $data['total_artwork'],
            $limit = $get_limit, $urlSegement = 3);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $data['colleges'] = $this->getColleges();

        $data['get_limit'] = $get_limit;
        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('index', $data);
    }

    /**
     * View Details of Artwork
     *
     * @param $artworkID
     */
    public function view($artworkID)
    {
        $data['artwork'] = $this->Artwork_model->getByID((int) $artworkID);

        $this->load->view('view', $data);
    }

    /**
     * Filter Artwork Submission
     *
     * @param int $offset
     */
    public function filter($offset = 0)
    {
        $artworkCategories = $this->Artwork_category_model->fields('id, name')->get_all();

        foreach ($artworkCategories as $key => $artworkCategory) {
            $data['artwork_categories'][$artworkCategory['id']] = $artworkCategory['name'];
        }

        $data['total_artwork'] = $this->Artwork_model->filter($this->input->get(), $count = true);
        $data['artworks'] = $this->Artwork_model->filter($this->input->get(), $count = false, 5, $offset);

        $data['pagination'] = $this->setPagination(base_url() . "admin/submissions/filter", $data['total_artwork'],
            $limit = 5, $urlSegement = 4);

        if($data['pagination'] != '') {
            $data['pagination_message'] = 'Showing '. ((($this->pagination->cur_page-1)*$this->pagination->per_page)+1) .
                ' to ' . ($this->pagination->cur_page*$this->pagination->per_page) . ' of ' . $this->pagination->total_rows;
        }

        $data['colleges'] = $this->getColleges();

        $this->template->title('Admin Dashboard');
        $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
            ->set_layout('admin')->build('index', $data);
    }

    /**
     * Export data to CSV
     */
    public function export()
    {
        header("Content-Disposition: attachment; filename=List_Of_Artworks-" . date("d-m-Y") . ".csv");
        header("Content-Type: application/vnd.ms-excel");

        $totalArtwork = $this->Artwork_model->filter($this->input->get(), $count = true);

        // Open output stream
        $handle = fopen('php://output', 'w');

        // Add CSV headers
        fputcsv($handle, array(
            'Artwork ID',
            'Total Votes',
            'Votes By',
            'Artwork Name',
            'Artwork Creative Rational',
            'Artwork Full Image URL',
            'Artwork Highlight Image URL',
            'Participant Name',
            'Participant Gender',
            'Participant NRIC No',
            "Participant Email",
            "Participant Contact No",
            "Participant is Student",
            "Participant College Name",
            "Participant Home Address",
            "Submission Date",
        ));

        if(intval($totalArtwork < 500)) {
            $max = 1;
        } else {
            $max = intval($totalArtwork / 500);
        }

        for ($i = 0; $i <= $max; $i++) {
            $artworks = $this->Artwork_model->filter($this->input->get(), $count = false, $limit = 500, $offset = $i * 500);

            foreach ($artworks as $key => $artwork) {

                $participantGender = ($artwork['participant_gender_id'] == '1') ? 'Male' : 'Female';

                if (!empty($artwork['artwork_full_image'])) {
                    $artworkFullImage = base_url() . 'uploads/' . $artwork['artwork_full_image'];
                } else {
                    $artworkFullImage = 'N/A';
                }

                if (!empty($artwork['artwork_highlight_image'])) {
                    $artworkHighlightImage = base_url() . 'uploads/' . $artwork['artwork_highlight_image'];
                } else {
                    $artworkHighlightImage = 'N/A';
                }

                if ($artwork['participant_is_student']) {
                    $participantIsStudent = 'true';
                    if ($artwork['college_name'] == 'Other') {
                        $collegeName = $artwork['participant_college_name'];
                    } else {
                        $collegeName = $artwork['college_name'];
                    }
                } else {
                    $collegeName = 'N/A';
                    $participantIsStudent = 'false';
                }

                fputcsv($handle, array(
                    $artwork['id'],
                    $artwork['total_votes'],
                    $artwork['judges'],
                    $artwork['artwork_name'],
                    $artwork['artwork_creative_rationale'],
                    $artworkFullImage,
                    $artworkHighlightImage,
                    $artwork['participant_name'],
                    $participantGender,
                    $artwork['participant_nric_no'],
                    $artwork['participant_email'],
                    (string) $artwork['participant_contact_no'],
                    $participantIsStudent,
                    $collegeName,
                    $artwork['participant_home_address'],
                    $artwork['created_at']
                ));
            }
        };

        fclose($handle);

    }

    /**
     * Set Bootstrap Pagination
     *
     * @param $baseURL
     * @param $totalArtwork
     * @param $perPage
     * @param $uriSegment
     * @return mixed
     */
    private function setPagination($baseURL, $totalArtwork, $perPage, $uriSegment)
    {
        $this->load->config('pagination');
        $this->config->config['pagination']['total_rows'] = $totalArtwork;
        $choice = $totalArtwork / $perPage;
        $this->config->config['pagination']["num_links"] = 9;
        $this->config->config['pagination']["per_page"] = $perPage;
        $this->config->config['pagination']['base_url'] = $baseURL;
        $this->config->config['pagination']['use_page_numbers'] = true;
        $this->config->config['pagination']['uri_segment'] = $uriSegment;
        //$this->config->config['pagination']['first_url'] = $this->config->config['pagination']['base_url'].'?'.http_build_query($_GET);

        if (count($_GET) > 0) $this->config->config['pagination']['suffix'] = '?' . http_build_query($_GET, '', "&");

        $this->pagination->initialize($this->config->config['pagination']);

        return $this->pagination->create_links();
    }

    /**
     * Get list of colleges available
     *
     * @return array
     */
    private function getColleges()
    {
        $this->load->model('college/college_model');

        // Retrieve list of colleges available to populate as drop down
        $colleges = $this->college_model->fields('id, name')->get_all();

        foreach ($colleges as $key => $college) {
            $collegesDropdown[$college['id']] = $college['name'];
        }

        return $collegesDropdown;
    }


}
