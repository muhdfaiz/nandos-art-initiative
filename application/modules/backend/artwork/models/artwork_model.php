<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artwork_model extends MY_Model
{
    // Specify Table Name
    public $table = 'artworks';

    // Specify Primary Key
    public $primary_key = 'id';

    // Specify if timestamps need to update or create.
    public $timestamps = true;

    // Return query result as array
    public $return_as = 'array';

    /**
     * Artwork model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get Latest Artworks
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function get($limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $query = $this->db->select('a.*, ac.id as artwork_category_id, ac.name as artwork_category_name')
            ->from('artworks as a')
            ->join('artwork_categories as ac', 'ac.id = a.artwork_category_id')
            ->order_by('a.id', 'desc')->limit($limit, $offset)->get();

        return $query->result_array();
    }

    /**
     * Get Artwork Details by Artwork ID
     *
     * @param $artworkID
     * @return array
     */
    public function getByID($artworkID)
    {
        $query = $this->db->select('a.*, ac.id as artwork_category_id, ac.name as artwork_category_name, c.id as college_id, c.name as college_name')
            ->from('artworks as a')
            ->join('artwork_categories as ac', 'ac.id = a.artwork_category_id', 'left')
            ->join('colleges as c', 'c.id = a.participant_college_id', 'left')
            ->where('a.id', $artworkID)->get();

        return array_shift($query->result_array());
    }

    /**
     * Filter Artwork Submissions
     *
     * @param $filterData
     * @param int $limit
     * @param int $offset
     * @param bool|false $count
     * @return mixed
     */
    public function filter($filterData, $count = false, $limit = 30, $offset = 0)
    {
        if ($offset === 1 OR $offset === 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }

        $this->db->select('a.*, ac.id as artwork_category_id, ac.name as artwork_category_name, c.id as college_id,
            c.name as college_name')
            ->from('artworks as a')
            ->join('artwork_categories as ac', 'ac.id = a.artwork_category_id')
            ->join('colleges as c', 'c.id = a.participant_college_id', 'left');

        if (!empty($filterData['artwork_category_id'])) {
            $this->db->where('a.artwork_category_id', $filterData['artwork_category_id']);
        }

        if (!empty($filterData['participant_college_id'])) {
            $this->db->where('a.participant_college_id', $filterData['participant_college_id']);
        }

        if (!empty($filterData['participant_name'])) {
            $this->db->like('a.participant_name', $filterData['participant_name']);
        }


        if (!empty($filterData['participant_email'])) {
            $this->db->like('a.participant_email', $filterData['participant_email']);
        }

        if (!empty($filterData['participant_nric_no'])) {
            $this->db->like('a.participant_nric_no', $filterData['participant_nric_no']);
        }

        if (!empty($filterData['submission_date_range'])) {
            $dates = explode(' - ', $filterData['submission_date_range']);
            $startDate = DateTime::createFromFormat('d/m/Y', $dates[0]);
            $startDate = $startDate->format('Y-m-d') . ' 00:00:00';

            $endDate = DateTime::createFromFormat('d/m/Y', $dates[1]);
            $endDate = $endDate->format('Y-m-d') . ' 23:59:59';

            $this->db->where('a.created_at >=', $startDate );
            $this->db->where('a.created_at <=', $endDate);
        }

        if ($count) {
            $query = $this->db->get();
            return $query->num_rows();
        }

        if (is_null($offset) or is_null($limit)) {
            $query = $this->db->order_by('a.id', 'desc')->get();
        } else {
            $query = $this->db->order_by('a.id', 'desc')->limit($offset * $limit + $limit, $offset * $limit)->get();
        }

        $data = $query->result_array();

        foreach ($data as $key => $result) {
            $query1 = $this->db->select('f.*, u.first_name as judge_first_name, u.last_name as judge_last_name')->from('finalists as f')
                ->join('users as u', 'u.id = f.user_id')
                ->where('artwork_id', $result['id'])->get();

            $judgeData = $query1->result_array();

            $data[$key]['total_votes'] = count($judgeData);
            $data[$key]['judges'] = implode(', ', array_map(function ($judgeName) {
                return $judgeName['judge_first_name'] . ' ' . $judgeName['judge_last_name'];
            }, $judgeData));
        }

        return $data;
    }

    /**
     * Get All Artwork By Category ID and exclude artwork
     * that have been shortlist by judges
     *
     * @param $categoryID
     * @param bool $count
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getArtworksByCategoryForJudges($categoryID, $count = false, $limit = 10, $offset = 0)
    {
        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $subQuery = "a.`id` NOT IN (SELECT `artwork_id` FROM `shortlists` WHERE `user_id` = " . $this->ion_auth->user()->row()->id . ")";
        $this->db->select('a.*, ac.id as artwork_category_id, ac.name as artwork_category_name')->from('artworks as a');
        $this->db->join('artwork_categories as ac', 'ac.id=a.artwork_category_id');
        $this->db->where($subQuery, NULL, FALSE);
        $this->db->where('a.artwork_category_id', $categoryID);

        if ($count) {
            $query = $this->db->get();
            return $query->num_rows();
        }

        if (is_null($offset) or is_null($limit)) {
            $query = $this->db->order_by('a.id', 'desc')->get();
        } else {
            $query = $this->db->order_by('a.id', 'desc')->limit($limit, $offset)->get();
        }

        return $query->result_array();
    }

}