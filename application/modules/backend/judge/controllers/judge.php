<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Judge extends MY_Controller
{

    /**
     * Judge constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
        // If user not login, redirect user to admin login page.
        $this->userNotLoggedIn();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->model('finalist/Finalist_model');
        $this->load->model('artwork_category/Artwork_category_model');
    }

    /**
     * Display current votes status for judges
     */
    public function index()
    {
        $artworkCategories = $this->Artwork_category_model->get_all();

        if ($artworkCategories) {
            $data = '';
            foreach($artworkCategories as $key => $artworkCategory) {

                $judges = $this->ion_auth->users('2')->result();

                foreach ($judges as $key => $judge) {
                    $query = $this->db->select('count(artwork_id) as total_vote')->where('user_id', $judge->id)
                        ->where('artwork_category_id', $artworkCategory['id'])->from('finalists')->get();

                    $firstVote = array_shift($query->result_array());
                    $totalVote = $firstVote['total_vote'];
                    $data['category'][$artworkCategory['code']][$key] = json_decode(json_encode($judge), true);
                    $data['category'][$artworkCategory['code']][$key]['total_vote'] = $totalVote;
                    $data['category'][$artworkCategory['code']][$key]['artwork_category'] = $artworkCategory['name'];
                }
            }

            $this->template->title('Admin Dashboard');
            $this->template->set_partial('sidebar', 'partial/backend/sidebar')->set_partial('content_header', 'partial/backend/content_header')
                ->set_layout('admin')->build('index', $data);
        }
    }

}