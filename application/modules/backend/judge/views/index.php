<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo 'admin/dashboard'; ?>">Home</a></li>
        <li class="active">Votes Status</li>
    </ol>
</div>

<div class="body">
    <div class="row submission">

        <div class="col-xs-12 table-heading">
            <div class="col-xs-12 col-sm-7 text no-padding">
                <h4 class="pull-left">Votes Status</h4>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-condensed table-responsive">
                        <thead>
                        <tr>
                            <th>Artwork Category</th>
                            <th>Judges Name</th>
                            <th>Current Vote</th>
                            <th>Current Progress</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($category)) { ?>
                            <?php foreach($category as $key => $artworkCategory) { ?>
                                <tr>
                                    <td rowspan=<?php echo count($artworkCategory); ?>><strong><?php echo $category[$key][key($category[$key])]['artwork_category']; ?></strong></td>
                                    <?php foreach ($artworkCategory as $key1 => $judge) { ?>
                                            <td><?php echo $judge['first_name'] . ' ' . $judge['last_name']; ?></td>
                                            <td><?php echo $judge['total_vote'] . '/20'; ?></td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $judge['total_vote'] / 20 * 100; ?>%">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                            <?php } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="4" style="text-align:center"><div class="m-t-20">Right now current system doesn't have any judges.</div></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>