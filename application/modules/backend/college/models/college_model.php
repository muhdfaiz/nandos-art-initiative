<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class College_model extends MY_Model
{

    // Specify Table Name
    public $table = 'colleges';

    // Specify Primary Key
    public $primary_key = 'id';

    // Specify if timestamps need to update or create.
    public $timestamps = true;

    // Return query result as array
    public $return_as = 'array';

    /**
     * User_model constructor.
     */
    public function __construct()
    {
        $this->has_many['artworks'] = array('foreign_model'=>'Artwork_model','foreign_table'=>'artworks','foreign_key'=>'participant_college_id','local_key'=>'id');
        parent::__construct();
    }

}