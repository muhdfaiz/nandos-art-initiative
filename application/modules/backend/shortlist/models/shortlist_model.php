<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shortlist_model extends MY_Model
{
    // Specify Table Name
    public $table = 'shortlists';

    // Specify Primary Key
    public $primary_key = 'id';

    // Specify if timestamps need to update or create.
    public $timestamps = true;

    // Return query result as array
    public $return_as = 'array';

    /**
     * Shortlist model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     *
     * Get Artwork Shortlist by logged in user
     * @param $categoryID
     * @param bool|false $count
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getShortListing($categoryID, $count = false, $limit = 10, $offset = 0)
    {
        $loggedInUserID = (int)$this->ion_auth->user()->row()->id;

        if ($count) {
            $query = $this->db->query("SELECT COUNT(*) AS `numrows` FROM `shortlists` AS s WHERE s.user_id =" . $loggedInUserID .
                      " AND s.artwork_id NOT IN (SELECT `artwork_id` FROM `finalists` WHERE `user_id` = " . $loggedInUserID . ") AND artwork_category_id = $categoryID");
            return array_shift($query->result())->numrows;
        }

        if ($offset == 1 OR $offset == 0) {
            $offset = 0;
        } else {
            $offset = $offset - 1;
        }
        $offset = $offset * $limit;

        $subQuery = "s.`artwork_id` NOT IN (SELECT `artwork_id` FROM `finalists` WHERE `user_id` = " . $loggedInUserID . ")";
        $this->db->select('s.id as shortlist_id, s.artwork_id as shortlist_artwork_id, s.user_id as shortlist_user_id,
            s.artwork_category_id as shortlist_artwork_category_id, a.*, ac.id as artwork_category_id, ac.name as artwork_category_name')
            ->from('shortlists as s');
        $this->db->join('artworks as a', 'a.id = s.artwork_id');
        $this->db->join('artwork_categories as ac', 'ac.id = s.artwork_category_id');
        $this->db->where('a.artwork_category_id', $categoryID);
        $this->db->where($subQuery, NULL, FALSE);
        $this->db->where('s.user_id', $loggedInUserID);

        $query = $this->db->order_by('a.id', 'desc')->limit($limit, $offset)->get();

        return $query->result_array();
    }

}