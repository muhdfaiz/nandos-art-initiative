<div class="wrapper">

    <?php echo $template['partials']['header']; ?>

    <div class="mainContent">
        <div class="submission">
            <div class="submissionContent">
                <h1>Submission</h1>
                <h4 class="submissionSub">Submission close on 31 May at 11.59pm.</h4>
                <br />
                <br />
                <h4><span class="pinkFill">* Compulsory Fields</span></h4> <!-- UPDATED -->

                <?php echo form_open_multipart(base_url() . 'submission/store', array('id' => 'artSubmission' )); ?>
                    <div class="formField">
                        <h3>Select Category*</h3>
                            <!-- Input Radio For Artwork Category -->
                            <?php foreach ($artworkCategories as $key => $artworkCategory) { ?>
                                <p class="btnRadio">
                                    <label>
                                        <?php if ($key == 0) { ?>
                                            <input type="radio" name="artwork_category_id" id="artwork_category_id1"  value="<?php echo $artworkCategory['id'] ?>"
                                                   data-validation="required"
                                                   data-validation-error-msg="*Please choose one of the artwork category above."
                                                   data-validation-error-msg-container="#artworkCategoryError"/>
                                        <?php } else { ?>
                                            <input type="radio" name="artwork_category_id" id="artwork_category_id2" value="<?php echo $artworkCategory['id'] ?>"
                                                   data-validation="required"
                                                   data-validation-error-msg="*Please choose one of the artwork category above."
                                                   data-validation-error-msg-container="#artworkCategoryError"/>
                                        <?php } ?>
                                        <?php echo $artworkCategory['name'] ?>
                                    </label>
                                </p>
                            <?php } ?>
                            <div class="clear"></div>
                            <div><span id="artworkCategoryError" class="errorMsg"></span></div>
                        <br />
                        <strong>Upload Artwork*</strong><span class="formDisclaimer"> ( Photograph(s) cannot be larger than 5MB each. Permitted file formats are jpeg, gif and png only. )</span> <br />
                        <br />
                        <h4>Full view of Artwork</h4>
                        <div class="fullImage">
                            <input type="file" name="full_image" id="fullImage" class="inputfile"
                                   data-validation="required mime size"
                                   data-validation-allowing="jpeg, jpg, png, gif"
                                   data-validation-max-size="5M"
                                   data-validation-error-msg-required="*Artwork highlight image required."
                                   data-validation-error-msg-size="*The file you are trying to upload is too large (max 5MB)."
                                   data-validation-error-msg-mime="*Permitted file formats are jpeg, gif and png only."
                                   data-validation-error-msg-container="#artworkFullImageError">
                            <label for="fullImage"><div class="fileName">No File Selected</div><div class="uploadButton">Browse</div></label>
                        </div>
                        <span id="artworkFullImageError" class="errorMsg"></span>
                        <div class="clear"></div>
                        <br />

                        <h4>Artwork Highlight 1 ( Optional )</h4>
                        <div class="highlightImage">
                            <input type="file" name="highlight_image" id="highlightImage" class="inputfile"
                                   data-validation="mime size"
                                   data-validation-allowing="jpeg, jpg, png, gif"
                                   data-validation-max-size="5M"
                                   data-validation-error-msg-size="*The file you are trying to upload is too large (max 5MB)."
                                   data-validation-error-msg-mime="*Permitted file formats are jpeg, gif and png only."
                                   data-validation-error-msg-container="#artworkHightlightImageError">
                            <label for="highlightImage"><div class="fileName">No File Selected</div><div class="uploadButton">Browse</div></label>
                        </div>
                        <span id="artworkHightlightImageError" class="errorMsg"></span>
                    </div>
                    <div class="formField rightField">
                        <!-- Name of Artwork -->
                        <h3>Name of Artwork*</h3>
                        <input name="artwork_name" id="artwork_name" type="text" class="fullField"
                               data-validation="required"
                               data-validation-error-msg-required="*Please fill in Artwork Name."
                               data-validation-error-msg-container="#artworkNameError">
                        <span id="artworkNameError" class="errorMsg"></span>
                        <br />
                        <br />

                        <!-- Creative Rationale -->
                        <strong>Creative Rationale*</strong><span class="errorMsg"> ( in English or Bahasa Malaysia )</span>
                        <textarea name="artwork_creative_rationale" id="artwork_creative_rationale" class="textArea"
                                  data-validation="required length"
                                  data-validation-length="max1000"
                                  data-validation-error-msg-required="*Please fill in Creative Rationale."
                                  data-validation-error-msg-container="#artworkCreativeRationaleError"></textarea>
                        <p class="errorMsg">*<span id="maxlength">1000</span> character remaining</p>
                        <span id="artworkCreativeRationaleError" class="errorMsg"></span>
                    </div>
                    <div class="clear"></div>
                    <hr />
                    <h1>Personal Details</h1>
                    <br />
                    <br />
                    <div class="formField">
                        <!-- Name -->
                        <h3>Name as per NRIC*</h3>
                        <input name="participant_name" type="text" id="participant_name" class="fullField"
                               data-validation="required custom"
                               data-validation-regexp="^[a-zA-Z ]*$"
                               data-validation-error-msg-required="*Please fill in your name."
                               data-validation-error-msg-custom="*Only alphabets and space are allowed."
                               data-validation-error-msg-container="#nameError"/>
                        <span id="nameError" class="errorMsg"></span>
                        <br />
                        <br />

                        <!-- Gender -->
                        <h3>Gender*</h3>
                        <p class="btnRadio">
                            <label>
                                <input type="radio" name="participant_gender_id" value="1"
                                       data-validation="required"
                                       data-validation-error-msg="*Please choose one of the gender above."
                                       data-validation-error-msg-container="#genderError"/>
                                Male</label>
                        </p>
                        <p class="btnRadio">
                            <label>
                                <input type="radio" name="participant_gender_id" value="2"
                                       data-validation="required"
                                       data-validation-error-msg="*Please choose one of the gender above."
                                       data-validation-error-msg-container="#genderError"/>
                                Female</label>
                        </p>
                        <div class="clear"></div>
                        <div><span id="genderError" class="errorMsg"></span></div>
                        <br>
                        <br />

                        <!-- NRIC No -->
                        <h3>NRIC Number*</h3>
                        <input name="participant_nric_no" type="text" id="participant_nric_no" class="fullField" placeholder="123456-78-9012"
                               data-validation="required custom"
                               data-validation-regexp="^\d\d(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[-][0-9]{2}[-][0-9]{4}$"
                               data-validation-error-msg-required="*Please fill in your NRIC number."
                               data-validation-error-msg-custom="*Please fill in valid NRIC number."
                               data-validation-error-msg-container="#nricNoError"/>
                        <span id="nricNoError" class="errorMsg"></span>
                        <br />
                        <br />

                        <!-- Email -->
                        <h3>Email*</h3>
                        <input name="participant_email" type="email" id="participant_email" class="fullField"
                               data-validation="required"
                               data-validation="require email"
                               data-validation-optional="false"
                               data-validation-error-msg="*Please fill in valid Email."
                               data-validation-error-msg-container="#emailError"/>
                        <span id="emailError" class="errorMsg"></span>
                    </div>
                    <div class="formField rightField">
                        <h3>Contact No.*</h3>
                        <!-- UPDATED -->
                        <div class="customeTelcoBox">
                            <div class="selectArrow selectArrowTelco"></div>
                            <select name="participant_contact_no_prefix" class="telco">
                                <option>010</option>
                                <option>011</option>
                                <option>012</option>
                                <option>013</option>
                                <option>014</option>
                                <option>015</option>
                                <option>016</option>
                                <option>017</option>
                                <option>018</option>
                                <option>019</option>
                            </select>
                        </div>
                        <!-- Contact No -->
                        <input name="participant_contact_no" type="text" id="participant_contact_no" class="telcoNo" data-validation="required number length"
                               data-validation-length="7-8",
                               data-validation-error-msg-required="*Please fill in your contact no.",
                               data-validation-error-msg-number="*Contact no may only contain number.",
                               data-validation-error-msg-length="*Contact no must be between 7-8 characters.",
                               data-validation-error-msg-container = "#contactNoError"]/>

                        <span id="contactNoError" class="errorMsg"></span> <br />
                        <br />
                        <input name="participant_is_student" type="checkbox" style="width: 0.1px; position: absolute; opacity: 0.0" id="participant_is_student" class="styled"/>
                        <h3 class="checkBoxText">I am a college/ university student</h3>
                        <!-- UPDATED -->
                        <div class="customeCheckBox">
                            <div class="selectArrow"></div>
                            <?php echo form_dropdown('participant_college_id', $collegesDropdown, '1', 'id="participant_college_id" class="collStudent"'); ?>
                        </div>
                        <!-- College Name -->
                        <input name="participant_college_name" type="text" class="fullField" placeholder="Your College / University Name"
                               data-validation="college_name"
                               data-validation-error-msg-required="*Please fill up college name."
                               data-validation-error-msg-container="#collegeNameError">
                        <span id="collegeNameError" class="errorMsg"></span>
                        <br />
                        <br />

                        <!-- Home Address -->
                        <h3>Home Address*</h3>
                        <textarea name="participant_home_address" id="participant_home_address" class="addArea"
                                  data-validation="required"
                                  data-validation-error-msg="*Please fill in your home address."
                                  data-validation-error-msg-container="#addressError"></textarea>
                        <span id="addressError" class="errorMsg"></span>
                    </div>
                    <div class="clear"></div>
                    <hr />
                    <!-- Agree 1 -->
                    <input name="agree1" type="checkbox" style="width: 0.1px; position: absolute; opacity: 0.0" id="agree1" class="styled"
                           data-validation="required"
                           data-validation-error-msg-required="*Please ensure you have read & agree to our Terms & Conditions and Disclosure & use of personal data."
                           data-validation-error-msg-container="#agreeError">
                    <p class="checkBoxText">I have read and agree to the <a href="<?php echo base_url() . 'terms_and_conditions'; ?>" target="_blank">Terms & Conditions</a>.</p> <!-- UPDATED -->

                    <!-- Agree 2 -->
                    <input name="agree2" type="checkbox" style="width: 0.1px; position: absolute; opacity: 0.0;" id="agree2" class="styled"
                           data-validation="required"
                           data-validation-error-msg-required="*Please ensure you have read & agree to our Terms & Conditions and Disclosure & use of personal data."
                           data-validation-error-msg-container="#agreeError">
                    <p class="checkBoxText">I have read the ‘<a href="<?php echo base_url() . 'terms_and_conditions#disclosure_personal_data'; ?>" target="_blank">Disclosure and use of personal data</a>’ that is in accordance with the Personal Data Protection Act and agree to my personal data being used in the manner described.</p>

                    <input name="agree3" type="checkbox" style="width: 0.1px; position: absolute; opacity: 0.0" id="agree3"class="styled">

                    <p class="checkBoxText">I agree to receive communications from Nando’s.</p>
                    <div><span id="agreeError" class="errorMsg"></span></div>


                    <br />
                    <h4 class="finalDisclaimer">Please be sure to verify your information prior to submitting. You will not be able to edit it once you have submitted.</h4> <!-- UPDATED -->
                    <br />
                    <br />

                    <input type="submit" value="Submit" class="trans btnSubmit"/>

                <?php echo form_close(); ?>
            </div>
            <div class="textureSubmissionBackground"></div>
            <div class="textureSubmissionBottomLeft"></div>
            <div class="textureSubmissionBottom"></div>
        </div>
        <div id="popUpMessage" class="popUpThankYou">
            <div class="thankYouWrap">
                <div class="thankYouBox"></div>
                <a href="#" class="btnCloseSubmission trans"><img src="<?php echo asset('images/btn_close.png'); ?>" /></a>
                <div class="thankYouContent">
                    <h1 class="popupHeading"></h1>
                    <h4 class="popupContent"></h4>
                </div>
            </div>
            <div class="transBg">
            </div>
        </div>
    </div>
    <div class="spinner" style="display: none;">
        <img src="<?php echo asset('images/loader.gif'); ?>" />
    </div>

    <?php echo $template['partials']['footer']; ?>

</div>
<script src="<?php echo asset('js/frontend/jquery.form-validator.min.js'); ?>"></script>
<script type="text/javascript">
    $(".btnCloseSubmission, .transBg").click(function(event) {
        event.preventDefault();
        $('.popUpThankYou').fadeOut();
        if ($('#popUpMessage').hasClass('has-success')) {
            ga_virtualPage('/artinitiative/Thank-you',"Nando's Art Initiative - Thank you");
            window.location.href = "<?php echo base_url(); ?>";
        }
    });
    $("a.btnCloseMenu, .popUpBg").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeOut();
        enableScroll();
    });
    $("a.btnBurger").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeIn();
        disableScroll();
    });
    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }
    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }
    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
    function customRadio(radioName){
        var radioButton = $('input[name="'+ radioName +'"]');
        $(radioButton).each(function(){
            $(this).wrap( "<span class='custom-radio'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
        });
        $(radioButton).click(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
            $(radioButton).not(this).each(function(){
                $(this).parent().removeClass("selected");
            });
        });
    }
    $(document).ready(function (){

        customRadio("artwork_category_id");
        customRadio("participant_gender_id");

        var FIREFOX = /Firefox/i.test(navigator.userAgent);

        if (FIREFOX) {
            $('.selectArrow').hide();
        }

         /**
         * Added By Faiz
         */
        $(".inputfile").change(function(e) {
            var label	 = this.nextElementSibling,
                labelVal = label.innerHTML;

            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.querySelector( '.fileName' ).innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        $('#artwork_creative_rationale').restrictLength($('#maxlength'));

        $("#artSubmission").click(function(e){
            // Validate college name
            $.formUtils.addValidator({
                name : 'college_name',
                validatorFunction : function(value, el, config, language, form) {
                    var isStudent = $('#participant_is_student').is(":checked");
                    if (isStudent) {
                        if ($('#participant_college_id').find(':selected').text() == 'Other') {
                            if( !value ) {
                                return false;
                            }
                        }
                    }
                    return true;
                },
                errorMessage : '*Please fill up college name!',
                errorMessageKey: 'badEvenNumber'
            });

           $.validate({
                modules : 'file',
                scrollToTopOnError : false,
               onError : function($form) {
                   var mainMenu = $(".mainLogo").height();
                   var height = $(document).height();

                   $form.find(".error").each(function() {
                       if ($(this).offset().top < height) {
                           height = $(this).offset().top
                       }
                   });

                   $('html, body').animate({
                       scrollTop: height - mainMenu - 20
                   }, 1000);
               },
                onSuccess : function(e) {
                    var form = document.getElementById('artSubmission')
                    $.ajax({
                        url: "<?php echo base_url() . 'submission/store'; ?>",
                        type: 'post',
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        timeout: 600000,
                        beforeSend: function(result){
                            $('.spinner').show();
                        },
                        complete: function(result){
                            var response = JSON.parse(result.responseText);
                            console.log(response.error);
                            if (response.hasOwnProperty('error')) {
                                if (response.error.constructor === Array) {

                                } else {
                                    $('.popUpThankYou').addClass('has-error');
                                    $('.popupHeading').html('Submission Failed!');
                                    $('.popupContent').html('<span></span>' + response.error);
                                    $('.popUpThankYou').fadeIn();
                                }
                            }
                            if (response.hasOwnProperty('success')) {
                                $('.popUpThankYou').addClass('has-success');
                                $('.popupHeading').html('Submission Successful!');
                                $('.popupContent').html('<span></span>Thanks for participating. An e-mail with your submission details has been sent to you.')
                                $('.popUpThankYou').fadeIn();
                            }
                        },
                        error: function(result){
                            $('.spinner').hide();
                        }
                    }).done(function( data ) {
                        $('.spinner').hide();
                    });
                    return false;
                },
            });
        });
    })
</script>