<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submission extends MY_Controller
{

    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('college/college_model');
        $this->load->model('artwork_category/Artwork_category_model');
    }

    /**
     * Display Artwork Submission Form
     */
    public function create()
    {
        redirect(base_url(), 'refresh');
        // Load artwork category model & Retrieve all artwork categories.
        $artworkCategories = $this->Artwork_category_model->get_all();

        // Retrieve list of colleges available to populate as drop down
        $colleges = $this->college_model->fields('id, name')->get_all();

        foreach ($colleges as $key => $college) {
            $collegesDropdown[$college['id']] = $college['name'];
        }
        $this->template->title("Nando's Art Initiative - Submission");
        $this->template->set_partial('header', 'partial/frontend/header')->set_partial('footer', 'partial/frontend/footer')
            ->set_layout('frontend1')->build('submission', array('artworkCategories' => $artworkCategories, 'collegesDropdown' => $collegesDropdown ));
    }

    /**
     * Store Artwork Submission
     */
    public function store()
    {
        redirect(base_url(), 'refresh');
        // Form Validation
        $this->form_validation->set_rules('artwork_category_id', '', 'callback_artwork_category_id_check');
        $this->form_validation->set_rules('artwork_name', 'Artwork Name', 'required');
        $this->form_validation->set_rules('artwork_creative_rationale', 'Creative Rationale', 'required');
        $this->form_validation->set_rules('participant_gender_id', '', 'callback_gender_id_check');
        $this->form_validation->set_rules('participant_name', 'Name', 'require');
        $this->form_validation->set_rules('participant_email', 'Email', 'require|valid_email');
        $this->form_validation->set_rules('participant_contact_no_prefix', 'Contact No', 'required|numeric|exact_length[3]');
        $this->form_validation->set_rules('participant_contact_no', 'Contact No', 'required|numeric|min_length[7]|max_length[8]');
        $this->form_validation->set_rules('participant_home_address', 'Home Address', 'required');
        $this->form_validation->set_rules('agree1', 'Agree', 'required');
        $this->form_validation->set_rules('agree2', 'Agree', 'required');

        if ($this->form_validation->run() == FALSE) {
            header('Content-Type: application/json');
            echo json_encode(array('error' => $this->form_validation->error_array()));
            return;
        }

        // Retrieve all post data
        $postData = $this->input->post();

        // Load Artwork Model
        $this->load->model('artwork/Artwork_model');
        // Get Previous Submission by nric_no and artwork_category_id.
        // Every nric_no can submit one submission per artwork_category_id
        $previousSubmission = $this->Artwork_model->where('participant_nric_no', $postData['participant_nric_no'])
            ->where('artwork_category_id', $postData['artwork_category_id'])->get();

        if ($previousSubmission) {
            header('Content-Type: application/json');
            echo json_encode(array('error' => 'You are allowed to submit only one artwork per Artwork Category!'));
            return;
        }

        // Initialize Upload Library. Set upload config in /config/upload.php
        $this->load->config('upload');
        $this->load->library('upload', $this->config->config['submission']);
        $this->upload->initialize($this->config->config['submission']);

        // Retrieve uploaded file
        $imagesWantToUpload = $_FILES;

        // Remove highlight image from $imagesWantToUpload array when the error = 4.
        // Error 4 means user not upload the file. Highlight image is optional, so app must remove the image
        foreach ($imagesWantToUpload as $key => $imageWantToUpload) {
            if ($key === 'highlight_image' && $imagesWantToUpload[$key]['error'] === 4) {
                unset($imagesWantToUpload[$key]);
            }
        }

        //Process image uploading
        foreach ($imagesWantToUpload as $key1 => $imageWantToUpload1) {
            if ( ! $this->upload->do_upload($key1)) {
                $upload_error = $this->upload->display_errors();
                header('Content-Type: application/json');
                echo json_encode(array('error' => array($key1 => $upload_error)));
                return;
            }
            $uploadedImage = $this->upload->data();
            $postData['artwork_' . $key1] = $uploadedImage['file_name'];
        }

        $postData['artwork_creative_rationale'] = nl2br(htmlentities($postData['artwork_creative_rationale'], ENT_QUOTES, 'UTF-8'));
        $postData['participant_home_address'] = nl2br(htmlentities($postData['participant_home_address'], ENT_QUOTES, 'UTF-8'));
        $postData['participant_contact_no'] = $postData['participant_contact_no_prefix'] . $postData['participant_contact_no'];
        // Remove column that must be exclude during insert into database
        unset($postData['participant_contact_no_prefix']);
        unset($postData['agree1']);
        unset($postData['agree2']);
        unset($postData['agree3']);


        if (isset($postData['participant_is_student'])) {
            $postData['participant_is_student'] = '1';
        } else {
            unset($postData['participant_college_id']);
            $postData['participant_college_name'] = '';
        }

        // Insert Artwork Submission into database. If success return insert id (int)
        $insertStatus = $this->Artwork_model->insert($postData);

        if (isset($postData['participant_is_student'])) {
            if ($postData['participant_college_id'] == '43') {
                $postData['participant_college_name_for_email'] = $postData['participant_college_name'];
            } else {
                $postData['participant_college_name_for_email'] = reset($this->college_model->where('id',
                    $postData['participant_college_id'])->fields('name')->get());
            }
        }

        $postData['artwork_category_name'] = reset($this->Artwork_category_model->where('id', $postData['artwork_category_id'])->fields('name')->get());

        if (is_int($insertStatus)) {
            // Send Email
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'outlook.office365.com';
            $config['smtp_port'] = '587';
            $config['smtp_user'] = 'noreply@nandos.com.my';
            $config['smtp_pass'] = 'MediaCliq@1';
            $config['smtp_timeout'] = '60';
            $config['mailtype'] = 'html';
            $config['smtp_crypto'] = 'tls';
            $config['newline']="\r\n";
            $config['crlf'] = "\r\n";
            $config['charset'] = 'utf-8';

            $this->email->initialize($config);
            $this->email->from('noreply@nandos.com.my', 'Nandos');
            $this->email->to($postData['participant_email']);
            $this->email->subject('Thank you for participating in Nando’s Art Initiative');
            $mailContent = $this->load->view('edm/edm', $postData, TRUE);
            $this->email->message($mailContent);
            $this->email->send();
            //log_message('debug', $this->email->print_debugger());
            //dd($this->email->print_debugger());

            header('Content-Type: application/json');
            echo json_encode(array(
                'success' => array(
                'message' => 'Successfuly submitted artwork!',
                'id' => $insertStatus,
            )));
            return;
        }
    }

    /**
     * Callback function to validate artwork_category_id required
     *
     * @return bool
     */
    public function artwork_category_id_check()
    {
        if ($this->input->post('artwork_category_id')) {
            return true;
        } else {
            $error = 'Please choose one of the artwork category above.';
            $this->form_validation->set_message('artwork_category_id_check', $error);
            return false;
        }
    }

    /**
     * Callback function to validate gender id required
     *
     * @return bool
     */
    public function gender_id_check()
    {
        if ($this->input->post('participant_gender_id')) {
            return true;
        } else {
            $error = 'Please choose one of the gender above.';
            $this->form_validation->set_message('gender_id_check', $error);
            return false;
        }
    }

    public function email()
    {
        $this->load->view('edm/edm');
    }


}