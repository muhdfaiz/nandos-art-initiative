<div class="wrapper">

    <?php echo $template['partials']['header']; ?>

    <div class="mainContent">
        <div class="faq">
            <div class="faqContent">
                <h1>FAQ</h1>
                <div class="faqSection"> <a class="faqTitle" id="#general"> General <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="test">
                        <div class="faqExpand" id="general">
                            <div class="faqQuestion">
                                <div class="faqIndicator"> Q: </div>
                                <div class="faqQuestionContent"> How can I participate? And if I have participated in and/or received a prize in a previous Nando’s or other art competition, am I still eligible to participate? </div>
                            </div>
                            <div class="faqAnswer">
                                <div class="faqIndicator"> A: </div>
                                <div class="faqQuestionContent"> The competition is open to all Malaysian citizens and permanent residents who reside in Malaysia aged from 18 – 29 years old. You can participate the Competition as long as you are not an Established artists who:
                                    <ol>
                                        <li>Is a past grand prize winner of the Competition</li>
                                        <li>Is a full time artist</li>
                                        <li>Is / has been represented by a gallery</li>
                                        <li>Had a solo exhibition of their work</li>
                                    </ol>
                                    You can click <a href="<?php echo base_url() . 'terms_and_conditions'; ?>">here</a> to view the details. </div>
                            </div>
                            <div class="faqQuestion">
                                <div class="faqIndicator"> Q: </div>
                                <div class="faqQuestionContent"> Can I participate in all 2 categories? </div>
                            </div>

                            <div class="faqAnswer">
                                <div class="faqIndicator"> A: </div>
                                <div class="faqQuestionContent"> Yes, you can. But this is limited to only one entry per category. Your work must be original, unpublished and produced by you. If you submit more than one entry in a category, you will be disqualified. </div>
                            </div>

                            <div class="faqQuestion">
                                <div class="faqIndicator"> Q: </div>
                                <div class="faqQuestionContent"> What is the theme of the art competition? </div>
                            </div>

                            <div class="faqAnswer">
                                <div class="faqIndicator"> A: </div>
                                <div class="faqQuestionContent"> The theme is <b>“Your Art. Your Story”</b>. It is an open theme which is up to your own interpretation to tell the story that you want to your audience via your artwork </div>
                            </div>

                            <div class="faqQuestion">
                                <div class="faqIndicator"> Q: </div>
                                <div class="faqQuestionContent"> Is there a requirement that I need to include Nando’s related image (eg: chicken, chilli, PERi-PERi bottle sauce, Nando’s logo etc) in submitted artwork? </div>
                            </div>
                            <div class="faqAnswer">
                                <div class="faqIndicator"> A: </div>
                                <div class="faqQuestionContent"> The theme is <strong><u>not</u></strong> tied to Nando’s. It is independent of Nando’s and completely open to your interpretation. We strongly encourage you to exercise your creativity and not feel as if there is a need to incorporate chickens, chilies, PERi-PERi sauce bottles or the Nando’s brand in your entry. Besides, we do not award bonus points for it. </div>
                            </div>
                            <div class="faqQuestion">
                                <div class="faqIndicator"> Q: </div>
                                <div class="faqQuestionContent"> How will my entry be judged? </div>
                            </div>
                            <div class="faqAnswer">
                                <div class="faqIndicator"> A: </div>
                                <div class="faqQuestionContent"> All entries will be judged based on the following criteria:
                                    <ol>
                                        <li>Originality and creativity</li>
                                        <li>The Story –  concept and ideas</li>
                                        <li>Quality of design & composition</li>
                                        <li>Use of medium / artistic finish</li>
                                        <li>Overall impression</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#fineArt"> Fine Art Category <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="fineArt">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> Are there any restrictions on the medium? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> The medium is open. Acrylics, ink, oil painting, collage, silkscreen, wood, canvas, cloth, mixed media and others are all permissible.<br />
                                <br />
                                The only requirement is that your finished entry must be in a 2-dimensional hanging display form. The use of perishable materials is also discouraged. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> Is photography a permitted medium? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> No. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What are the required dimensions of my entry? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> A minimum of 2’ x 2’ or 3’ x 3’; and maximum of 3’ x 5’, in any shape or form. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#digitalArt"> Digital Art Category <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="digitalArt">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> Are there any restrictions on the medium? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> Your entry must be created by a digital art software tool which can include digital painting, vector drawings, 3D graphics and others. The final output of your entry must be in a 2-dimensional hanging display form. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What are the required dimensions of my entry? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> A minimum of 2’ x 2’ or 3’ x 3’; and maximum of 3’ x 5’, in any shape or form. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#onlineSubmission"> Online Submission of Entries <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="onlineSubmission">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> How can I submit my entries? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> All submissions are online only at <a href="http://www.nandos.com.my/artinitiative" target="_blank">www.nandos.com.my/artinitiative</a>. No physical submissions will be accepted. All entries must be uploaded on or before <strong>31st May 2016 (Tuesday)</strong>. Entries received after this will immediately be disqualified. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What if there are finer details in my entry that may not be visible in the photograph that I upload? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> You may take close up photographs of these details and upload a maximum of 2 images. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> In the online entry form for online submission, does the rationale behind my entry have to be in English? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> It can be in English or Bahasa Malaysia. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#prizes"> The Prizes <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="prizes">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What are the Prizes of the competition? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> You can click <a href="<?php echo base_url() . '#prizes'; ?>">here</a> to view the details. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#shortlistedEntries"> Shortlisted Entries <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="shortlistedEntries">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> How will I know whether my entry has been shortlisted? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> If your entry is shortlisted, you will be contacted via telephone, e-mail and / or SMS on or <strong>before 6th July 2016 (Wednesday)</strong>. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What happens after my entry been shortlisted? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> Once shortlisted, you must submit your physical entry for the final judging. You must label your entry (on the reverse side) with your full name (as per NRIC), contact number, artwork title and artwork rationale.<br />
                                <br />
                                Shortlisted entries must be sent to:<br />
                                <br />
                                <strong>Event Plus (M) Sdn Bhd</strong><br />
                                43, Jalan Kemuning Prima D33/D,<br />
                                Kemuning Utama Commercial Centre,<br />
                                40400 Shah Alam, Selangor.<br />
                                Contact Person: <strong>Ace (018 - 962 3868)</strong><br />
                                Office hours (i.e. 9am – 6pm) from Monday to Friday, excluding Public Holidays.<br />
                                <br />
                                OR<br />
                                <br />
                                <strong>LOT 123 @ Nando’s Chinatown</strong> <br />
                                No.123, 2nd & 3rd floor,<br />
                                Jalan Sultan,50000 Kuala Lumpur<br />
                                Contact Person: <strong>Ace (018 - 962 3868)</strong><br />
                                Office hours (i.e. 10am – 6pm) from Monday to Friday, excluding Public Holidays. <br />
                                Entries must reach on or before <strong>24th July 2016 (Sunday)</strong> or they will be disqualified.<br />
                            </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> Will the delivery cost be covered by Nando’s? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> No, all costs incurred from participating in this competition will be borne by the participant. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#annoucementResult"> Announcement of Results <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="annoucementResult">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> How will I know whether my entry has won or is selected to be placed on public display? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> You will be contacted via telephone, e-mail and / or SMS on or before <strong>15th August 2016 (Monday)</strong>. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What happens to my entry if I am a winner? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> The winning entries will be exhibited in LOT123, </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What if I’m one of the winners, but am unable to attend the prize-giving event? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> Your may send a representative to receive the prize on your behalf. Upon registering at the event, your representative must present:
                                <ol>
                                    <li>a copy of the e-mail informing you of the prize-giving event</li>
                                    <li>a photocopy of your NRIC as proof</li>
                                </ol>
                                If you are unable to attend, please inform us as soon as possible, so that we can make the appropriate arrangements. Contact <strong>Ace (018 - 962 3868)</strong> to let us know. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What happens to the other entries that are display for exhibition? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> These are available for the public to bid on. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#auctionArtwork"> Auction of Artworks <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="auctionArtwork">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> How does the silent auction work? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> Bidding forms will be made available on site during the exhibition period. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What happens if there is a successful bid on my entry? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> After the exhibition period is over, we will contact the highest bidders to arrange for payment and delivery of the entry. Once this is complete, you will be contacted to receive a cheque equivalent to 50% of the amount pledged by the successful bidder. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> What happens to the other 50% of the amount? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> The amount will go to the selected charity organization or the college / university of the grand prize winner. </div>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#reclaimationEntries"> Reclaimation of Entries <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="reclaimationEntries">
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> How can I reclaim my entry after the exhibition/auction has ended? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> Entries must be collected<strong> before 30th Nov 2016 (Wednesday)</strong> from <br />
                                <br />
                                <strong>Event Plus (M) Sdn Bhd</strong> <br />
                                43, Jalan Kemuning Prima D33/D, Kemuning Utama Commercial Centre, <br />
                                40400 Shah Alam, Selangor <br />
                                <br />
                                OR<br />
                                <br />
                                <strong>LOT 123 @ Nando’s Chinatown</strong> <br />
                                No.123, 2nd & 3rd floor, <br />
                                Jalan Sultan,50000 Kuala Lumpur.<br />
                                <br />
                                (Contact Person: <strong>Ace: 018 - 962 3868</strong>)<br />
                                <br />
                                If you do not reclaim your entry after this date, it will become the property of Nando’s Chickenland Malaysia Sdn Bhd. </div>
                        </div>
                        <div class="faqQuestion">
                            <div class="faqIndicator"> Q: </div>
                            <div class="faqQuestionContent"> How do I contact you if I have any questions? </div>
                        </div>
                        <div class="faqAnswer">
                            <div class="faqIndicator"> A: </div>
                            <div class="faqQuestionContent"> You can contact us at the following:<br />
                                <br />
                                Mobile:
                                <ol>
                                    <li><strong> Ace: 018 - 962 3868</strong></li>
                                    <li><strong> Gene: 016-435 2265</strong></li>
                                </ol>
                                E-mail: <a href="mailto:peri-peri@nandos.com.my" target="_top">peri-peri@nandos.com.my</a> <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="faqTexture">
                <div class="faqTextureLeft"></div>
                <div class="faqTextureRight"></div>
            </div>
        </div>
    </div>

    <?php echo $template['partials']['footer']; ?>

</div>
<script language="javascript" type="text/javascript">
    $("a.btnCloseMenu, .popUpBg").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeOut();
        enableScroll();
    });
    $("a.btnBurger").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeIn();
        disableScroll();
    });
    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }
    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }
    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
    $(".faqSection a.faqTitle").click(function(event){
        event.preventDefault();
        var tab = $(this).attr("id");
        $(".faqExpand").not(tab).slideUp(400);
        $(tab).slideToggle(function(){
            var mainMenu = $(".naviContent").height();
            $('html, body').animate({
                scrollTop: $(this).offset().top - mainMenu - 30
            }, 400);
        });
        $(".faqSection a.faqTitle span").removeClass("arrowUp");
        $(this).find("span").addClass("arrowUp");

    });
</script>