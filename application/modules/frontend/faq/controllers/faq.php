<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends MY_Controller
{

    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
    }

    public function index()
    {
        $this->template->title("Nando's Art Initiative - FAQ");
        $this->template->set_partial('header', 'partial/frontend/header')->set_partial('footer', 'partial/frontend/footer')
            ->set_layout('frontend')->build('faq');
    }

}