<div class="wrapper">

    <?php echo $template['partials']['header']; ?>

    <div class="subLandingMenu" id="stuff">
        <div class="subNavi">
            <ul>
                <li><a href="#fine" class="active"><span></span>Fine Art</a></li>
                <li><a href="#digital"><span></span>Digital Art</a></li>
                <div class="clear"></div>
            </ul>
        </div>
    </div>

    <div class="mainContent">
        <!-- <br><br><br> -->
        <div class="section faq">
            <div class="faqContent resultContent first">
                <h1>Results</h1>
                <p>We would like to thank all participants who have submitted their work. There’s no doubt that talent runs through veins of our young Malaysian artists. It was a tough call, but after much deliberation, we have shortlisted the top artworks.</p>
            </div>
        </div>

        <div class="section faq" id="fine">
            <div class="faqContent resultContent">
                <br><h3 class="txt-center">Fine Art Finalists</h3><br>
                <table class="resultTable" data-result-category="fine" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Name</th>
                        <th>Artwork Title</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="clear"></div>
        </div>

        <div class="section faq" id="digital">
            <div class="faqContent resultContent">
                <br><h3 class="txt-center">Digital Art Finalists</h3><br>
                <table class="resultTable" data-result-category="digital" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Name</th>
                        <th>Artwork Title</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="clear"></div>
        </div>

        <div class="resultPattern1"></div>
        <div class="resultPattern2"></div>

    </div>

    <?php echo $template['partials']['footer']; ?>
</div>
<script>
    $(".subNavi ul li a").click(function(event) {
        event.preventDefault();
        var tab = $(this).attr("href");
        var mainMenu = $(".mainNavi").height();
        var subMenu = $(".subLandingMenu").height();
        var totalNavi = mainMenu + subMenu;
        $('html, body').animate({
            scrollTop: $(tab).offset().top - totalNavi
        }, 500)
    });

    var sections = $('.section'),
        nav = $('.subNavi'),
        mainNavi = $('.mainNavi').height(),
        navHeight = nav.outerHeight();

    $(window).on('scroll', function() {
        var curPos = $(this).scrollTop();

        sections.each(function() {
            var top = $(this).offset().top - navHeight - mainNavi,
                bottom = top + $(this).outerHeight();

            if (curPos >= top && curPos <= bottom) {
                nav.find('a').removeClass('active');
                sections.removeClass('active');


                $(this).addClass('active');
                nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');

            } else if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                $(".subNavi ul li a").removeClass('active');
                nav.find('a[href="#criteria"]').addClass('active');
            }
        });
    });

    // 20160713 - input data winner list

    $(document).ready(function(){

        $('table[data-result-category]').each(function(){
            category = $(this).data('result-category');

            if( category === 'fine'){
                finalistList = fineArtFinalists;
            }else if( category === 'digital' ){
                finalistList = digitalArtFinalists;
            }

            for(i=0; i < finalistList.length; i++){

                template = '<tr><td>' + finalistList[i][0] + '</td><td>' + finalistList[i][1] + '</td><td>' + finalistList[i][2] + '</td></tr>'
                $(this).find('tbody').append(template);

            }

        });

    })

    fineArtFinalists = [
        [1,"Abdul Syukur Bin Haji Abdul Rani","Survival Of The Fittest "],
        [2,"Abigail Chang Qian Yi","Raging Sea"],
        [3,"Abu Ubaidah Amir Ibrahim","Wajah"],
        [4,"Adi Harire Mohamad Safri","Home And Away"],
        [5,"Ahmad Awis Bin Kamaruzaman","Ruang"],
        [6,"Ahmad Fikril Haniff Bin Shahruddin","The Dyslexic Mimiking"],
        [7,"Akhmal Asyraf Bin Azri","Viral   "],
        [8,"Alexis Lyndy Pantan Anak Siman","Food For Thought"],
        [9,"Alia Suzaireen Binti Md Suhaimi","Disappearing Woman"],
        [10,"Brandon Anak Simon Scott","11 Am, Stesen Jelatek (Alla Prima)"],
        [11,"Chang Chee Siong","One Lazy Sunday, At The Garden"],
        [12,"Chok Ming Hoong","Teenagers"],
        [13,"Chok Yue Zan","In Memory, In Reality "],
        [14,"Choo Ai Xin","Playground?"],
        [15,"Chu Chia Chen","Fake Myself"],
        [16,"Chuah Phaik Hoon","The Lost Of Generation"],
        [17,"Haz Yusup","The Man Who Reads"],
        [18,"Huan Jia Jin","Awareness"],
        [19,"Huzairy Bin Mohammad","The Gift Of Memory"],
        [20,"Imanordin Bin Mohd Shah","Jendela 7 (Tengkujuh Telah Berlalu)"],
        [21,"Kallai Vani Ramani","Violence Against Women"],
        [22,"Koh Hooi San","Study Of Self"],
        [23,"Kong Shue Man","Life"],
        [24,"Lee Yok Qian","Fate"],
        [25,"Lem Tee Sin","Borderless"],
        [26,"Lim Jun Hui","Floating Heritage "],
        [27,"Lim Si Ting","To My Beloved"],
        [28,"Low Kar Lai","Faceless"],
        [29,"Lukman Hakim Bin Mohd Bahari ","Relaxing Old Man With His Cigars "],
        [30,"Mohamad Firdaus Bin Md Yusof","Punca Rezeki"],
        [31,"Mohamad Isa Bin Mohammad Ishak","Ditelani Zaman"],
        [32,"Mohammad Aiman Adha Bin Azizul Rozany","Animal Show"],
        [33,"Mohammad Haziq Syawal Bin Mohd Hanis","The Symbols Of The Past"],
        [34,"Mohd Marwan Bin Jauhari","Destroying Nature"],
        [35,"Mohd Rushdi Bin Ahmad","Babysitter"],
        [36,"Muhamad Effi Syafiq Bin Jusoh","Sempadan"],
        [37,"Muhamad Nabil Bin Adnan","Biggest Apologize"],
        [38,"Muhammad Akif Azmi ","Beauty Is Pain"],
        [39,"Muhammad Amirul Bin Roslan","Nature Destruction"],
        [40,"Muhammad Azrul Fahmi Bin Azman","Living Things 'Rakus'"],
        [41,"Muhammad Fareez Bin Mohd Fauzi","Equivalence"],
        [42,"Muhammad Hafiz Bin Ab Gapor","Emotion III"],
        [43,"Muhammad Nur Faisa Bin Azmi","Kaulah Puncanya II"],
        [44,"Muhammad Nur Syafiq Bin Nizam Shah","Taxidermy Of Memories"],
        [45,"Muhammad Salehuddin Bin Zakaria","Libertarian Series 1"],
        [46,"Muhammad Shahied Bin Shariff","Smartphone Obsession"],
        [47,"Muhammad Suhaimi Bin Ali","Saudagar Senja"],
        [48,"Muhammad Syafiq Bin Kholid","Angan-Angan Si Anak Jantan"],
        [49,"Ng Ee Ling","The Hand"],
        [50,"Nik Mohd Shahfiz Bin Nik Shairozi","Yakbokte! (Latah Mak Peah)"],
        [51,"Norizatteey Binti Adam","Setiap Sekarang "],
        [52,"Nur Amira Binti Hanafi","Bacteria Art: Self-Portrait"],
        [53,"Nur Atiqah Binti Khairul Anuar","Unconditional Love"],
        [54,"Nur Farhan Nabila Binti Zakaria","Bond"],
        [55,"Nurhaliza Binti Rosli","Icon"],
        [56,"Nurhidayat Bin Arshad","Maze Of Life"],
        [57,"Ong Ka Wern","The Best Taste From Tradisional"],
        [58,"Poh Chern Bin","Inner Self"],
        [59,"Raden Hisbullah Bin Radin Abu Bakar","Why The World Is Too Hot?"],
        [60,"Satish  Manoharan","Renagade Coffee War"],
        [61,"Sim Li Mei","Fishing Village"],
        [62,"Siti Khairunnisaq Binti Abd Muin","Alunan Bernyawa Sisipan Dunia"],
        [63,"Siti Nor Hidayah Bt Mohamad Mokhtar","Drama Tragedi"],
        [64,"Suriati Binti Suaimi","Coke Parade"],
        [65,"Syamsol Azhar Bin Zulkafli","Georgetown In Action"],
        [66,"Syarifah Nadhirah Binti Haji Syed Abdul Rahman","Beneath The Still Waters Of Societal Hieroglyphs"],
        [67,"Tan Bon Peng","The Promise Between Me And Myself"],
        [68,"Tan Ghee Chiao","Chicken Talk"],
        [69,"Tang Li Yi","My Life On Repeat , An Artist As Like An Astronaut"],
        [70,"Teh Hung Lih","Me Crown Me"],
        [71,"Teh Nadirah Binti Hamdan","The Train Trip"],
        [72,"Victor Fok","The Wanderer"],
        [73,"Wan Amalia Ilyana Binti Wan Muda","Proud Old Man"],
        [74,"Wan Amy Nazira Binti Abu Bakar","Artist"],
        [75,"Wan Yin Khei","Borneo Dream"],
        [76,"Wong Ming Hao","Intrinsic Sentiment"]
    ]

    digitalArtFinalists = [
        [1,"Ahmad Miqdat Bin Lokman","20 Sen"],
        [2,"Aina Munirah Binti Abdul Rahim","Butterfly Soul"],
        [3,"Ainul Syahirah Binti Amzan ","The Memories"],
        [4,"Amirul Naim Bin Mohd Sokhin Azmi","The Tears Of Sorrowful Life"],
        [5,"Chan Hsieh Hua","Childhood Memory"],
        [6,"Chee Kok Seang","A Pianist"],
        [7,"Chee Sze Yin","Perpetual Motion Machine"],
        [8,"Chen Pei Lin","Wait"],
        [9,"Danial Saadon","Energy Mindful Cat"],
        [10,"Ephrem Sangeeth Rokk","Popeye Pop Art"],
        [11,"Fadli Bin Ahmad Alawi","Memories"],
        [12,"Fazuliana Binti Ahmad Azuwaniza","I Love Japan"],
        [13,"Foo Chuan Choong","Stolen Attention"],
        [14,"Izuan Bin Idris","Instagram 101"],
        [15,"Karenveenajit Kaur ","Don't Call Me Plus Size!"],
        [16,"Katrin Yeoh Huay Fen","Old Trades"],
        [17,"Kee Boon Syuen","Blossom"],
        [18,"Khalida Liyana Binti Khalid","The Descendant"],
        [19,"Law Pei Yong","Sound Of The Sea"],
        [20,"Law Wai Hong Dornam","Beyond The Screen"],
        [21,"Lee Chee Wan","Floating World"],
        [22,"Lee Chin Chian","I Believe I Can Fly"],
        [23,"Lee Poh Li","Snow In Penang"],
        [24,"Lim Chee Leng","Walk Alone Under The Starry Night"],
        [25,"Lim Chun Yie","Wonderful Egg"],
        [26,"Lim Jee Teng","The Trishaw Rider"],
        [27,"Lim Kah Yin","The Afternoon"],
        [28,"Lim Tian Jing","Water Pollution Awareness Again"],
        [29,"Lim Yi Wan","The Story Of Tears"],
        [30,"Loh Poh Yi","Inner Beauty"],
        [31,"Low Shi Jian","Catching Shadow"],
        [32,"Low Sun Wing","Kobe Bryant"],
        [33,"Mak Mun Choon","Sora - Sky Village"],
        [34,"Mohd Rozali Hussain ","Japanese Girl Autumn "],
        [35,"Mohd Zulfadhly Bin Luey","Within Me"],
        [36,"Mohd Zulkarnain Bin Mohd Halil","Abstract Wild"],
        [37,"Muhamad Syahrin Bin Mohd Nazri","Some Comfort Lies In A Mind Of Someone Living "],
        [38,"Muhammad Aimman Bin Mohd Hafizal","S.W.A.Y?"],
        [39,"Muhammad Anas Bin Zakaria","Behind The Picture"],
        [40,"Muhammad Luqman Bin Muhaiyuddin","Art Of Chaos And Arrangement  "],
        [41,"Muhammad Muzammil Bin Mukhtar","Kau Berangan Jelah"],
        [42,"Naim Bin Harith Saiful Bahrin","Aku Mahu Ke Langit"],
        [43,"Ng Jierick","What Are Human Doing?"],
        [44,"Nik Zulhilmi Afiq Bin Nik Amran","The End Of Nature"],
        [45,"Norizan Binti Abd Karim","Intangible Cultural Heritage"],
        [46,"Nurul Safi Izzati Binti Jon","Double Exposure"],
        [47,"Olaf Yen","Count"],
        [48,"Qistina Khalidah Binti Shaikh Yahaya","The Fool"],
        [49,"Seow Jing Ying","Voice Of Nature"],
        [50,"Siti Aishah Binti Zaiton","Tinggalan"],
        [51,"Soh Hui Xiang","Class Of 2016"],
        [52,"Soo Jo Vin","My Earth My Home"],
        [53,"Syed Muhammad Iftah Bin Syed Abdillah ","Inside The Box"],
        [54,"Tan Howe Qin","Shadow"],
        [55,"Tan Sheau Ping","Life's Perspective"],
        [56,"Tay Yi Tong","Hope"],
        [57,"Teow Hai Way","What's Goin On In My Brain"],
        [58,"Tong Xin Hua","Trapped"],
        [59,"Wong Suet Ting","I Thinks I Still Can Save 'Her'...!"],
        [60,"Wong Tuck Wai","Forgotten Friends"],
        [61,"Yau Boon Han","Jungle Boy"],
        [62,"Yong Yeat Fhan","The Amber Night"],
        [63,"Yu Shi Jing","Green Part Of The Earth"],
        [64,"Zahirudin Bin Ali","Well Behave"]
    ]
</script>