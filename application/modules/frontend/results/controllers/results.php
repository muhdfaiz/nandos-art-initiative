<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Results extends MY_Controller
{

    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display Terms & Conditions Page
     */
    public function index()
    {
        $this->template->set_partial('header', 'partial/frontend/header')->set_partial('footer', 'partial/frontend/footer')
            ->set_layout('frontend')->build('results');
    }

}