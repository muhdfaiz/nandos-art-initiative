<div class="wrapper">

    <?php echo $template['partials']['header']; ?>

    <div class="mainContent">
        <div class="faq">
            <div class="faqContent">
                <h1>Terms and Conditions</h1>
                <div class="faqSection"> <a class="faqTitle" id="#general"> 1. General <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="test">
                        <div class="faqExpand" id="general">
                            <ol class="alphabet">
                                <li>The Nando’s Art Initiative 2016 (“the Competition”) is open to all Malaysians citizens and Permanent Residents of Malaysia aged between 18 and 29 years old excluding the employees of Nando’s Chickenland Malaysia Sdn Bhd and their immediate family members, its promotion agencies, advertising agencies, PR agencies and any associate companies.</li>
                                <li>Established artist who meets any of the following criteria is not allowed to participate in the Competition:-
                                    <ol class="roman">
                                        <li>Is a past grand prize winner of the Competition</li>
                                        <li>Is a full time artist</li>
                                        <li>Is / has been represented by a gallery</li>
                                        <li>Had a solo exhibition of their work</li>
                                    </ol>
                                </li>
                                <li>This Competition is held in Malaysia only.</li>
                                <li>Each participant is eligible to submit their entry in both of the categories: Fine Art and Digital Art category.</li>
                                <li>Each participant shall be allowed to submit ONE entry per category.</li>
                                <li>The artwork(s) submitted by the Participants must be originally produced by the Participants and must be unpublished or has never been submitted to any other
                                    competitions or events including but not limited to any of the competitions organised by Nando’s Chickenland Malaysia Sdn Bhd.</li>
                                <li>Any participant found to have submitted more than one entry per category shall be disqualified.</li>
                                <li>All artwork will be judged based on the following criteria and each criteria will carry 20%:
                                    <ol class="roman">
                                        <li>Originality and creativity</li>
                                        <li>The Story –  concept and ideas</li>
                                        <li>Quality of design and composition</li>
                                        <li>Use of medium / Artistic finish</li>
                                        <li>Overall impression</li>
                                    </ol>
                                </li>
                                <li>Each participant is entitled to win one prize only per category.</li>
                                <li>The use of Nando’s Chickenland Malaysia Sdn Bhd’s trademarks including but not limited to the logos, symbols, icons, designs and images are not permitted.</li>
                                <li>All costs incurred in participating in this Competition (including the framing of the artworks) shall be at the sole expense of the participants.</li>
                                <li>Nando’s Chickenland Malaysia Sdn Bhd reserves the right to extend any date specify for the Competition contain herein and the decision will be made at the sole discretion of Nando’s Chickenland Malaysia Sdn Bhd.</li>
                                <li>Nando’s Chickenland Malaysia Sdn Bhd reserves the right to change, suspend or vary the Competition’s Terms and Conditions contain herein at any time, wholly
                                    or in part by way of posting such changes on Nando’s official website or in any other way deemed suitable by Nando’s Chickenland Malaysia Sdn Bhd without prior notice to the participants and the decision
                                    discretion of Nando’s Chickenland Malaysia Sdn Bhd.</li>
                                <li>By participating this Competition, the participants agree to be bound by the Competition terms and conditions contain herein (“Terms and Conditions”) and to the decisions of Nando’s Chickenland Malaysia Sdn Bhd shall be final and binding in all respects.</li>
                                <li>The Terms and Conditions shall be governed by laws of Malaysia and the participants hereby agree to submit to the non-exclusive jurisdiction of the courts in Malaysia and consent to service of process by mail or in any other manner permitted by the laws of Malaysia.</li>
                                <li>A waiver (whether expressed or implied) by Nando’s Chickenland Malaysia Sdn Bhd of any of these Terms and Conditions or of any breach of or default by a participant in performing any of its obligations under these Terms and Conditions shall not constitute a continuing waiver and that waiver shall not prevent Nando’s Chickenland Malaysia Sdn Bhd from acting on any subsequent breach of or default by the participants under these Terms and Conditions.</li>
                                <li>Nando’s Chickenland Malaysia Sdn Bhd shall not be responsible nor shall accept any liabilities of any nature and however arising or suffered by the participants and/or any third parties resulting directly or indirectly from the conduct of the participants to participate in this Competition.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#fineArt">2.  Fine Art Category <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="fineArt">
                        <ol class="alphabet">
                            <li>Only individual shall be allowed to participate in this category.</li>
                            <li>All artwork must capture and express the story, concepts and ideas which shall be determined by Nando’s Chickenland Malaysia Sdn Bhd in its sole discretion.</li>
                            <li>The maximum size is 3feet x 5feet and the minimum size is 2feet x 2feet or 3 feet x 3feet. There is no specific requirement in the orientation or shape of a participant’s artwork.</li>
                            <li>The medium of the fine art is open (acrylics, collage, ink, mixed media, wood, canvas, cloth, metal & etc.), provided that the finished artwork must be in a hanging 2 dimensional form. Participants should refrain from using perishable materials in their artwork.</li>
                            <li>Photography will not be accepted as an artwork under this category and such entry will not be entertained by Nando’s Chickenland Malaysia Sdn Bhd.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#digitalArt"> 3. Digital Art Category <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="digitalArt">
                        <ol class="alphabet">
                            <li>Only individual shall be allowed to participate in this category.</li>
                            <li>All artwork must capture and express the story, concepts and ideas which shall be determined by Nando’s Chickenland Malaysia Sdn Bhd in its sole discretion.</li>
                            <li>The participant’s artwork must be able to produce a physical piece for 2D display with the size of maximum 3feet x 5feet and the minimum size is 2feet x 2feet or 3 feet x 3feet.</li>
                            <li>Artwork must be produced using digital art software tool as the design and production tools.</li>
                            <li>Photography will not be accepted as an artwork under this category and such entry will not be entertained by Nando’s Chickenland Malaysia Sdn Bhd.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#prizes"> 4. The Prizes <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="prizes">
                        <ol class="alphabet">
                            <li>The prizes for both categories are as follows:</li>
                        </ol>
                        <div class="prizeTable">
                            <div class="prizeContent firstBox">
                                <strong>FINE ART CATEGORY</strong>
                                <u>1 x Grand Prize:</u>
                                <ul>
                                    <li>Solo exhibition</li>
                                    <li>Mentoring</li>
                                    <li>RM5,000 cash</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                                <u>1 x 1st Runner Up:</u>
                                <ul>
                                    <li>Group Exhibition</li>
                                    <li>RM3,000 cash</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                                <u>1 x 2nd Runner Up:</u>
                                <ul>
                                    <li>Group Exhibition</li>
                                    <li>RM2,000 cash</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                                <u>3 x Consolation Prizes:</u>
                                <ul>
                                    <li>Group Exhibition</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                            </div>
                            <div class="prizeContent">
                                <strong>DIGITAL ART CATEGORY</strong>
                                <u>1 x Grand Prize:</u>
                                <ul>
                                    <li>Solo exhibition</li>
                                    <li>Mentoring</li>
                                    <li>RM5,000 cash</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                                <u>1 x 1st Runner Up:</u>
                                <ul>
                                    <li>Group Exhibition</li>
                                    <li>RM3,000 cash</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                                <u>1 x 2nd Runner Up:</u>
                                <ul>
                                    <li>Group Exhibition</li>
                                    <li>RM2,000 cash</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                                <u>3 x Consolation Prizes:</u>
                                <ul>
                                    <li>Group Exhibition</li>
                                    <li>RM500 worth of Nando's vouchers</li>
                                </ul>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#onlineSubmission"> 5. Online Submission of Entries <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="onlineSubmission">
                        <ol class="alphabet">
                            <li>All artwork must be submitted through online submission only.</li>
                            <li>No physical submission will be allowed.</li>
                            <li>All artwork must be uploaded on or <strong>before 11.59pm, 31st May 2016 (Tuesday)</strong> at: <a href="http://www.nandos.com.my/artinitiative/" target="_blank">www.nandos.com.my/artinitiative</a>. Artwork received after 31st May 2016 will be disqualified.</li>
                            <li>Participants must accurately complete their details in the e-form and upload at least a photograph of their artwork. <strong>Participants may upload up to maximum of
                                    2 photographs</strong> of their artwork to highlight close-up details of their entry. Nando’s Chickenland Malaysia Sdn Bhd will not be liable for any consequences due to the mistake and omission by the participants in submitting their details in the e-form.</li>
                            <li>Each Photograph <strong>shall not be larger than 2MB</strong>. Permitted file formats are jpeg, gif and png only.</li>
                            <li>Participants must make sure the finished artwork match with the submitted photograph.</li>
                            <li>For Fine Art category, the photograph(s) uploaded must shows/reflects the actual conditions of the artwork, any photograph(s) that are found to have been digitally altered or modified in any manner will be disqualified.</li>
                            <li>For Digital Art category, the picture of the artwork must be final and complete. If the artwork is different from the photographs submitted, Nando’s Chickenland Malaysia Sdn Bhd reserve the right to disqualify the participants based on their sole discretion.</li>
                            <li>Participants must accurately fill in their name in the e-form as per NRIC.</li>
                            <li>The artwork rationale / write up of the artwork may be submitted in English or Bahasa Malaysia.</li>
                            <li>By submitting the artwork, participants agree to accept and to be bound by the Terms and Conditions of this Competition.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#shortlistedEntries"> 6. Shortlisted Artwork <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="shortlistedEntries">
                        <ol class="alphabet">
                            <li>If your artwork is shortlisted for the final judging, you will be notified by telephone, e-mail and/or SMS <strong>on or before 6th July 2016 (Wednesday)</strong>.</li>
                            <li>The shortlisted participants are required to submit the physical copy of the artwork <strong>before or by 5pm, 24th July 2016 (Sunday)</strong>. Any shortlisted artwork received after this date will be automatically disqualified.</li>
                            <li>The artwork must be submitted to: <br /><br />
                                <strong>Event Plus (M) Sdn Bhd</strong><br />
                                43, Jalan Kemuning Prima D33/D,<br />
                                Kemuning Utama Commercial Centre,<br />
                                40400 Shah Alam, Selangor.<br />
                                Contact Person:  <strong>Ace (018 - 962 3868)</strong><br /><br />
                                OR<br /><br />
                                <strong> LOT 123 @ Nando’s Chinatown</strong><br />
                                No.123, 2nd & 3rd floor,<br />
                                Jalan Sultan,50000 Kuala Lumpur<br />
                                Contact Person:  <strong>Ace (018 - 962 3868)</strong><br /><br />
                            </li>
                            <li>Nando’s will not be liable in any manner to the participants should any artwork be lost, missing or damaged if the artwork is submitted by post.</li>
                            <li>Every artwork must be accompanied by the information form attached to the top right-hand corner of the back of the artwork with the info of:
                                <ol class="roman">
                                    <li>Participant’s full name and identity card number (as per NRIC)</li>
                                    <li>Original artwork title</li>
                                    <li>Concise artwork rationale in English or Bahasa Malaysia clarifying the participant’s artwork story, idea or concept</li>
                                    <li>Contact details: contact no., address, e-mail address etc.</li>
                                </ol>
                            </li>
                            <li>All costs incurred in transporting of the shortlisted artwork shall be at the sole expense of the participants.</li>
                            <li>By submitting the artwork, the participants agree to accept and to be bound by the Terms and Conditions of this Competition.</li>
                            <li>All the shortlisted artworks are belong to Nando’s Chickenland Malaysia Sdn Bhd and Nando’s Chickenland Malaysian Sdn Bhd reserves the right to exhibit and sell the shortlisted artworks during exhibition.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#resultCompetition"> 7. Result of the Competition <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="resultCompetition">
                        <ol class="alphabet">
                            <li>The artworks will be judged by a panel of local and/or overseas artists/art connoisseurs/art critics/curators or other creative professionals appointed at the absolute discretion of Nando’s Chickenland Malaysia Sdn Bhd. The decision of the judges is final and conclusive. No correspondence will be entered into on this subject before, during, and/or after the Competition.</li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd reserves the right not to award any of the stipulated prizes should artwork fail, in the judges’ opinion, to meet the required standards.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#annoucementResult"> 8. Announcement of Results <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="annoucementResult">
                        <ol class="alphabet">
                            <li>The winners will be selected by a panel of appointed judges and will be notified through telephone, e-mail and/or SMS on <strong>or before 15th August 2016 (Monday)</strong>. The judges’ decisions are final and no appeal or correspondence will be entertained.</li>
                            <li>All awards will be presented to all winners at an award ceremony in Kuala Lumpur and the winners will be notified on the venue, date and time by phone on <strong>15th August 2016 (Monday)</strong>.</li>
                            <li>Awards, certificates and prizes must be collected personally at <strong>Event Plus (M) Sdn Bhd</strong> -  43, Jalan Kemuning Prima D33/D, Kemuning Utama Commercial Centre, 40400 Shah Alam, Selangor <strong>(Contact Person:  Ace 018 - 962 3868)</strong> during office hours (i.e. 9am – 6pm) from Monday to Friday, excluding Public Holidays.</li>
                            <li>The cost of travelling will be borne by the winners / participants.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#exhibitionWinningArt"> 9. Exhibition of Winning Artwork <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="exhibitionWinningArt">
                        <ol class="alphabet">
                            <li>All winning artworks will be exhibited at the <strong>9TH Nando’s Art Initiative Exhibition</strong> (“Exhibition”). The venue and the dates of the Exhibition will be confirmed later by Nando’s Chickenland Malaysia Sdn Bhd.</li>
                            <li>Winning artworks will become the property of Nando’s Chickenland Malaysia Sdn Bhd. Nando’s Chickenland Malaysia Sdn Bhd reserves the right to promote all winners and their artworks for its advertising and promotional activities without further reference to the winners and without obtaining any other further consent or making any payment whatsoever to the winners for publicity,
                                advertising, trade or promotion purposes in any media except in the case of payment to the winner(s) pursuant to the sale of artwork.</li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd reserves the right to exhibit or reproduce any winning artwork in whatever way deemed fit without prior notice, consent or payment to the winners except in the case of payment to the winners pursuant to any sale of any artwork.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#auctionArtwork"> 10. Auction of Artworks <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="auctionArtwork">
                        <ol class="alphabet">
                            <li>The participants hereby agree to allow Nando’s Chickenland Malaysia Sdn Bhd to auction their artworks for the purpose more particularly defined in the clause 11(b) herein.</li>
                            <li>If there is any party interested in purchasing a participant’s artwork <strong>during the Exhibition period</strong>, upon the successful conclusion of the sale and payment of the sales proceeds:
                                <ol class="roman">
                                    <li>50% of the proceeds from the sale of the artwork will be remitted to the participant.; and</li>
                                    <li>50% of the proceeds from the sale of the artwork will be remitted to the selected charity organization or the winning college/university of the grand prize winner.</li>
                                </ol>
                            </li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd reserves the right to sell the exhibited shortlisted artworks at such price as Nando’s Chickenland Malaysia Sdn Bhd may determine in its sole discretion.</li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd reserves the right to decide any artwork by any of the participant to be exhibited during the Exhibition.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#returnArtwork"> 11. Return of Artworks <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="returnArtwork">
                        <ol class="alphabet">
                            <li>The artworks of the short listed participants that were not sold during the Exhibition period or is in the possession of Nando’s Chickenland Malaysia Sdn Bhd after the Exhibition will need to collect their respective artworks at <strong>Event Plus (M) Sdn Bhd</strong> - 43, Jalan Kemuning Prima D33/D, Kemuning Utama Commercial Centre, 40400 Shah Alam, Selangor OR <strong>LOT 123 @ Nando’s Chinatown</strong> - No.123, 2nd & 3rd floor, Jalan Sultan, 50000 Kuala Lumpur. before <strong>6pm, 30th Nov 2016 (Wednesday) (Contact Person:  Ace: 018 - 962 3868)</strong>.</li>
                            <li>If the shortlisted participants fail to collect their artworks after this date, such artworks shall be deemed to be the property of Nando’s Chickenland Malaysia Sdn Bhd who shall have absolute discretion to deal with the same in any manner whatsoever as deemed fit by Nando’s Chickenland Malaysia Sdn Bhd without any reference to the shortlisted participants.</li>
                            <li>Further, the shortlisted participants will not hold Nando’s Chickenland Malaysia Sdn Bhd liable nor will the shortlisted participants claim against Nando’s Chickenland Malaysia Sdn Bhd, its employees, representatives, agents and servants for any costs, losses and expenses that may be incurred by the shortlisted participants due to the shortlisted participants’ failure to collect
                                his/her art work within the said period.</li>
                            <li>The cost of travelling will be borne by the shortlisted participants.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#exculsionLiability"> 12. Exclusion of Liability <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="exculsionLiability">
                        <ol class="alphabet">
                            <li>Nando’s Chickenland Malaysia Sdn Bhd will not be liable for any losses suffered by the participants or any third party due to the participants’ breach of any Terms and Conditions contain herein.</li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd will not be liable for any damages or losses to the artwork in any circumstances including but not limited to the following:-
                                <ol class="roman">
                                    <li>damages or losses to the artworks during the delivering of the artwork to Nando’s Chickenland Malaysia Sdn Bhd;</li>
                                    <li>damages or losses to the artworks when the artwork is in the possession of the Nando’s Chickenland Malaysia Sdn Bhd; and</li>
                                    <li>damages or losses to the artworks after the participants have collected their respective artworks.</li>
                                </ol>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#indemnity"> 13. Indemnity <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="indemnity">
                        <ol class="alphabet">
                            <li>In the event there is any claims against Nando’s Chickenland Malaysia Sdn Bhd, its agencies, representative offices, affiliated, associated or related corporations, and their respective officers, servants or agents by any individual, organisation, governmental authority or any third party due to the conduct of the participants, the participants agree to indemnify the same to Nando’s Chickenland Malaysia Sdn Bhd for any damages and losses suffered.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection"> <a class="faqTitle" id="#copyrightProperty"> 14. Copyright / Intellectual Property <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="copyrightProperty">
                        <ol class="alphabet">
                            <li>All submission materials and artwork submitted to Nando’s Chickenland Malaysia Sdn Bhd in accordance with these Terms and Conditions must be the original works produced by the participants. The participants must also be the owners of the physical, copyright and intellectual property of all the submitted materials and artwork.</li>
                            <li>All winning artwork including the documents and materials submitted/Artwork to Nando’s Chickenland Malaysia Sdn Bhd shall be the sole property of Nando’s Chickenland Malaysia Sdn Bhd. Nando’s Chickenland Malaysia Sdn Bhd reserves the right to retain all winning artworks and any supporting documentation. Participants are advised to photograph their submissions for their own records
                                prior to submission.</li>
                            <li>All winners shall grant Nando’s Chickenland Malaysia Sdn Bhd, royalty-free, exclusive license to exhibit, archive and reproduce images of their winning artwork including, but not limited to brochures, calendars, catalogues, post cards and websites or any kind of medium which in the opinion of Nando’s Chickenland Malaysia Sdn Bhd is fit for promoting the Competition or marketing
                                purposes.</li>
                            <li>Each participant undertakes to indemnify and hold Nando’s Chickenland Malaysia Sdn Bhd and its officers and agents harmless from and against any and all liability, loss, damage, costs and expenses in relation to any claim or action brought against Nando’s Chickenland Malaysia Sdn Bhd or its officers and agents arising out of a breach of this warranty by the participant.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection" id="terms_conditions"> <a class="faqTitle" id="#acceptanceTc"> 15. Acceptance of Terms and Conditions <span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="acceptanceTc">
                        <ol class="alphabet">
                            <li>Submission of artwork signifies acceptance of the Terms and Conditions of the Competition by the participants. While all reasonable care will be taken of the artwork submitted, Nando’s Chickenland Malaysia Sdn Bhd assumes no responsibility for any loss of or damage to the artwork including but not limited to the artwork, photographs and any documents submitted before, during,
                                and/or after the Competition.</li>
                        </ol>
                    </div>
                </div>
                <div class="faqSection" id="disclosure_personal_data"> <a class="faqTitle" id="#disclosure"> 16. Disclosure and Use of Personal Data<span class="trans"><img src="<?php echo asset('images/icon_faq_arrow.jpg'); ?>" /></span> </a>
                    <div class="faqExpand" id="disclosure">
                        <ol class="alphabet">
                            <li>By submitting an entry to Nando’s Chickenland Malaysia Sdn Bhd, the participants consent to Nando’s Chickenland Malaysia Sdn Bhd for disclosing the Personal Data and photographs of the participants for:
                                <ol class="roman">
                                    <li>publishing in any mass media or marketing materials for advertising or publicity purposes; and</li>
                                    <li>any other purpose in relation to the Competition which is considered by Nando’s Chickenland Malaysia Sdn Bhd to be in Nando’s interest, or the interest of any members of Nando’s (collectively “the Group”), in any manner as and when Nando’s Chickenland Malaysia Sdn Bhd deems necessary without further notice to the participants and/or the award winners.</li>
                                </ol>
                            </li>
                            <li>The participants agree and giving consent to Nando’s branches, agencies, representative offices, affiliated, associated or related corporations, and their respective officers, servants or agents, whether situated in or out of Malaysia, and the Group are permitted to process all the Personal Data submitted by the participants. The term “process” shall include but is not limited to, collecting, recording, holding, storing, using and/or disclosing.</li>
                            <li>“Personal Data” may include, but is not limited to, the participants’ name, identity card number, address, e-mail address, phone number, gender and other relevant information provided by the participants during or after the Competition.</li>
                            <li>The purpose for which Nando’s Chickenland Malaysia Sdn Bhd collect the Personal Data (“Purpose”) includes the following:
                                <ol class="roman">
                                    <li>to establish the participants’ and/or the award winners’ identity and background;</li>
                                    <li>for communications with the participants and/or the award winners on any matters</li>
                                    <li>pertaining to the Competition, including but not limited to queries on the entry form,</li>
                                    <li>details on the award ceremony and notification to the winners on the awards and prizes;</li>
                                    <li>to contact the participants and/or the award winners to discuss any future exhibitions,</li>
                                    <li>publicity events related to their artworks and other art events;</li>
                                    <li>to create a Nando’s Art Initiative alumni mailing list to update alumni members on activities relating to any future similar campaigns or competitions;</li>
                                    <li>to meet the disclosure requirements of any law binding on Nando’s Chickenland Malaysia Sdn Bhd;</li>
                                    <li>for any other purpose that is required or permitted by any law, regulations, guidelines</li>
                                    <li>and/or relevant regulatory authorities.</li>
                                </ol>
                            </li>
                            <li>The participants’ Personal Data will be collected from the information the participants have provided to Nando’s Chickenland Malaysia Sdn Bhd in the entry form and any other documents provided in relation to the Competition.</li>
                            <li>The participants need to provide Nando’s Chickenland Malaysia Sdn Bhd with the Personal Data which are requested by Nando’s Chickenland Malaysia Sdn Bhd, failing which Nando’s Chickenland Malaysia Sdn Bhd may refuse to accept the participants’ registration/entry in the Competition.</li>
                            <li>The participants and/or the award winners agree and consent that Nando’s Chickenland Malaysia Sdn Bhd may transfer their Personal Data outside of Malaysia. All Personal Data held by Nando’s and the Group will be accorded with reasonable level of protection against any loss, misuse, modification, unauthorized or accidental access or disclosure, alteration or deletion.</li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd agrees to take reasonable steps and measures to secure the safety of the Personal Data collected from the participants.</li>
                            <li>Nando’s Chickenland Malaysia Sdn Bhd agrees the Personal Data collected from the participants shall not be kept longer than is necessary for the fulfillment of the purposes stated in this Terms and Conditions.</li>
                            <li>In the event the participants wish to update, rectify or want to know what kind of Personal Data have been collected by Nando’s Chickenland Malaysia Sdn Bhd, the participants may contact this number 03-7848 7488 or e-mail to <a href="mailto:peri-peri@nandos.com.my">peri-peri@nandos.com.my</a> for further enquiry.</li>
                            <li>If any participant at any time want to know or rectify his/her Personal Data that has been stored by Nando’s Chickenland Malaysia Sdn Bhd or want to withdraw the consent given by them under this Terms and Conditions, the participant can contact Marketing Department at 03-7848 7488 for Nando’s Chickenland Malaysia Sdn Bhd further action.</li>
                        </ol>
                    </div>
                </div>

            </div>
            <div class="faqTexture">
                <div class="faqTextureLeft"></div>
                <div class="faqTextureRight"></div>
            </div>
        </div>
    </div>

    <?php echo $template['partials']['footer']; ?>

</div>
<script language="javascript" type="text/javascript">
    if(window.location.hash) {
        var tab = '#' + window.location.hash.substring(1);
        var mainMenu = $(".mainNavi").height();
        var totalNavi = mainMenu;

        var tab1 = $(tab).children(':first').attr('id');
        $(tab1).slideToggle();
        $(".faqExpand").not(tab1).slideUp();
        $(".faqSection a.faqTitle span").removeClass("arrowUp");
        $(this).find("span").addClass("arrowUp");

        $('html, body').animate({
            scrollTop: $(tab).offset().top - totalNavi
        }, 1500)
    }

    $("a.btnCloseMenu, .popUpBg").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeOut();
        enableScroll();
    });
    $("a.btnBurger").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeIn();
        disableScroll();
    });
    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }
    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }
    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
    $(".faqSection a.faqTitle").click(function(event){
        event.preventDefault();
        var tab = $(this).attr("id");
        $(".faqExpand").not(tab).slideUp(400);
        $(tab).slideToggle(function(){
            var mainMenu = $(".naviContent").height();
            $('html, body').animate({
                scrollTop: $(this).offset().top - mainMenu - 30
            }, 400);
        });
        $(".faqSection a.faqTitle span").removeClass("arrowUp");
        $(this).find("span").addClass("arrowUp");

    });
</script>