<div class="wrapper">

    <?php echo $template['partials']['header']; ?>

    <?php echo $template['partials']['navigation']; ?>

    <div class="mainContent">
        <div class="section mainHeader" id="about">
            <div class="logoMasthead"> <img src="<?php echo asset('images/logo_masthead.png'); ?>" /> </div>
            <div class="headerContent">
                <p>Contrary to popular belief, Nando's isn't just about serving great tasting PERi-PERi chickens. With roots that are steeped in African culture and heritage, we're also highly passionate about supporting local communities and providing opportunities for people to realise their potential.</p>
                <br />
                <p>The Nando's Art Initiative is our way of nurturing Malaysian talents while giving them a platform to express themselves freely, to Malaysia and even to the world! Express your thoughts and identity through art. Take charge of your own creativity and tell your story in your very own way. The freedom of expression is yours!</p>
                <div class="scrollDownIcon"> Scroll Down<br />
                    <img src="<?php echo asset('images/icon_down.png'); ?>" /> </div>
            </div>
            <div class="headerImg"> <img src="<?php echo asset('images/texture_landing_header_right.png'); ?>" class="web"/> <img src="<?php echo asset('images/texture_landing_header_right_mob.png'); ?>" class="mob"/> </div>
            <div class="clear"></div>
        </div>
        <div class="section eventTimeline" id="timeline">
            <h1>Event Timeline</h1>
            <h4>You don’t want to miss out on this opportunity!</h4>
            <div class="timelineBox">
                <div class="timelineContent">
                    <h2>Competition Period:</h2>
                    <h3><span></span>1 March - 31 May 2016</h3>
                    <h2>Last Date of Entry Submission:</h2>
                    <h3><span></span>31 May 2016</h3>
                </div>
                <div class="timelineContent">
                    <h2>Physical Submission of Shortlisted Entry:</h2>
                    <h3><span></span>23 June - 24 July 2016</h3>
                    <h2>Prize Giving Ceremony:</h2>
                    <h3><span></span>7 September 2016</h3>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="eventTextureLeft"></div>
            <div class="eventTextureRight"></div>
        </div>
        <div class="section categories" id="categories">
            <h1>Categories</h1>
            <h4>Are you a Digital or Fine Art expert? Choose your battle</h4>
            <div class="categoriesType fineArtImg">
                <div class="categoryContent">
                    <h1 class="fineArt">Fine Art</h1>
                    <div class="categoryContentDesc">
                        <p>Medium is open and can include acrylics, oil painting, silkscreen, collage, ink, mixed media, wood, canvas, metal, cloth  and others.</p>
                        <br />
                        <p>Photography is not allowed.</p>
                        <br />
                        <p>The finished artwork can be in any shape or form provided it is 2-dimensional hanging display within the minimum of 2ft x 2ft, 3ft x 3ft or a maximum size of 3ft x 5ft.</p>
                    </div>
                </div>
            </div>
            <div class="categoriesType digitalArtImg">
                <div class="categoryContent">
                    <h1 class="fineArt">Digital Art</h1>
                    <div class="categoryContentDesc">
                        <p>All artwork must be digitally produced which can include digital painting, vector drawings, 3D graphics and others.</p>
                        <br />
                        <p>The finished artwork has to be produced into a 2-dimensional hanging display.</p>
                        <br />
                        <p>All artwork must be within the minimum size of 2ft x 2ft, 3ft x 3ft or a maximum size of 3ft x 5ft with no preference of orientation or shape.</p>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="section prizes" id="prizes">
            <div class="prizesBox">
                <h1>Prizes</h1>
                <h4>Win your own solo exhibit.</h4>
                <ul>
                    <li>
                        <h2><span></span>Grand Prize</h2>
                        <p><strong>Solo Exhibition</strong> + Mentoring + RM5,000 cash + RM500 worth of Nando’s vouchers.</p>
                        <span></span> </li>
                    <li>
                        <h2><span></span>1st Runner-Up</h2>
                        <p><strong>Group Exhibition</strong> + RM3,000 cash + RM500 worth of Nando’s vouchers.</p>
                        <span></span> </li>
                    <li>
                        <h2><span></span>2nd Runner-Up</h2>
                        <p><strong>Group Exhibition</strong> + RM2,000 cash + RM500 worth of Nando’s vouchers.</p>
                        <span></span> </li>
                    <div class="clear"></div>
                </ul>
                <div class="consoPrize">
                    <h2><span></span>Consolation Prize x 3</h2>
                    <p><strong>Group Exhibition</strong> +  RM500 worth of Nando’s vouchers.</p>
                    <span></span> </div>
            </div>
            <div class="prizesTextureLeft"></div>
            <div class="prizesTextureRight"></div>
        </div>
        <div class="section howToJoin" id="join">
            <div class="howToJoinBox">
                <h1>How to Join</h1>
                <h4>It’s time to unleash your inner Picasso. Just follow these simple steps.</h4>
                <ul>
                    <li>
                        <div class="stepImg step1"></div>
                        <h2><span></span>Step 1</h2>
                        <p>Take photos of your artwork and upload them onto the site.</p>
                        <span></span>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="stepImg step2"></div>
                        <h2><span></span>Step 2</h2>
                        <p>Fill up your personal details and submit your artwork.</p>
                        <span></span>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="stepImg step3"></div>
                        <h2><span></span>Step 3</h2>
                        <p>You'll be contacted via phone and / or email if your work is selected. Get ready to walk away with hot prizes!</p>
                        <span></span>
                        <div class="clear"></div>
                    </li>
                    <div class="clear"></div>
                </ul>
                <!--<a onclick="ga_event_onclick('Submission','How To Join')" href="<?php /*echo base_url() . 'submission'; */?>" class="btnCta trans">Join Now</a>--> </div>
            <div class="joinTextureTopLeft"></div>
            <div class="joinTextureTopRight"></div>
            <div class="joinTextureBottomLeft"></div>
            <div class="joinTextureBottomRight"></div>
        </div>
        <div class="section judges" id="judges">
            <div class="judgesBox">
                <h1>Meet The Judges</h1>
                <h4>They don’t know who you are, but they will look at your art and they will judge it.</h4>
                <span>Click on each judge to see their profile.</span>
                <ul>
                    <li>
                        <a href="#" id="#stephen">
                            <img src="<?php echo asset('images/img_stephen.jpg'); ?>" class="judgesHover"/>
                            <img src="<?php echo asset('images/img_stephen_hover.jpg'); ?>" class="judgesOri"/>
                            <h3>Stephen Chew</h3>
                            <p>CEO of Nando’s Chickenland Malaysia Sdn. Bhd.</p>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="#chungLynn">
                            <img src="<?php echo asset('images/img_mac_chung_lynn.jpg'); ?>" class="judgesHover" />
                            <img src="<?php echo asset('images/img_mac_chung_lynn_hover.jpg'); ?>" class="judgesOri" />
                            <h3>Mac Chung Lynn</h3>
                            <p>Group CEO of Nando’s Chickenland Malaysia and Singapore </p>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="#hakim">
                            <img src="<?php echo asset('images/img_hakim_alias.jpg'); ?>" class="judgesHover" />
                            <img src="<?php echo asset('images/img_hakim_alias_hover.jpg'); ?>" class="judgesOri" />
                            <h3>Hakim Alias</h3>
                            <p>Graphic designer and founder of the famous art collective – RANTAi Art </p>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="#giap">
                            <img src="<?php echo asset('images/img_chong_fei_giap.jpg'); ?>" class="judgesHover" />
                            <img src="<?php echo asset('images/img_chong_fei_giap_hover.jpg'); ?>" class="judgesOri" />
                            <h3>Chong Fei Giap</h3>
                            <p>Digital Artist and Illustrator, co-founder and creative director of Running Snail Art Studio </p>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="#najib">
                            <img src="<?php echo asset('images/img_najib_ahmad_bamadhaj.jpg'); ?>" class="judgesHover" />
                            <img src="<?php echo asset('images/img_najib_ahmad_bamadhaj_hover.jpg'); ?>" class="judgesOri" />
                            <h3>Najib Ahmad Bamadhaj</h3>
                            <p>Emerging artist and a past year winner of Nando’s Art Initiative </p>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="#izan">
                            <img src="<?php echo asset('images/img_izan_tahir.jpg'); ?>" class="judgesHover" />
                            <img src="<?php echo asset('images/img_izan_tahir_hover.jpg'); ?>" class="judgesOri" />
                            <h3>Izan Tahir</h3>
                            <p>Full time artist and founding member of the Alternative Printing group, Goblock.</p>
                        </a>
                    </li>
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="judgesPopUp">
                <div class="judgesDesc">
                    <a href="#" class="btnClosePopUp trans">
                        <img src="<?php echo asset('images/btn_close.jpg'); ?>" />
                    </a>
                    <a href="#" class="judgesSliderLeft">
                        <span class="trans"></span>
                    </a>
                    <a href="#" class="judgesSliderRight"> <span class="trans"></span> </a>

                    <div id="slider">
                        <div class="slide" id="stephen">
                            <div class="judgesCopy"> <img src="<?php echo asset('images/img_stephen_lrg.jpg'); ?>" />
                                <h2>Stephen Chew</h2>
                                <br />
                                <p>The CEO of Nando’s Chickenland Malaysia Sdn. Bhd. has been grilling the Malaysian F&B industry for more than 20 years. Now, he heads one of the most fun-natured restaurants in town and perched on expanding the Nando’s Art Initiative.</p>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="slide" id="chungLynn">
                            <div class="judgesCopy"> <img src="<?php echo asset('images/img_mac_chung_lynn_lrg.jpg'); ?>" />
                                <h2>Mac Chung Lynn</h2>
                                <br />
                                <p>It was 1998 when Mac Chung Lynn introduced Nando’s to her fellow Malaysians by opening the first Nando’s restaurant in Bangsar. Having been in the chain restaurant business for 18 years, this former architect’s flaming passion for art has never died down.</p>
                                <br />
                                <p>Chung Lynn, Group CEO of Nando’s Chickenland Malaysia Sdn. Bhd., started the Nando’s Art Initiative in 2008 as she believes that the new generation of artists need a good platform to unearth their talents and one day make a name for themselves in the art scene.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="slide" id="hakim">
                            <div class="judgesCopy"> <img src="<?php echo asset('images/img_hakim_alias_lrg.jpg'); ?>" />
                                <h2>Hakim Alias</h2>
                                <br />
                                <p>Through his passion for art and beginnings as a Graphic Designer, Hakim Alias founded the famous art collective, RANTAI Art with the
                                    initial aim to facilitate and nurture the public interest in art. Today, the RANTAI Art Festival has grown to be an anticipated yearly art event among the youth, among the cool, among all art-loving Malaysians. His drive of bringing art to the forefront has brought innovation to his collective and has even recently launched a mobile application for RANTAI Art.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="slide" id="giap">
                            <div class="judgesCopy"> <img src="<?php echo asset('images/img_chong_fei_giap_lrg.jpg'); ?>" />
                                <h2>Chong Fei Giap</h2>
                                <br />
                                <p>Born in Seremban and a graduate of The One Academy, Chong Fei Giap is a Digital Artist and Illustrator best known for his detailed illustrations
                                    of Asian Cityscapes. His works entitled Loka Made are a quirky cross between illustration, animation, manga and digital art. Chong is also the co-founder and creative director of Running Snail Art Studio – an illustration production company which provides illustrations and designs for clients. As though this young artist is not busy enough, he also teaches part-time!</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="slide" id="najib">
                            <div class="judgesCopy"> <img src="<?php echo asset('images/img_najib_ahmad_bamadhaj_lrg.jpg'); ?>" />
                                <h2>Najib Ahmad Bamadhaj</h2>
                                <br />
                                <p>Najib is a proud alum of the Nando’s Art Initiative having participated twice and coming out on top in 2010! This talented and driven artist has made a mark in the art world not only locally but also internationally! From acquiring residencies in Bali to having solo exhibitions in Singapore, Najib has certainly grown from strength to strength since his early days of being a participant of Nando’s Art Initiative.</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="slide" id="izan">
                            <div class="judgesCopy"> <img src="<?php echo asset('images/img_izan_tahir_lrg.jpg'); ?>" />
                                <h2>Izan Tahir</h2>
                                <br />
                                <p>Though born in Malaysia, Izan Tahir spent her formative years garnering experience in the UK after receiving a BA in Art & Design from the London College of Printing in 1971. She returned to Malaysia for good only in 2004 where she is now a full time artist with her own studio and is a founding member of the Alternative Printing group, Goblock. Her no-
                                    nonsense attitude is reminiscent of the good ol’ British charm.</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="section judgingCriteria" id="criteria">
            <div class="judgingCriteriaBox">
                <h1>Judging Criteria</h1>
                <h4>See what it takes to win prizes and a lifetime’s worth of bragging rights.</h4>
                <ul>
                    <li> <span>20%</span>
                        <h2><span></span>Originality & Creativity</h2>
                    </li>
                    <li> <span>20%</span>
                        <h2><span></span>Use of Medium & Artistic Finish</h2>
                    </li>
                    <li> <span>20%</span>
                        <h2><span></span>The Story - Concept & Ideas</h2>
                    </li>
                    <li> <span>20%</span>
                        <h2><span></span>Overall Impression</h2>
                    </li>
                    <li> <span>20%</span>
                        <h2><span></span>Quality of Design & Composition</h2>
                    </li>
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="judgingCriteriaTexture"></div>
        </div>
    </div>
</div>
</div>
</div>

<?php echo $template['partials']['footer']; ?>

</div>
</div>
<script language="javascript" type="text/javascript">

    $(".judgesPopUp").hide;

    if(window.location.hash) {
        var tab = '#' + window.location.hash.substring(1);
        var mainMenu = $(".mainNavi").height();
        var subMenu =  $(".subLandingMenu").height();
        var totalNavi = mainMenu + subMenu;
        $('html, body').animate({
            scrollTop: $(tab).offset().top - totalNavi
        }, 500)
    }

    $(".subNavi ul li a").click(function(event) {
        event.preventDefault();
        var tab = $(this).attr("href");
        console.log(tab);
        var mainMenu = $(".mainNavi").height();
        var subMenu =  $(".subLandingMenu").height();
        var totalNavi = mainMenu + subMenu;
        $('html, body').animate({
            scrollTop: $(tab).offset().top - totalNavi
        }, 500)
    });

    $("a.btnCloseMenu, .popUpBg").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeOut();
        enableScroll();
    });
    $("a.btnBurger").click(function(event) {
        event.preventDefault();
        $('.navigation').slideToggle();
        $('.popUpBg').fadeIn();
        disableScroll();
    });
    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }
    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }
    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
    var x,y,top,left,down;

    $("#stuff").mousedown(function(e){
        e.preventDefault();
        down=true;
        x=e.pageX;
        y=e.pageY;
        top=$(this).scrollTop();
        left=$(this).scrollLeft();
    });

    $("body").mousemove(function(e){
        if(down){
            var newX=e.pageX;
            var newY=e.pageY;
            $("#stuff").scrollTop(top-newY+y);
            $("#stuff").scrollLeft(left-newX+x);
        }
    });

    $("body").mouseup(function(e){down=false;});
    $(".judgesBox a").click(function(event){
        event.preventDefault();
        var judges = $(this).attr("id");
        $(".judgesPopUp").fadeIn();
        $(".slide").not(judges).hide();
        $(judges).show();
        $(judges).addClass("active");
        var mainMenu = $(".mainNavi").height();
        var subMenu =  $(".subLandingMenu").height();
        var totalNavi = mainMenu + subMenu;
        $('html, body').animate({
            scrollTop: $("#slider").offset().top - totalNavi
        }, 500)
    });

    $("a.btnClosePopUp").click(function(event){
        event.preventDefault();
        $(".judgesPopUp").fadeOut();
        $('.slide').removeClass('active');
    });

    $('a.judgesSliderRight').click(function(event){
        event.preventDefault();
        nextSlide();
    });
    $('a.judgesSliderLeft').click(function(event){
        event.preventDefault();
        prevSlide();
    });
    function nextSlide(){
        $('#slider .active').removeClass('active').addClass('oldActive');
        if($('#izan').is(':visible')){
            $('.slide').first().addClass('active');
        }
        $('.oldActive').next().addClass('active');
        $('.oldActive').removeClass('oldActive');
        $('.slide').fadeOut();
        $('.active').fadeIn();
    }

    function prevSlide(){
        $('#slider .active').removeClass('active').addClass('oldActive');
        if($('#stephen').is(':visible')){
            $('.slide').last().addClass('active');
        }
        $('.oldActive').prev().addClass('active');
        $('.oldActive').removeClass('oldActive');
        $('.slide').fadeOut();
        $('.active').fadeIn();
    }

    var sections = $('.section')
        , nav = $('.subNavi')
        , mainNavi = $('.mainNavi').height()
        , navHeight = nav.outerHeight();

    $(window).on('scroll', function () {
        var curPos = $(this).scrollTop();
        sections.each(function() {
            var top = $(this).offset().top - navHeight - mainNavi,
                bottom = top + $(this).outerHeight();

            if (curPos >= top && curPos <= bottom) {
                nav.find('a').removeClass('active');
                sections.removeClass('active');
                $(this).addClass('active');
                nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');

                if ($(this).attr('id') === 'about') {
                    localStorage.removeItem("timeline");
                    localStorage.removeItem("categories");
                    localStorage.removeItem("prizes");
                    localStorage.removeItem("join");
                    localStorage.removeItem("judges");
                    localStorage.removeItem("criteria");

                    if (!localStorage.getItem('about')) {
                        localStorage.setItem('about', 'true');
                        ga_virtualPage('/artinitiative/About',"Nando's Art Initiative - About");
                        console.log('sent about GA');
                    }
                } else if($(this).attr('id') === 'timeline') {
                    localStorage.removeItem("about");
                    localStorage.removeItem("categories");
                    localStorage.removeItem("prizes");
                    localStorage.removeItem("join");
                    localStorage.removeItem("judges");
                    localStorage.removeItem("criteria");

                    if (!localStorage.getItem('timeline')) {
                        localStorage.setItem('timeline', 'true');
                        ga_virtualPage('/artinitiative/Event-timeline',"Nando's Art Initiative - Event Timeline");
                        console.log('sent timeline GA');
                    }
                } else if($(this).attr('id') === 'categories') {
                    localStorage.removeItem("about");
                    localStorage.removeItem("timeline");
                    localStorage.removeItem("prizes");
                    localStorage.removeItem("join");
                    localStorage.removeItem("judges");
                    localStorage.removeItem("criteria");

                    if (!localStorage.getItem('categories')) {
                        localStorage.setItem('categories', 'true');
                        ga_virtualPage('/artinitiative/Categories',"Nando's Art Initiative - Categories");
                        console.log('sent categories GA');
                    }
                } else if($(this).attr('id') === 'prizes') {
                    localStorage.removeItem("about");
                    localStorage.removeItem("timeline");
                    localStorage.removeItem("categories");
                    localStorage.removeItem("join");
                    localStorage.removeItem("judges");
                    localStorage.removeItem("criteria");

                    if (!localStorage.getItem('prizes')) {
                        localStorage.setItem('prizes', 'true');
                        ga_virtualPage('/artinitiative/Prizes',"Nando's Art Initiative - Prizes");
                        console.log('sent prizes GA');
                    }
                } else if($(this).attr('id') === 'join') {
                    localStorage.removeItem("about");
                    localStorage.removeItem("timeline");
                    localStorage.removeItem("categories");
                    localStorage.removeItem("prizes");
                    localStorage.removeItem("judges");
                    localStorage.removeItem("criteria");

                    if (!localStorage.getItem('join')) {
                        localStorage.setItem('join', 'true');
                        ga_virtualPage('/artinitiative/How-To-Join',"Nando's Art Initiative - How To Join");
                        console.log('sent join GA');
                    }
                } else if($(this).attr('id') === 'judges') {
                    localStorage.removeItem("about");
                    localStorage.removeItem("timeline");
                    localStorage.removeItem("categories");
                    localStorage.removeItem("prizes");
                    localStorage.removeItem("join");
                    localStorage.removeItem("criteria");

                    if (!localStorage.getItem('judges')) {
                        localStorage.setItem('judges', 'true');
                        ga_virtualPage('/artinitiative/Meet-The-Judges',"Nando's Art Initiative - Meet The Judges");
                        console.log('sent judges GA');
                    }
                }  else if($(this).attr('id') === 'criteria') {
                    localStorage.removeItem("about");
                    localStorage.removeItem("timeline");
                    localStorage.removeItem("categories");
                    localStorage.removeItem("prizes");
                    localStorage.removeItem("join");
                    localStorage.removeItem("judges");

                    if (!localStorage.getItem('criteria')) {
                        localStorage.setItem('judges', 'true');
                        ga_virtualPage('/artinitiative/Judging-Criteria',"Nando's Art Initiative - Judging Criteria");
                        console.log('sent judges GA');
                    }
                }
            }
            else if($(window).scrollTop() + $(window).height() == $(document).height()) {
                $(".subNavi ul li a").removeClass('active');
                nav.find('a[href="#criteria"]').addClass('active');
            }
        });
    });

</script>