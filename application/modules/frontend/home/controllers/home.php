<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{

    /**
     * Submission constructor.
     * Load Form Helper
     */
    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Display Homepage
     */
    public function index()
    {
        $this->template->title("Nando's Art Initiative");
        $this->template->set_partial('header', 'partial/frontend/header')->set_partial('navigation', 'partial/frontend/navigation')
            ->set_partial('footer', 'partial/frontend/footer')->set_layout('frontend')->build('home');
    }

}