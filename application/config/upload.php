<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Upload Configuration
 */
$config['submission']['upload_path'] = './uploads/';
$config['submission']['allowed_types'] = 'gif|jpg|png|jpeg';
$config['submission']['max_size'] = '5300';
$config['submission']['encrypt_name'] = TRUE;