<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/**
 * Frontend Routes
 */
$route['default_controller'] = 'home';
$route['submission'] = 'submission/create';
$route['submission/store'] = 'submission/store';
$route['faq'] = 'faq/index';
$route['terms_and_conditions'] = 'terms_conditions/index';
$route['results'] = 'results/index';

/**
 * Backend Routes
 */
$route['admin/login'] ='auth/getLogin';
$route['admin/postLogin'] ='auth/postLogin';
$route['admin/logout'] = 'auth/logout';

// Dashboard Routes
$route['admin/dashboard'] ='dashboard/index';

// Artwork Submission Routes
$route['admin/submissions/(:num)'] = 'artwork/index/$1';
$route['admin/submissions'] = 'artwork/index';
$route['admin/submissions/view/(:num)'] = 'artwork/view/$1';
$route['admin/submissions/filter/(:num)'] = 'artwork/filter/$1';
$route['admin/submissions/filter'] = 'artwork/filter';
$route['admin/submissions/export'] = 'artwork/export';

// Users Route
$route['admin/users'] = 'user/index';
$route['admin/users/create'] = 'user/create';
$route['admin/users/store'] = 'user/store';
$route['admin/users/edit/(:num)'] = 'user/edit/$1';
$route['admin/users/update/(:num)'] = 'user/update/$1';
$route['admin/users/delete/(:any)'] = 'user/destroy/$1';

// Judge Status
$route['admin/judges'] = 'judge/index';

/**
 * Judge Routes
 */

// Fine Artwork Route
$route['admin/submissions/fine_art'] = 'fine_art/index';
$route['admin/submissions/fine_art/(:num)'] = "fine_art/index/$1";
$route['admin/submissions/fine_art/view/(:num)'] = 'fine_art/view/$1';
$route['admin/submissions/fine_art/shortlisting'] = "fine_art/storeShortListing";
$route['admin/submissions/fine_art/(:num)/shortlisting'] = "fine_art/storeShortListing";
$route['admin/submissions/fine_art/shortlists'] = "fine_art/getShortListing";
$route['admin/submissions/fine_art/shortlists/(:num)'] = "fine_art/getShortListing/$1";
$route['admin/submissions/fine_art/shortlists/delete'] = "fine_art/deleteShortListing";
$route['admin/submissions/fine_art/finallisting'] = "fine_art/storeFinalListing";
$route['admin/submissions/fine_art/(:num)/finallisting'] = "fine_art/storeFinalListing";
$route['admin/submissions/fine_art/finalists'] = "fine_art/getFinalListing";
$route['admin/submissions/fine_art/finalists/(:num)'] = "fine_art/getFinalListing/$1";
$route['admin/submissions/fine_art/finalists/delete'] = "fine_art/deleteFinalListing";
$route['admin/submissions/fine_art/votes'] = "vote/getFineArtVotes";
$route['admin/submissions/fine_art/votes/(:num)'] = "vote/getFineArtVotes/$1";
$route['admin/submissions/fine_art/votes/export'] = "vote/export";


// Digital Artwork Route
$route['admin/submissions/digital_art'] = 'digital_art/index';
$route['admin/submissions/digital_art/(:num)'] = "digital_art/index/$1";
$route['admin/submissions/digital_art/view/(:num)'] = 'digital_art/view/$1';
$route['admin/submissions/digital_art/shortlisting'] = "digital_art/storeShortListing";
$route['admin/submissions/digital_art/(:num)/shortlisting'] = "digital_art/storeShortListing";
$route['admin/submissions/digital_art/shortlists'] = "digital_art/getShortListing";
$route['admin/submissions/digital_art/shortlists/(:num)'] = "digital_art/getShortListing/$1";
$route['admin/submissions/digital_art/shortlists/delete'] = "digital_art/deleteShortListing";
$route['admin/submissions/digital_art/finallisting'] = "digital_art/storeFinalListing";
$route['admin/submissions/digital_art/(:num)/finallisting'] = "digital_art/storeFinalListing";
$route['admin/submissions/digital_art/finalists'] = "digital_art/getFinalListing";
$route['admin/submissions/digital_art/finalists/(:num)'] = "digital_art/getFinalListing/$1";
$route['admin/submissions/digital_art/finalists/delete'] = "digital_art/deleteFinalListing";
$route['admin/submissions/digital_art/votes'] = "vote/getDigitalArtVotes";
$route['admin/submissions/digital_art/votes/(:num)'] = "vote/getDigitalArtVotes/$1";
$route['admin/submissions/digital_art/votes/export'] = "vote/export";

/* End of file routes.php */
/* Location: ./application/config/routes.php */