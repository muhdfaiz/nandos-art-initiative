<?php

/**
 * Get value only for certain key from array
 *
 * @param array $items
 * @param $keys
 * @return array
 */
function pluck($items = [], $keys)
{
    $items = array_map(function ($item) use($keys, $items) {
        return $item[$keys];
    }, $items);

    return $items;
}