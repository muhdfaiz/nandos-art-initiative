<?php
/**
 * User: faiz
 * Date: 3/1/16
 * Time: 3:19 PM
 */

if ( ! function_exists('asset'))
{
    function asset($path)
    {
        $CI =& get_instance();
        $assetPath = $CI->config->item('asset_path');

        return base_url() . $assetPath . $path;
    }
}