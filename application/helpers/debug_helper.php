<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Readable print_r
 *
 * Prints human-readable information about a variable
 *
 * @access	public
 * @param	mixed
 */
if ( ! function_exists('dd'))
{
    function dd($var)
    {
        $CI =& get_instance();
        echo '<pre>' . print_r($var, TRUE) . '</pre>';
        die();
    }
}
// ------------------------------------------------------------------------
/**
 * Readable var_dump
 *
 * Readable dump information about a variable
 *
 * @access	public
 * @param	mixed *
 */
if ( ! function_exists('debug'))
{
    function debug($var)
    {
        $CI =& get_instance();
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        die();
    }
}