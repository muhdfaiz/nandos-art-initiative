<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo $this->security->get_csrf_hash(); ?>">

    <title>Admin Dashboard</title>
    <!-- Font -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300,300italic,500italic|Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
    <!-- Bootstrap -->
    <link href="<?php echo asset('css/backend/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/material-design-iconic-font.min.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/reset.min.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/helper.min.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/style.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/animate.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/sweet-alert.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/daterangepicker.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/backend/jquery.bxslider.css') ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <?php echo $template['partials']['sidebar']; ?>

    <section id="content">
        <?php echo $template['partials']['content_header']; ?>

        <?php echo $template['body']; ?>
    </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo asset('js/jquery.min.js') ?>"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo asset('js/backend/bootstrap.min.js') ?>"></script>
<!-- WayPoint JS -->
<script src="<?php echo asset('js/backend/waypoints.min.js') ?>"></script>
<!-- Moment JS -->
<script src="<?php echo asset('js/backend/moment.min.js') ?>"></script>
<!-- Date Range Picker JS -->
<script src="<?php echo asset('js/backend/daterangepicker.js') ?>"></script>
<!-- Counter Up JS -->
<script src="<?php echo asset('js/backend/jquery.counterup.min.js') ?>"></script>
<!-- Sweet Alert -->
<script src="<?php echo asset('js/backend/sweet-alert.min.js') ?>"></script>
<script src="<?php echo asset('js/backend/jquery.elevateZoom.min.js') ?>"></script>
<script src="<?php echo asset('js/backend/jquery.bxslider.min.js') ?>"></script>
<!-- App JS -->
<script src="<?php echo asset('js/backend/app.js') ?>"></script>
</body>
</html>