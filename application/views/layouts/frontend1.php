<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <title><?php echo $template['title']; ?></title>
    <link href="<?php echo asset('css/frontend/style.css') ?>" rel="stylesheet">
    <script src="<?php echo asset('js/jquery.min.js') ?>"></script>
    <script src="<?php echo asset('js/frontend/modernizr_2.6.1.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo asset('js/frontend/custome.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo asset('js/frontend/ga_custom_script.js'); ?>" type="text/javascript"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-17144988-7', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <?php echo $template['body']; ?>
</body>
</html>