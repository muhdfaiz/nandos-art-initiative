<div class="subLandingMenu" id="stuff">
    <div class="subNavi">
        <ul>
            <li><a onclick="ga_virtualPage('/artinitiative/About','Nando&lsquo;s Art Initiative - About');" href="#about" class="active"><span></span>About</a></li>
            <li><a onclick="ga_virtualPage('/artinitiative/Event-timeline','Nando&lsquo;s Art Initiative - Event Timeline');" href="#timeline"><span></span>Event Timeline</a></li>
            <li><a onclick="ga_virtualPage('/artinitiative/Categories','Nando&lsquo;s Art Initiative - Categories');" href="#categories" ><span></span>Categories</a></li>
            <li><a onclick="ga_virtualPage('/artinitiative/Prizes','Nando&lsquo;s Art Initiative - Prizes');" href="#prizes" ><span></span>Prizes</a></li>
            <li><a onclick="ga_virtualPage('/artinitiative/How-To-Join','Nando&lsquo;s Art Initiative - How To Join');" href="#join" ><span></span>How To Join</a></li>
            <li><a onclick="ga_virtualPage('/artinitiative/Meet-The-Judges','Nando&lsquo;s Art Initiative - Meet The Judges');" href="#judges"><span></span>The Judges</a></li>
            <li><a onclick="ga_virtualPage('/artinitiative/Judging-Criteria','Nando&lsquo;s Art Initiative - Judging Criteria');" href="#criteria" ><span></span>Judging Criteria</a></li>
            <div class="clear"></div>
        </ul>
    </div>
</div>