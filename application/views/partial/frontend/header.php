<div class="mainNavi">
    <div class="naviContent">
        <div class="mainLogo"> <a href="<?php echo base_url(); ?>"><img src="<?php echo asset('images/logo_nai.png'); ?>" /></a> </div>
        <div class="navigation"> <a href="#" class="btnCloseMenu"><img src="<?php echo asset('images/btn_close_menu.jpg'); ?>" /></a>
            <ul>
                <li><a href="<?php echo base_url(); ?>" class="<?php echo ($this->uri->segment(1))  ? 'trans' : 'active trans'; ?>"><span></span>Home</a></li>
                <!--<li><a onClick="ga_event_onclick('Submission','Top Menu - Submission')" href="<?php /*echo base_url() . 'submission'; */?>" class="<?php /*echo ($this->uri->segment(1) == 'submission')  ? 'active trans' : 'trans'; */?>"><span></span>Submission</a></li>-->
                <li><a href="<?php echo base_url() . 'faq'; ?>" class="<?php echo ($this->uri->segment(1) == 'faq')  ? 'active trans' : 'trans'; ?>"><span></span>FAQ</a></li>
                <li><a href="<?php echo base_url() . 'results'; ?>" class="<?php echo ($this->uri->segment(1) == 'results')  ? 'active trans' : 'trans'; ?>"><span></span>Results</a></li>
            </ul>
        </div>
        <a href="#" class="btnBurger"><img src="<?php echo asset('images/btn_burger_menu.jpg'); ?>" /></a>
        <div class="popUpBg"></div>
        <div class="clear"></div>
    </div>
</div>