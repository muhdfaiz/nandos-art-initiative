<div class="footer">
    <div class="footerLinkWrap">
        <div class="footerLink"> <a target="_blank" href="<?php echo base_url() . 'terms_and_conditions'; ?>">Terms & Conditions</a> | <a target="_blank" href="<?php echo base_url() . 'faq'; ?>">FAQ</a> | <a href="mailto:peri-peri@nandos.com.my" target="_top">Contact Us</a> | © Copyright Nando's 2016 </div>
        <div class="footerSocial">
            <ul>
                <li>Follow us on </li>
                <li><a onclick="ga_event_socialShare('Facebook')" target="_blank" href="https://www.facebook.com/nandos.my"  class="btnFb"><span></span></a></li>
                <li><a onclick="ga_event_socialShare('Twitter')" target="_blank" href="https://twitter.com/NandosMY" class="btnTwitter"><span></span></a></li>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>