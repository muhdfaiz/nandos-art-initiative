<div class="header">
    <div class="wrapper">
        <div class="nav-toggle pull-left">
            <i class="zmdi zmdi-menu zmdi-hc-2x"></i>
        </div>
        <div class="user-menu pull-right">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="true">
                <img alt="" src="<?php echo asset('images/avatar.png') ?>" class="img-circle profile-img thumb-sm">
                <span class="username"><?php echo $this->ion_auth->user()->row()->first_name . ' ' . $this->ion_auth->user()->row()->last_name; ?></span> <span class="caret"></span>
            </a>

            <ul class="dropdown-menu pro-menu fadeInUp animated"">
            <li><a href="<?php echo base_url() . 'admin/logout'; ?>"><i class="zmdi zmdi-lock-outline"></i> Log Out</a></li>
            </ul>
        </div>
    </div>
</div>