<section id="sidebar">
    <!-- Logo -->
    <div class="logo">
        <a href="<?php echo base_url() . 'admin/dashboard'; ?>"><img src="<?php echo base_url() . 'assets/images/logo_nai.png'; ?>"></a>
    </div>

    <!-- Navigation -->
    <div class="navigation">
        <ul class="list-unstyled">
            <li>
                <a href="<?php echo base_url() . 'admin/dashboard'; ?>">
                    <i class="zmdi zmdi-home"></i>
                    <div class="nav-label">Dashboard</div>
                </a>
                <ul class="list-unstyled nav-label-mobile">
                    <li>
                        <a href="<?php echo base_url() .'admin/dashboard'; ?>">
                            Dashboard
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Menu For Judge -->
            <?php if($this->ion_auth->in_group(2)) { ?>
                <li>
                    <a href="#">
                        <i class="zmdi zmdi-book-image"></i>
                        <div class="nav-label">Fine Art Submission</div>
                    </a>
                    <ul class="list-unstyled sub-nav nav-label-mobile">
                        <li>
                            <a href="<?php echo base_url() . 'admin/submissions/fine_art'; ?>"><span>Fine Artworks</span>
                                <span class="badge bg-light-blue"><?php echo ($total_artwork_fine_art) ? $total_artwork_fine_art : '0'; ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'admin/submissions/fine_art/shortlists'; ?>"><span>Shortlist Fine Artworks</span>
                                <span class="badge bg-light-blue"><?php echo ($total_artwork_shortlist_fine_art) ? $total_artwork_shortlist_fine_art : '0'; ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'admin/submissions/fine_art/finalists'; ?>">
                                <span>Finalist Fine Artworks</span><span class="badge bg-light-blue"><?php echo ($total_artwork_finalist_fine_art) ? $total_artwork_finalist_fine_art : '0'; ?>/20</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="zmdi zmdi-book-image"></i>
                        <div class="nav-label">Digital Art Submission</div>
                    </a>
                    <ul class="list-unstyled sub-nav nav-label-mobile">
                        <li>
                            <a href="<?php echo base_url() . 'admin/submissions/digital_art'; ?>"><span>Digital Artworks</span>
                                <span class="badge bg-green"><?php echo ($total_artwork_digital_art) ? $total_artwork_digital_art : '0'; ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'admin/submissions/digital_art/shortlists'; ?>"><span>Shortlist Digital Artworks</span>
                                <span class="badge bg-green"><?php echo ($total_artwork_shortlist_digital_art) ? $total_artwork_shortlist_digital_art : '0'; ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'admin/submissions/digital_art/finalists'; ?>"><span>Finalist Digital Artworks</span>
                                <span class="badge bg-green"><?php echo ($total_artwork_finalist_digital_art) ? $total_artwork_finalist_digital_art : '0'; ?>/20</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } ?>

            <?php if($this->ion_auth->in_group(1)) { ?>
            <!-- Menu For Admin -->
            <li>
                <a href="<?php echo base_url() . 'admin/submissions'; ?>">
                    <i class="zmdi zmdi-book-image"></i>
                    <div class="nav-label">Submissions</div>
                </a>
                <ul class="list-unstyled nav-label-mobile">
                    <li>
                        <a href="<?php echo base_url() .'admin/submissions'; ?>">
                            Submissions
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url() . 'admin/judges'; ?>">
                    <i class="zmdi zmdi-accounts-outline"></i>
                    <div class="nav-label">Votes Status</div>
                </a>
                <ul class="list-unstyled nav-label-mobile">
                    <li>
                        <a href="<?php echo base_url() . 'admin/judges'; ?>">
                            Judge Status
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="zmdi zmdi-cloud-download"></i>
                    <div class="nav-label">Votes Result</div>
                </a>
                <ul class="list-unstyled sub-nav nav-label-mobile">
                    <li>
                        <a href="<?php echo base_url() . 'admin/submissions/fine_art/votes'; ?>"><span>Fine Art</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'admin/submissions/digital_art/votes'; ?>"><span>Digital Art</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url() . 'admin/users'; ?>">
                    <i class="zmdi zmdi-accounts-outline"></i>
                    <div class="nav-label">Users</div>
                </a>
                <ul class="list-unstyled nav-label-mobile">
                    <li>
                        <a href="<?php echo base_url() . 'admin/users'; ?>">
                            Users
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php } ?>
    </div>
</section>