<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:valign="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Nando's ART INITIATIVE</title>
</head>

<body>
<table id="Table_01" cellpadding="0" cellspacing="0" align="center" border="0" width="650" bgcolor="#ece7ca">
    <tbody>
    <tr>
        <td width="26"><img src="http://www.nandos.com.my/edm/border-1.jpg" width="26" height="99" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
        <td><a href="http://www.nandos.com.my/artinitiative" target="_blank"><img src="http://www.nandos.com.my/edm/lg-nandos-art-initiative.jpg" width="119" height="99" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></a></td>
        <td width="134">&nbsp;</td>
        <td width="92">&nbsp;</td>
        <td width="48">&nbsp;</td>
        <td width="209" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#1e2323; text-align: center; line-height: 12px;"><a href="http://www.nandos.com.my/artinitiative" target="_blank" style="text-decoration:none; color:#1e2323;"><strong>www.nandos.com.my/artinitiative</strong></a></td>
        <td width="22">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/thank-you-for-submitting.jpg" width="650" height="156" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ece7ca">
                <tbody>
                <tr>
                    <td width="36">&nbsp;</td>
                    <td width="84">&nbsp;</td>
                    <td width="35"><img src="http://www.nandos.com.my/edm/icon-1.jpg" width="35" height="109" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
                    <td width="359" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#1e2323; line-height: 17px; text-align:left;">
                        <strong>Hi <?php echo $participant_name; ?>! We have successfully received your masterpiece! Thank you for participating in Nando’s Art Initiative, and here are your details:</strong></td>
                    <td width="71">&nbsp;</td>
                    <td width="24">&nbsp;</td>
                    <td width="41">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/border-11.jpg" width="650" height="46" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/full-view-of-artwork.jpg" width="650" height="51" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ece7ca">
                <tbody>
                <tr>
                    <td width="16">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width="26">&nbsp;</td>
                    <td width="510"><img src="<?php echo base_url() . 'uploads/' . $artwork_full_image; ?>" width="510" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
                    <td width="21">&nbsp;</td>
                    <td width="27">&nbsp;</td>
                    <td width="22">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ece7ca">
                <tbody>
                <tr>
                    <td width="44" height="174">&nbsp;</td>
                    <td width="86">&nbsp;</td>
                    <td width="65">&nbsp;</td>
                    <td width="69">&nbsp;</td>
                    <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#1e2323; line-height: 19px; text-align:center; font-weight:bold;">
                        <span style="color:#00afaa; font-size: 14px;">Name of Artwork</span><br />
                        <?php if (isset($artwork_name) && !empty($artwork_name)) {
                            echo $artwork_name;
                        } else  { ?>
                            NA
                        <?php } ?>
                        <br />
                        <br />
                        <span style="color:#00afaa; font-size: 14px;">Category</span><br />
                        <?php if (isset($artwork_category_name) && !empty($artwork_category_name)) {
                            echo $artwork_category_name;
                        } else  { ?>
                            NA
                        <?php } ?>
                    </td>

                    <td width="171">&nbsp;</td>
                    <td width="93">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ece7ca">
                <tbody>
                <tr>
                    <td width="26" height="81">&nbsp;</td>
                    <td width="18">&nbsp;</td>
                    <td width="14">&nbsp;</td>
                    <td width="527" style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:14px; color:#1e2323; line-height: 16px; text-align:center;">
                        <span style="color:#00afaa; font-size: 14px;"><strong>Creative Rationale</strong></span><br /><br />
                        <?php if (isset($artwork_creative_rationale) && !empty($artwork_creative_rationale)) {
                            echo $artwork_creative_rationale;
                        } else  { ?>
                            NA
                        <?php } ?>
                    </td>
                    <td width="24">&nbsp;</td>
                    <td width="19">&nbsp;</td>
                    <td width="22">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/border-30.jpg" width="650" height="47" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/personal-details.jpg" width="650" height="65" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ece7ca">
                <tbody>
                <tr>
                    <td width="26" height="268">&nbsp;</td>
                    <td width="43">&nbsp;</td>
                    <td width="257" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#1e2323; line-height: 17px; text-align:left; font-weight:bold;" valign="top">
                        <span style="color:#00afaa;">Name as per NRIC</span><br />
                        <?php if (isset($participant_name) && !empty($participant_name)) {
                            echo $participant_name;
                        } else  { ?>
                            NA
                        <?php } ?>
                        <br />
                        <br />
                        <span style="color:#00afaa;">NRIC Number</span><br />
                        <?php if (isset($participant_nric_no) && !empty($participant_nric_no)) {
                            echo $participant_nric_no;
                        } else  { ?>
                            NA
                        <?php } ?>
                        <br />
                        <br />
                        <span style="color:#00afaa;">Contact No.</span><br />
                        <?php if (isset($participant_contact_no) && !empty($participant_contact_no)) {
                            echo $participant_contact_no;
                        } else  { ?>
                            <b>NA</b>
                        <?php } ?>
                        <br />
                        <br />
                        <span style="color:#00afaa;">Home Address</span><br />
                        <?php if (isset($participant_home_address) && !empty($participant_home_address)) {
                            echo $participant_home_address;
                        } else  { ?>
                            NA
                        <?php } ?>
                    </td>
                    <td width="37">&nbsp;</td>
                    <td width="217" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#1e2323; line-height: 17px; text-align:left; font-weight:bold;" valign="top">
                        <span style="color:#00afaa;">Gender</span><br />
                        <?php if (isset($participant_gender_id) && !empty($participant_gender_id)) {
                            if ($participant_gender_id == 1) {
                                echo 'Male';
                            } else {
                                echo 'Female';
                            }
                        } else  { ?>
                            NA
                        <?php } ?><br />
                        <br />
                        <span style="color:#00afaa;">Email</span><br />
                        <?php if (isset($participant_email) && !empty($participant_email)) {
                            echo $participant_email;
                        } else  { ?>
                            NA
                        <?php } ?>
                        <br />
                        <br />
                        <span style="color:#00afaa;">College / university</span><br />
                        <span style="color:#500050;">
                            <?php if (isset($participant_college_name_for_email) && !empty($participant_college_name_for_email)) { ?>
                                <span style="color:#1e2323;"><?php echo $participant_college_name_for_email; ?></span>
                            <?php } else  { ?>
                                <span style="color:#1e2323;">NA</span>
                            <?php } ?>
                        </span></td>

                    <td width="39">&nbsp;</td>
                    <td width="31">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/border-36.jpg" width="650" height="61" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#ece7ca">
                <tbody>
                <tr>
                    <td width="26">&nbsp;</td>
                    <td width="18">&nbsp;</td>
                    <td width="38"><img style="display:block; border:none;" src="http://www.nandos.com.my/edm/icon-2-2.jpg" width="38" height="107" alt="Nando's ART INITIATIVE"></td>
                    <td width="498"git style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#e6007d; line-height: 18px; text-align:left;" valign:"top">
                        <em>Kindly be reminded that your submission is <strong>final</strong> and <strong>non-editable</strong>. <br />
                            <br />
                            If your artwork is shortlisted for the final judging, you will be notified by
                            telephone, e-mail and/or SMS.<br />
                            <br />
                            Please visit <a href="http://www.nandos.com.my/artinitiative/" target="_blank" style="text-decoration:none; color:#e6007d;"><strong>www.nandos.com.my/artinitiative</strong></a> for more info. Good Luck! </em></td>
                    <td width="13">&nbsp;</td>
                    <td width="35">&nbsp;</td>
                    <td width="21">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/border-42.jpg" width="650" height="82" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e2323">
                <tbody>
                <tr>
                    <td width="26" height="179">&nbsp;</td>
                    <td width="18">&nbsp;</td>
                    <td width="22">&nbsp;</td>
                    <td width="519" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#a5afaa; line-height: 15px; text-align:left;">
                        <span style="font-size:15px; color:#e1e1d2;">If you have any questions about your submission, please <a href="mailto:peri-peri@nandos.com.my" target="_blank" style="color:#e6007d;"><strong>contact us</strong></a>.</span><br />
                        <br />
                        This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited</td>
                    <td width="24">&nbsp;</td>
                    <td width="19">&nbsp;</td>
                    <td width="22">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e2323">
                <tbody>
                <tr>
                    <td width="66" height="70">&nbsp;</td>
                    <td width="284" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#a5afaa; line-height: 14px; text-align:left;">
                        <a style="text-decoration: none; color: #a5afaa;" target="_blank" href="<?php echo base_url() . 'terms_and_conditions'; ?>">Terms & Conditions</a>   |   © Copyright Nando's 2016</td>
                    <td width="80">&nbsp;</td>
                    <td width="95" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#ffffff; line-height: 15px; text-align:left;">
                        Follow us on</td>

                    <td width="26"><a href="https://www.facebook.com/nandos.my" target="_blank"><img src="http://www.nandos.com.my/edm/icon-facebook.jpg" width="26" height="70" alt="Nando's Malaysia: Facebook" style="display:block; border: 0;"></a></td>
                    <td width="34"><a href="https://twitter.com/NandosMY" target="_blank"><img style="display:block; border:none;" src="http://www.nandos.com.my/edm/icon-twitter.jpg" width="34" height="70" alt="Nando's Malaysia: Twitter"></a></td>
                    <td width="64">&nbsp;</td>
                </tr>
                </tbody>
            </table></td>
    </tr>
    <tr>
        <td colspan="7"><img src="http://www.nandos.com.my/edm/border-52.jpg" width="650" height="36" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></td>
    </tr>
    <tr>
        <td colspan="7"><a href="http://www.nandos.com.my" target="_blank"><img src="http://www.nandos.com.my/edm/lg-nandos-peri-peri-chicken.jpg" width="650" height="78" alt="Nando's ART INITIATIVE" style="display:block; border: 0;"></a></td>
    </tr>
    </tbody>
</table>
</body>
</html>