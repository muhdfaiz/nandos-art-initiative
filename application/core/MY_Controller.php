<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    /**
     * MY_Controller constructor.
     * Load ion_auth_library
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
    }

    protected function userNotLoggedIn()
    {
        // Check If User Logged In Or Not. If not logged in redirect to login page
        if (!$this->ion_auth->logged_in()) {
            return redirect(base_url() . 'admin/login', 'refresh');
        }
    }

    protected function userLoggedIn()
    {
        // Check If User Logged In Or Not. If not logged in redirect to login page
        if ($this->ion_auth->logged_in()) {
            return redirect(base_url() . 'admin/dashboard', 'refresh');
        }
    }

}