/**
 * Get Client Screen Width and Height
 *
 * @type {client}
 */
var client = new function() {
    this.width = function() {
        return $(window).width();
    }

    this.height = function() {
        return $(window).height();
    }
}

/**
 * Define sidebar component
 *
 * @type {sidebar}
 */
var sidebar = new function() {
    this.container = $('#sidebar');
    this.toggle = $('#content .header .nav-toggle');
}

/**
 * Define content container element
 * @type {content}
 */
var content = new function() {
    this.container = $('#content');
}

/**
 * Variable Info class
 * Check Variable exist or not
 */
var variableInfo = new function() {
    this.exist = function(varName) {
        if (typeof varName !== 'undefined') {
            return true;
        }
        return false;
    }
}

/**
 * Check if sidebar collapse or not during page load
 * Check key 'collapsed' in local storage.
 * If exist and true means user click the toggle nav button.
 */
var checkSidebarCollapse = function() {
    //console.log(localStorage.getItem("collapsed"));
    if (client.width() < 768) {
        sidebar.container.removeClass('collapsed');

    } else {
        if (localStorage.getItem("collapsed") == 'true') {
            //console.log('add class collapsed');
            sidebar.container.addClass('collapsed');
        }
        if (localStorage.getItem("collapsed") == 'false'){
            sidebar.container.removeClass('collapsed');
            //console.log('remove class collapsed');
        }
    }
};
checkSidebarCollapse();

$( window ).resize(function() {
    checkSidebarCollapse();
});

/**
 * Toggle Navigation Button
 */
var toggleSidebar = function() {
    //console.log(localStorage.getItem("collapsed"));
    $( sidebar.toggle ).click(function() {
        if (client.width() < 768) {
            sidebar.container.removeClass('collapsed');
            sidebar.container.toggleClass('expand');

            if (sidebar.container.hasClass('expand')) {
                localStorage.setItem("expand", "true");
                //console.log('collapsed true');
            } else {
                localStorage.setItem("expand", "false");
                //console.log('collapsed false');
            }
        } else {
            sidebar.container.removeClass('expand');
            sidebar.container.toggleClass('collapsed');

            if (sidebar.container.hasClass('collapsed')) {
                localStorage.setItem("collapsed", "true");
                //console.log('collapsed true');
            } else {
                localStorage.setItem("collapsed", "false");
                //console.log('collapsed false');
            }
        };

    });
}();

/**
 * Count Up Number Animation. Used in dashboard pages to count up submission statistics
 */
$('.counter-up').counterUp();


/**
 * Initialize tooltip
 */
$('[data-toggle="tooltip"]').tooltip();

/**
 * Date Range to filter submission between 2 dates
 */
$('#submissionDateTimeRange').daterangepicker({
    autoUpdateInput: true,
    locale: {
        cancelLabel: 'Clear'
    }
});

/**
 * Auto Complete Input Value. Used in filtering Submission
 */
var autoCompleteField = function(element, source, minLength) {
    $(element).autocomplete({
        source: source,
        minLength: minLength,
        select: function( event, ui ) {
            $(this).val(ui.item.id);
        }
    });
}

/**
 * DOM events
 */
// Click event on clear button to clear all input filter in submission index page
$('.clear-filter').click(function() {
    $('.filter').find(':input').each(function() {
        if(this.type == 'text'){
            $(this).val('');
        } else if(this.type == 'select-one') {
            $(this).val(0);
        }
    });
});

// Click Event for 'apply' button for date picker range. Use to specify submission date during filtering process.
// When click 'apply', the date value will be set as the value for input
$('#submissionDateTimeRange').on('apply.daterangepicker', function(ev, picker) {
    $('#submission_date_range').val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
});

// Click Event for 'cancel' button for date picker range. Use to specify submission date during filtering process.
// When click cancel, the input value will be set to empty
$('#submissionDateTimeRange').on('cancel.daterangepicker', function(ev, picker) {
    $('#submission_date_range').val('');
});

/**
 * Select/UnSelect All checkbox available on submission index page
 */
$('input[name="select-all"]').change(function(e) {
    $('input[name="select"]').prop('checked', function (i, value) {
        return !value;
    });
});

/**
 * Shortlist Single Artwork
 */
$(document).on("click", ".shortlist-single-artwork", function(e){
    e.preventDefault();
    var artworkName = $(this).attr('data-artwork-name');

    var pathArray = window.location.pathname.split( '/' );
    var categoryName = pathArray[pathArray.length-1];


    $.ajax({
        url: categoryName + '/shortlisting',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            artworks_id: [ $(this).attr("data-artwork-id") ],
        },
        type: 'POST',
        success: function(result) {
            if (result) {
                $('#artworkDetailModal').modal('hide');
                swal({
                        title: "Success!",
                        text: "Successfully add artwork " + artworkName + " into shortlist.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            } else {
                swal({
                        title: "Failed!",
                        text: "Failed to add artwork " + artworkName + " into shortlist.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            }
        }
    });
});

/**
 * Shortlist Multiple Artwork
 */
$('.shortlist-multiple-artworks').click(function(e) {
    e.preventDefault();

    var pathArray = window.location.pathname.split( '/' );
    var categoryName = pathArray[pathArray.length-1];

    var artworksID = {};
    var selectedCheckbox = $('input[name="select"]:checked');

    if (selectedCheckbox.length < 1) { swal("Oops...", "Please tick one of the artwork before shortlisting", "error"); return;}

    selectedCheckbox.each(function (index, element){
        artworksID[index] = this.value;
    });

    $.ajax({
        url: categoryName + '/shortlisting',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            artworks_id: artworksID,
        },
        type: 'POST',
        success: function(result) {
            if (result) {
                swal({
                        title: "Success!",
                        text: "Successfully shortlisted the artworks.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            } else {
                swal({
                        title: "Failed!",
                        text: "Failed to shortlist the artworks",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            }
        }
    });
});

$(document).on("click", ".remove-shortlist-single-artwork", function(e){
    e.preventDefault();
    var artworkID = $(this).attr('data-artwork-id');
    var artworkName = $(this).attr('data-artwork-name');
    $('#artworkDetailModal').modal('hide');
    swal({
            title: '',
            text: "Are you sure to remove artwork " + artworkName + " from shortlist?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: 'shortlists/delete',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    artworks_id: artworkID,
                },

                success: function(result) {
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Removed!",
                                text: "Artwork " + artworkName + " has been removed from shortlist.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Remove Failed!",
                                text: "Failed to remove artwork " + artworkName + " from shortlist.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});

$(document).on("click", ".remove-shortlist-multiple-artwork", function(e){
    e.preventDefault();
    var artworksID = {};
    var selectedCheckbox = $('input[name="select"]:checked');

    if (selectedCheckbox.length < 1) { swal("Oops...", "Please tick one of the artwork before remove from shortlists", "error"); return;}

    selectedCheckbox.each(function (index, element){
        artworksID[index] = this.value;
    });
    swal({
            title: '',
            text: "The selected artwork will be removed from shortlist. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: 'shortlists/delete',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    artworks_id: artworksID,
                },

                success: function(result) {
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Removed!",
                                text: "Artworks has been removed from shortlists.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Remove Failed!",
                                text: "Failed to remove artworks from shortlists.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});

/**
 * Finalist Single Artwork
 */
$(document).on("click", ".finalist-single-artwork", function(e){
    e.preventDefault();
    var artworkName = $(this).attr('data-artwork-name');

    $.ajax({
        url: 'finallisting',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            artworks_id: [ $(this).attr("data-artwork-id") ],
        },
        type: 'POST',
        success: function(result) {
            $('#artworkDetailModal').modal('hide');
            if (result instanceof Object) {
                if ('error' in result) {
                    swal({
                            title: "Failed!",
                            text: result.error,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: "OK",
                            closeOnConfirm: false },
                        function(){
                            location.reload();
                        }
                    );
                    return;
                }
            }

            if (result) {
                swal({
                        title: "Success!",
                        text: "Successfully add artwork " + artworkName + " into finalists.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            } else {
                swal({
                        title: "Failed!",
                        text: "Failed to add artwork " + artworkName + " into finalists.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            }
        }
    });
});

/**
 * Finalist Multiple Artwork
 */
$('.finalist-multiple-artworks').click(function(e) {
    e.preventDefault();
    var artworksID = {};
    var selectedCheckbox = $('input[name="select"]:checked');

    if (selectedCheckbox.length < 1) { swal("Oops...", "Please tick one of the artwork before set as finalists", "error"); return;}

    selectedCheckbox.each(function (index, element){
        artworksID[index] = this.value;
    });

    $.ajax({
        url: 'finallisting',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            artworks_id: artworksID,
        },
        type: 'POST',
        success: function(result) {
            if (result instanceof Object) {
                if ('error' in result) {
                    swal({
                            title: "Failed!",
                            text: result.error,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: "OK",
                            closeOnConfirm: false },
                        function(){
                            location.reload();
                        }
                    );
                    return;
                }
            }

            if (result) {
                swal({
                        title: "Success!",
                        text: "Successfully set the artworks as finalists.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            } else {
                swal({
                        title: "Failed!",
                        text: "Failed to set the artworks as finalists",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: "OK",
                        closeOnConfirm: false },
                    function(){
                        location.reload();
                    }
                );
            }
        }
    });
});

$(document).on("click", ".remove-finalist-single-artwork", function(e){
    e.preventDefault();
    var artworkID = $(this).attr('data-artwork-id');
    var artworkName = $(this).attr('data-artwork-name');
    $('#artworkDetailModal').modal('hide');
    swal({
            title: '',
            text: "Are you sure want to remove artwork " + artworkName + " from finalists?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it!',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: 'finalists/delete',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    artworks_id: artworkID,
                },

                success: function(result) {
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Removed!",
                                text: "Artwork " + artworkName + " has been removed from finalists.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Deleting Failed!",
                                text: "Failed to remove artwork " + artworkName + " from finalists",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});


$(document).on("click", ".remove-finalist-multiple-artwork", function(e){
    e.preventDefault();
    var artworksID = {};
    var artworksName = '';
    var selectedCheckbox = $('input[name="select"]:checked');

    if (selectedCheckbox.length < 1) { swal("Oops...", "Please tick one of the artwork before remove from finalists", "error"); return;}

    selectedCheckbox.each(function (index, element){
        artworksID[index] = this.value;

        if (selectedCheckbox.length - 1 == index) {
            artworksName = artworksName + $(this).attr("data-name");
        } else {
            artworksName = artworksName + $(this).attr("data-name") + ', ';
        }
    });

    swal({
            title: '',
            text: "The selected artwork will be removed from finalist. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: 'finalists/delete',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    artworks_id: artworksID,
                },

                success: function(result) {
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Removed!",
                                text: "Artworks has been removed from finalists.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Remove Failed!",
                                text: "Failed to remove artworks from shortlist.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});

/**
 * Click Event on Delete User Button
 */
$('.delete-user').click(function(event) {
    var userID = $(this).attr('data-id');
    var username = $(this).attr('data-name');

    swal({
            title: "Are you sure want to delete User " + username + " ?",
            text: "You will not be able to recover this User after deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, remove it!',
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'users/delete/' + userID,
                type: 'GET',
                success: function(result) {
                    console.log(result);
                    if (result) {
                        swal("Deleted!", "");
                        swal({
                                title: "Deleted!",
                                text: "User " + username + " has been deleted.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Deleting Failed!",
                                text: "User " + username + " failed to delete.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: "OK",
                                closeOnConfirm: false },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        });
});


/**
 * Export Data to excel or csv
 */
$("#submissions-export").click(function(e) {
    e.preventDefault();
    var artworkCategoryID = $("#artwork_category_id").val();
    var submissionDateRange = $("#submission_date_range").val();
    var collegeID = $("#participant_college_id").val();
    var name = $("#participant_name").val();
    var email = $("#participant_email").val();
    var nricNo = $("#participant_nric_no").val();
    var exportURL = '';

    if(window.location.href.indexOf("filter") > -1) {
        exportURL = "export?artwork_category_id=";
    } else {
        exportURL = "submissions/export?artwork_category_id=";
    }
    window.location = exportURL + artworkCategoryID +
        "&submission_date_range=" + submissionDateRange + "&participant_college_id" + collegeID + "&participant_name=" + name +
        "&participant_email=" + email + "&participant_nric_no=" + nricNo;
});

$("#export-votes-result").click(function(e) {
    e.preventDefault();
    var categoryID = $(this).data('category-id');
    var exportURL = "votes/export?artwork_category_id=";
    //console.log(exportURL + categoryID);
    window.location = exportURL + categoryID;


});

$(document).ready(function () {
    $('#artworkDetailModal').on('shown.bs.modal', function() {
        $('.spinner').show();

        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager',
            controls: true,
            adaptiveHeight: true,
            adaptiveHeightSpeed: 100,
            preloadImages: 'visible',
            onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                $.removeData($("li.active-slide .zoom"), 'elevateZoom');//remove zoom instance from image
                $('.zoomContainer').remove();// remove zoom container from DOM
                if (currentSlideHtmlObject == 1) {
                    $('.ribbon-red').html('Artwork Highlight');
                }
                if (currentSlideHtmlObject == 0) {
                    $('.ribbon-red').html('Artwork Full');
                }
                console.log(currentSlideHtmlObject);
                $('.active-slide').removeClass('active-slide');
                $('.bxslider > li').eq(currentSlideHtmlObject + 1).addClass('active-slide');
                $("li.active-slide .zoom").elevateZoom({
                    scrollZoom : true
                });
            },
            onSliderLoad: function () {
                $('.spinner').hide();
                $(".slider").css("visibility", "visible");
                $('.bxslider > li').eq(1).addClass('active-slide');
                $("li.active-slide .zoom").elevateZoom({
                    scrollZoom : true
                });
            },
        });

    });

    $("#artworkDetailModal").on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
        $( ".zoomContainer" ).remove();
    });
});

$('.viewArtworkButton').on('click', function(e) {
    e.preventDefault();
    callback;
});

$('.artwork-image > img').on('click', function(e) {
    e.preventDefault();
    callback;
});


function callback() {
    // Find modal body on the document
    var $currentDetailsModal = $('#artworkDetailModal');
    // Clone modal and save copy
    var $cloneDetailsModal = $currentDetailsModal.clone();
    // Find links in this modal which open this modal again and bind this function to their click events
    $(".viewArtworkButton", $cloneDetailsModal).click(callback);
}

function callback() {
    // Find modal body on the document
    var $currentDetailsModal = $('#artworkDetailModal');
    // Clone modal and save copy
    var $cloneDetailsModal = $currentDetailsModal.clone();
    // Find links in this modal which open this modal again and bind this function to their click events
    $(".artwork-image > img", $cloneDetailsModal).click(callback);
}


function gup( name, url ) {
  if (!url) url = location.href;
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( url );
  return results == null ? null : results[1];
}

$('.data_per_page').on('click', function(e) {
    var url = $('#change-limit').data('url');
    var limit = gup('limit',window.location.href);
    console.log(window.location);
    window.location.href = url +'/1?limit=' + $('#change-limit').val();
});
/*$('#change-limit').on('change',function(e){
  var url = $(this).data('url');
  var limit = gup('limit',window.location.href);
  console.log(window.location);
  window.location.href = url +'/1?limit=' + $('#change-limit').val();
});*/
