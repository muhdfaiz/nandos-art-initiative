﻿function ga_event(category,action,label)
{
ga('send','event',''+category+'',''+action+'',''+label+'');
}

function ga_event_onclick(category,label)
{
ga('send','event',''+category+'','click',''+label+'');
}

function ga_event_view(category,label)
{
ga('send','event',''+category+'','view',''+label+'');
}

function ga_event_video(category,action,label)
{
ga('send','event',''+category+'',''+action+'',''+label+'');
}

function ga_event_form(category,label)
{
ga('send','event',''+category+'','fill-in',''+label+'');
}

function ga_event_socialMedia(category)
{
ga('send','event',''+category+'','click','socialMedia');
}

function ga_event_socialShare(category)
{
ga('send','event',''+category+'','share','socialMedia');
}


function ga_event_download(category,label)
{
ga('send','event',''+category+'','download',''+label+'');
}

function ga_virtualPage(page,title)
{
ga('send','pageview',{'page':''+page+'','title':''+title+''});
}