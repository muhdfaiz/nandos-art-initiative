-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2016 at 05:11 PM
-- Server version: 10.0.22-MariaDB-1~trusty-log
-- PHP Version: 5.6.18-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nandos_art`
--

-- --------------------------------------------------------

--
-- Table structure for table `artworks`
--

CREATE TABLE IF NOT EXISTS `artworks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `participant_gender_id` tinyint(1) unsigned NOT NULL,
  `participant_name` varchar(100) NOT NULL,
  `participant_nric_no` varchar(100) NOT NULL,
  `participant_email` varchar(100) NOT NULL,
  `participant_contact_no` varchar(100) NOT NULL,
  `participant_home_address` varchar(100) NOT NULL,
  `participant_is_student` tinyint(3) unsigned NOT NULL,
  `participant_college_id` tinyint(3) unsigned DEFAULT NULL,
  `participant_college_name` varchar(100) NOT NULL,
  `artwork_category_id` tinyint(1) unsigned zerofill NOT NULL,
  `artwork_name` varchar(100) NOT NULL,
  `artwork_full_image` varchar(255) NOT NULL,
  `artwork_highlight_image` varchar(255) DEFAULT NULL,
  `artwork_creative_rationale` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `participant_id` (`participant_gender_id`),
  KEY `artwork_category_id` (`artwork_category_id`),
  KEY `participant_nric_no` (`participant_nric_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `artworks`
--

INSERT INTO `artworks` (`id`, `participant_gender_id`, `participant_name`, `participant_nric_no`, `participant_email`, `participant_contact_no`, `participant_home_address`, `participant_is_student`, `participant_college_id`, `participant_college_name`, `artwork_category_id`, `artwork_name`, `artwork_full_image`, `artwork_highlight_image`, `artwork_creative_rationale`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muhammad Faiz Bin Halim', '891024-02-5715', 'muhdfaiz@mediacliq.com', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun Kedah', 1, 15, 'Unikl MIIT', 1, 'An Artist Self Portrait', 'b870a01b3cdbbf3ab8dad990cf347a1c.jpg', 'ffd8041199bc8c5346b8c5a54f6e0bf5.jpg', 'Combining traditional figure painting with modern abstract painting, creating tension between realism and abstract, this artist self-portrait is original and definitely one of its kind in the field!', '2016-03-23 15:21:11', '0000-00-00 00:00:00'),
(2, 2, 'yuhanis', '820909-12-4555', 'yuhanis@mediacliq.my', '0124567789', 'sdasdasdsad', 1, 1, 'mmu', 1, 'colourful', '68c947126849e83e837e82916ef5a971.jpg', NULL, 'bla bla bla', '2016-03-24 04:09:35', '0000-00-00 00:00:00'),
(3, 1, 'Muhammad Faiz', '891024-02-5733', 'cupin06@gmail.com', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun Kedah', 1, 1, '', 1, 'Muhammad Faiz', '6f77d9f32ddc348c48c8e749adaa4963.jpg', NULL, 'adsdasdasd', '2016-03-25 05:16:29', '0000-00-00 00:00:00'),
(4, 2, 'MediaCliQ', '901111-10-1113', 'testing@gmail.com', '0101234567', 'MediaCliQ ', 0, 0, '', 2, '', '9c54c7cdc446ea322a55725fbb86d243.jpg', NULL, '', '2016-03-25 05:55:42', '0000-00-00 00:00:00'),
(5, 1, 'Muhammad Faiz', '851024-02-5715', 'cupin06@gmail.com', '0104862127', 'asasasassa', 1, 15, '', 2, 'Muhammad Faiz', '67842bebb55aa3eb343554d77ede8400.jpg', NULL, '', '2016-03-26 01:02:41', '0000-00-00 00:00:00'),
(6, 2, 'Muhammad Faiz', '891024-02-5715', 'cupin06@gmail.com', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun', 1, 28, '', 2, 'Artwork 1', 'eaaca8436684145f7bfdcd03ac1d4fc8.jpg', '7f85e0f32d8d3b2769baa075625ddb80.jpg', '', '2016-03-26 01:13:11', '0000-00-00 00:00:00'),
(7, 1, 'Muhammad Faiz', '861024-02-5715', 'cupin06@gmail.com', '0104862127', 'sadsadasd', 0, 0, '', 2, '', '8c74aff91f3e45224deb658a666e5bf2.jpg', NULL, 'asdasdasd', '2016-03-26 01:18:56', '0000-00-00 00:00:00'),
(8, 1, 'Muhammad Faiz', '991024-02-5715', 'cupin06@gmail.com', '0104862127', 'adsasdasd', 1, 1, '', 1, 'Muhammad Faiz', 'b87366a3a9205cdca81bd45d0d0f8556.jpg', NULL, '', '2016-03-26 01:22:29', '0000-00-00 00:00:00'),
(9, 2, 'Muhammad Faiz', '811024-02-5715', 'cupin06@gmail.com', '0154862127', 'asdasdasd', 1, 1, '', 1, 'Muhammad Faiz', '84dcdf5e5c1d07956491fa201409b5bb.jpg', NULL, '', '2016-03-26 01:24:58', '0000-00-00 00:00:00'),
(10, 1, 'Muhammad Faiz', '771024-02-5715', 'cupin06@gmail.com', '0164862127', 'dasdasdasd', 1, 15, '', 2, 'sadsadsad', '650ea7eca0fea1672971d617f29966ae.jpg', NULL, 'sadsadasd', '2016-03-26 01:28:52', '0000-00-00 00:00:00'),
(11, 1, 'Muhammad Faiz', '591024-02-5715', 'cupin06@gmail.com', '0104862127', 'sdasdas', 1, 1, '', 1, 'Artwork 1', 'fd9a83e15c6b24902d097c07c35fe1ce.jpg', NULL, '', '2016-03-26 01:30:39', '0000-00-00 00:00:00'),
(12, 1, 'Muhammad Faiz', '891021-02-5715', 'cupin06@gmail.com', '0104862127', 'asdasdasdasd', 1, 1, '', 1, 'Artwork 1', 'e582190bde645f4ecb6ef5d2f4ce4968.jpg', NULL, 'sadsad', '2016-03-26 04:07:52', '0000-00-00 00:00:00'),
(13, 1, 'Muhammad Faiz', '891024-02-6715', 'cupin06@gmail.com', '0104862127', 'asdasdasd', 0, 0, '', 2, 'Muhammad Faiz', 'bc6bbd04991ac36346978197618f626d.jpg', NULL, '', '2016-03-26 04:17:04', '0000-00-00 00:00:00'),
(14, 1, 'Muhammad Faiz', '891024-03-5715', 'cupin06@gmail.com', '0144862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun Kedah', 1, 43, 'Unikl MIIT', 1, 'Artwork 1', '09c0a07181bab4755fb5257e3d498f46.jpg', '83acbed106de9a9764a7b10979b817f0.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br />\n<br />\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2016-03-26 04:37:31', '0000-00-00 00:00:00'),
(15, 1, 'Muhammad Faiz', '891024-02-5234', 'cupin06@gmail.com', '0144862127', 'No 9 Lorong 1 taman Gurun Jaya 08300 Gurun', 1, 43, 'Unikl MIIT', 2, 'Digital Painting', '9bdf4b54d97d661ca65cccb40bb1d29b.jpg', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2016-03-26 04:40:41', '0000-00-00 00:00:00'),
(16, 1, 'MediaCliQ', '901111-10-1113', 'testing@gmail.com', '0101234567', 'MediaCliQ', 0, 0, '', 1, '', 'bae725c44247ddfeebd36d638e83f981.jpg', NULL, '', '2016-03-27 22:22:49', '0000-00-00 00:00:00'),
(17, 2, 'MediaCliQ', '901111-10-1114', 'leijing@mediacliq.com', '0101234567', 'MediaCliQ', 0, 0, '', 2, 'testing', 'ad8c24dddad6d3325556428b1dc7e07c.jpg', '10652c14afea4273a872ab4c11647edc.jpg', '', '2016-03-27 22:24:17', '0000-00-00 00:00:00'),
(18, 2, 'MediaCliQ', '901111-10-1115', 'leijing@mediacliq.com', '0101234567', 'MediaCliQ', 1, 43, 'UPM', 1, 'testing', '5271f19565e3f79d09e1b784d31b67de.jpg', 'bb4601a3e41ec57f3445d8b6e02220eb.jpg', 'testing', '2016-03-27 22:43:37', '0000-00-00 00:00:00'),
(19, 1, 'MediaCliQ', '901111-10-1114', 'leijing@mediacliq.com', '0101234567', 'MediaCliQ', 0, 0, '', 1, '', '87579ec99c3ff304258c7da06025dd37.jpg', NULL, '', '2016-03-27 23:50:59', '0000-00-00 00:00:00'),
(20, 2, 'Didi Khadijah', '900309-01-5360', 'khadijasakaria@gmail.com', '0171234567', 'Unit 10-9-10, Sri Alam Fasa 2, Off Jln Istana 57100 Kuala Lumpur', 1, 32, '', 1, 'ABCD', '168e940cf8ea05b92deebf65e9f678b0.JPG', 'fce6c08a095e3f382cfa92b4100da262.JPG', 'qwertyuiop', '2016-03-28 02:35:42', '0000-00-00 00:00:00'),
(21, 2, 'Dk', '911120-08-1234', 'didykhadija@gmail.com', '0101234566', 'Kuala Sungai Baru,Melaka', 0, 0, '', 2, 'Abcde', '644feddb5b303f72b7539479b3bf3d7d.jpeg', '3b41935d30620c44c687b01a613b56da.jpeg', 'Qwertyuiopasd', '2016-03-28 04:03:40', '0000-00-00 00:00:00'),
(22, 2, 'Lim Meng Hooi', '860904-14-5174', 'menghooi.lim@nandos.com.my', '0122140518', 'No. 123, Jalan Sultan, 53300 Kuala Lumpur', 1, 43, 'Art College Metropolis', 1, '', 'fcb7c022adf6dad8f19ad682fc70c10d.jpg', '8fad4f66056057de7dc8df7e4197709c.gif', 'dfkfjkldsfjdsklfjsklfjasklfjaslkfjaskljsldkfds/lfsgja;lsjfew;lrk;lwekr;L,XCM.,M,.VM;WLRQKEPDKAS;LK;LZCKMZDMVS;ALKTIPOWEIF;LSKDCXM.X,MCX,.CMX.MX,.MXMV.CXMVX.CMV,XVM.,VMX.MV.,XCMVX,MVMV,MXV,CMVC,MV.CX,MV.X,CMV.X,CMVX.,MX.,MVCX,.MVX.C,MVX,.MV.X,CMV.CX,VMCX,.MV,.MVX.C,MVX,.VMX.C,VM.CX,MVCX,.MVX.C,MVX,.MVCX.,MVX.C,VM.MCX,.VMX.MSOERPOEWIRPOQIPres;fd;lfkds;lfkds;lkfds;lkfds;lkf;dslfkds;lfk;sdlfks;dlkfds;lfks;ldfks;ldkfs;ldkf;slkf;sldkf;slfkds;lfkd;lfkds;lfks;dlfks;dlfks;dlfks;dlkdfkfjkldsfjdsklfjsklfjasklfjaslkfjaskljsldkfds/lfsgja;lsjfew;lrk;lwekr;L,XCM.,M,.VM;WLRQKEPDKAS;LK;LZCKMZDMVS;ALKTIPOWEIF;LSKDCXM.X,MCX,.CMX.MX,.MXMV.CXMVX.CMV,XVM.,VMX.MV.,XCMVX,MVMV,MXV,CMVC,MV.CX,MV.X,CMV.X,CMVX.,MX.,MVCX,.MVX.C,MVX,.MV.X,CMV.CX,VMCX,.MV,.MVX.C,MVX,.VMX.C,VM.CX,MVCX,.MVX.C,MVX,.MVCX.,MVX.C,VM.XC,VMCX,.VMX.MSOERPOEWIRPOQIPres;fd;lfkds;lfkds;lkfds;lkfds;lkf;dslfkds;lfk;sdlfks;dlkfds;lfks;ldfks;ldkfs;ldkf;slkf;sldkf;slfkds;lfkd;lfkds;lfks;dlfks;dlfks;dlfks;dlkdsdkljasdjaskldjalkdjaskldjalkdjakldja', '2016-03-28 05:37:01', '0000-00-00 00:00:00'),
(23, 2, 'Lin Meng hooi', '860904-14-5174', 'menghooi.lim@gmail.com', '0102140518', 'No.456, jalan sultan, 53300 Kuala Lumpur ', 1, 43, 'The best uni', 2, 'Sure win', '190211baa787eb86cfe047dfbe28f241.jpg', NULL, 'Please choose me as winner', '2016-03-28 05:53:35', '0000-00-00 00:00:00'),
(24, 1, 'Muhammad Faiz', '891024-02-5710', 'muhdfaiz@mediacliq.my', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun Kedah', 1, 43, 'adasdasd', 1, 'Artwork 1', 'f711a6600fb90dbdc9158ff6d4bdfa63.jpg', NULL, 'asdasdasdasd', '2016-03-29 14:54:16', '0000-00-00 00:00:00'),
(25, 1, 'Muhammad Faiz', '891024-44-5715', 'muhdfaiz@mediacliq.my', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun', 1, 8, '', 2, 'Artwork 1', '3516a55c5d5ee4d55c48d1d6526bf82c.jpg', NULL, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. N', '2016-03-29 14:59:56', '0000-00-00 00:00:00'),
(26, 1, 'Muhammad Faiz', '891024-02-1256', 'muhdfaiz@mediacliq.my', '0174862127', 'Lorem ipsum dolor sit amet, noster expetendis interesset eos ei. In ipsum corrumpit reprimique sed, ', 1, 1, '', 2, 'Artwork 1', '7a3a40644e969e3b0ff55cca21d7589e.jpg', NULL, 'asdadsasd', '2016-03-30 14:08:26', '0000-00-00 00:00:00'),
(27, 1, 'Muhammad Faiz', '891024-02-5444', 'cupin06@gmail.com', '0104862127', 'Lorem ipsum dolor sit amet, noster expetendis interesset eos ei. In ipsum corrumpit reprimique sed, ', 1, 1, '', 2, 'Artwork 1', '903ad6a7744bc8226d413ada0a513c4c.jpg', NULL, 'asdadasdsad', '2016-03-30 14:17:07', '0000-00-00 00:00:00'),
(28, 1, 'Muhammad Faiz', '891024-02-7777', 'muhdfaiz@mediacliq.my', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun', 1, 18, '', 2, 'Asdasdasd', '99b1f3e963b2325fcda3831e68e61167.jpg', NULL, 'asdasd', '2016-03-30 14:19:39', '0000-00-00 00:00:00'),
(29, 1, 'Muhammad Faiz', '891024-02-5709', 'faizhalim@hotmail.com', '0104862127', 'sdaasdasdsad', 1, 1, '', 1, 'Muhammad Faiz', 'f82e8a877ae0a0e0109c5cc428954926.jpg', NULL, 'asdadasd', '2016-03-30 15:08:39', '0000-00-00 00:00:00'),
(30, 2, 'Muhammad Faiz', '891024-02-5722', 'cupin06@gmail.com', '0104862127', 'sadasdasd', 1, 30, '', 1, 'Muhammad Faiz', '1bf1ce635a7ab3ae6d4291103871e755.jpg', NULL, 'sadasdadsasd', '2016-03-30 15:14:10', '0000-00-00 00:00:00'),
(31, 1, 'Muhammad Faiz', '891024-02-1133', 'cupin06@gmail.com', '0104862127', 'No 9 Lorong 1 Taman Gurun Jaya 08300 Gurun Kedah', 1, 18, '', 1, 'Artwork 1', '5229a41e6e5d6669e8cd77c2f1cdfe7d.jpg', '4f8b11123b59f742ccfa5aaf904c24be.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br />\n<br />\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<br />\n<br />\n It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2016-04-04 00:06:21', '0000-00-00 00:00:00'),
(32, 1, 'Muhammad Faiz', '891024-02-7788', 'cupin06@gmail.com', '0104862127', 'asdadadads', 0, NULL, '', 1, 'Muhammad Faiz', '68ab0cf91cbc9628c37dcf41845a66d0.jpg', NULL, 'adasdasdasdasdasdasdasdasdadssadsad', '2016-04-04 15:15:00', '0000-00-00 00:00:00'),
(33, 1, 'Muhammad Faiz Bin Halim', '891024-02-5779', 'cupin06@gmail.com', '0104522876', 'asdsadsad', 0, NULL, '', 2, 'Artwork 1', '058ce3572dfe8534c2d3bd8868e03415.jpg', '53bfb7f8ac72c4abc7c0ee5abde432ac.jpg', 'sadsad asdsad', '2016-04-04 15:30:39', '0000-00-00 00:00:00'),
(34, 2, 'yuhanis', '820606-14-5778', 'cupin06@gmail.com', '0131234456', 'sadasdasd sad asd as d as d asd a sdasd', 1, 1, 'asdasdasd', 1, 'art work', '68dc30b89374aa3054ae6e656497d201.jpg', NULL, 'adsadasd', '2016-04-05 11:29:03', '0000-00-00 00:00:00'),
(35, 2, 'asddadsads', '891024-02-5255', 'cupin06@gmail.com', '0104862127', 'asdasdasd', 1, 43, 'sadasdasd', 1, 'Muhammad Faiz', '2881fefbb02cdc3a15408441360f98d0.jpg', '7758eaf6b93a5548e6a537d601535e76.jpg', 'asdasdasd', '2016-04-05 11:48:46', '0000-00-00 00:00:00'),
(36, 1, 'Muhammad Faiz', '891024-02-1100', 'cupin06@gmail.com', '0104862127', 'adsd', 0, NULL, '', 2, 'Muhammad Faiz', 'c5a6d90ae372b544c2b2f733aa400f09.jpg', NULL, 'sadasdasdsad', '2016-04-05 11:58:29', '0000-00-00 00:00:00'),
(37, 2, 'Muhammad Faiz', '891024-59-5715', 'cupin06@gmail.com', '0104862127', 'asdasdasd', 1, 1, '', 2, 'asdasdasd', 'eef3414265f9238243ea5ea0349edf07.jpg', '09fcc0b10ab56b64c458a6682c1dce15.jpg', 'asdasdasd', '2016-04-07 02:25:33', '0000-00-00 00:00:00'),
(38, 2, 'Muhammad Faiz', '891024-02-5334', 'cupin06@gmail.com', '0104862127', 'No. 2, Jalan USJ 3B/5, 47610, Subang Jaya, Selangor, Malaysia.', 1, 43, 'Fairview International School Wangsa Maju', 2, 'Asdasd', 'a735d71cddbb8f5eb727190e84ffc442.png', NULL, 'asdasdasdasdasdasd', '2016-04-20 14:37:54', '0000-00-00 00:00:00'),
(39, 2, 'Khaw Ann Li', '980307-10-5816', 'khawannli@gmail.com', '0102674012', 'No. 2, Jalan USJ 3B/5, 47610, Subang Jaya, Selangor, Malaysia.', 1, 1, 'Fairview International School Wangsa Maju', 1, 'Concern', '34c7bede8b2401a683cdf5f3cebe99cf.png', NULL, 'In Mandarin, an insult for being obese is directly translated into &ldquo;big, dumb elephant&rdquo;. My painting relates to the theme of identity, specifically women&rsquo;s emotional insecurity due to body image. Amongst teenagers, insults and incriminating jokes are common because both males and females, propagate stereotypical views about beauty onto themselves and others. The anxiety that follows from wanting to appear beautiful is, I think, an extremely exhausting pursuit. Inspired by painter Simon Birch and the Cubist art movement, I used their styles to depict my figure as faceted and crystalline, an emotionally fragile and anxious figure, self-conscious of her physical appearance. She is angrily clenching and pushing up her breasts, wanting to change her physical appearance. I wanted to depict this self-crimination and self-frustration to reach our conceptions of beauty, hence the use of sharp, jagged shapes. In efforts to raise awareness, I hope to empathise with audiences in this manner. ', '2016-04-20 14:42:41', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `artwork_categories`
--

CREATE TABLE IF NOT EXISTS `artwork_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `artwork_categories`
--

INSERT INTO `artwork_categories` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Fine Art', 'fine_art', '2016-03-07 11:28:14', '2016-03-07 11:28:14'),
(2, 'Digital Art', 'digital_art', '2016-03-07 11:28:21', '2016-03-07 11:28:21');

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE IF NOT EXISTS `colleges` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ALFA International College', '2016-03-07 14:00:58', '2016-03-21 13:39:37'),
(2, 'Aswara', '2016-03-07 14:01:04', '2016-03-21 13:39:43'),
(3, 'DASEIN Academy of Art', '2016-03-07 14:01:11', '2016-03-21 13:39:56'),
(4, 'EQUATOR Academy of Art', '2016-03-07 14:01:28', '2016-03-21 13:41:47'),
(5, 'IACT College', '2016-03-07 14:01:34', '2016-03-21 13:41:49'),
(6, 'IIUM (International Islamic University Malaysia)', '2016-03-07 14:01:48', '2016-03-21 13:41:50'),
(7, 'Inti International University and Colleges', '2016-03-07 14:01:55', '2016-03-21 13:41:52'),
(8, 'KBU International College', '2016-03-07 14:02:02', '2016-03-21 13:41:53'),
(9, 'KDU University College', '2016-03-07 14:02:10', '2016-03-21 13:41:54'),
(10, 'KLMU', '2016-03-07 14:02:16', '2016-03-21 13:41:56'),
(11, 'LASALLE College of the Arts', '2016-03-07 14:02:22', '2016-03-21 13:41:57'),
(12, 'Lim Kok Wing University', '2016-03-07 14:02:29', '2016-03-21 13:41:58'),
(13, 'MIA (Malaysian Institute of Art)', '2016-03-07 14:03:03', '2016-03-21 13:42:00'),
(14, 'Multimedia University', '2016-03-07 14:03:11', '2016-03-21 13:42:01'),
(15, 'New Era University College', '2016-03-07 14:03:21', '2016-03-21 13:42:03'),
(16, 'PJ College or Art and Design', '2016-03-07 14:03:27', '2016-03-21 13:42:04'),
(17, 'Saito College', '2016-03-07 14:03:34', '2016-03-21 13:42:05'),
(18, 'SEGi College Subang Jaya', '2016-03-07 14:03:40', '2016-03-21 13:42:07'),
(19, 'SEGi University Kota Damansara', '2016-03-07 14:03:46', '2016-03-21 13:42:08'),
(20, 'Sunway University', '2016-03-07 14:03:52', '2016-03-21 13:42:10'),
(21, 'TARUC (Tunku Abdul Rahman University College)', '2016-03-07 14:04:00', '2016-03-21 13:42:11'),
(22, 'Taylors College Subang Jaya', '2016-03-07 14:04:11', '2016-03-21 13:42:12'),
(23, 'Taylors University Lakeside Campus', '2016-03-07 14:04:17', '2016-03-21 13:42:14'),
(24, 'The One Academy', '2016-03-07 14:04:24', '2016-03-21 13:42:16'),
(25, 'UCSI University', '2016-03-07 14:04:33', '2016-03-21 13:42:18'),
(26, 'UiTM (Universiti Teknologi Mara)', '2016-03-07 14:04:42', '2016-03-21 13:42:20'),
(27, 'UiTM Lendu Alor Gajah', '2016-03-07 14:04:47', '2016-03-21 13:42:21'),
(28, 'UiTM Puncak Alam', '2016-03-07 14:04:53', '2016-03-21 13:42:23'),
(29, 'UiTM Puncak Perdana', '2016-03-07 14:05:00', '2016-03-21 13:42:24'),
(30, 'UiTM Seri Iskandar Perak', '2016-03-07 14:05:06', '2016-03-21 13:42:28'),
(31, 'UiTM Shah Alam', '2016-03-07 14:05:13', '2016-03-21 13:42:31'),
(32, 'Universiti Pendidikan Sultan Idris', '2016-03-07 14:05:22', '2016-03-21 13:42:33'),
(33, 'Universiti Putra Malaysia', '2016-03-07 14:05:32', '2016-03-21 13:42:34'),
(34, 'Universiti Sains Malaysia', '2016-03-07 14:05:38', '2016-03-21 13:42:36'),
(35, 'Universiti Teknologi Malaysia (UTM)', '2016-03-07 14:05:44', '2016-03-21 13:42:39'),
(36, 'Universiti Teknologi Malaysia Kuala Lumpur', '2016-03-07 14:05:50', '2016-03-21 13:42:40'),
(37, 'Universiti Teknologi Malaysia Skudai', '2016-03-07 14:06:06', '2016-03-21 13:42:42'),
(38, 'Universiti Teknologi Petronas', '2016-03-07 14:06:13', '2016-03-21 13:42:44'),
(39, 'University Malaysia Kelantan', '2016-03-07 14:06:19', '2016-03-21 13:42:46'),
(40, 'University Malaysia Kelantan Bachok', '2016-03-07 14:01:18', '2016-03-21 13:42:48'),
(41, 'UTAR (University Tunku Abdul Rahman)', '2016-03-07 14:06:30', '2016-03-21 13:42:50'),
(43, 'Other', '2016-03-22 23:44:42', '2016-03-22 23:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `finalists`
--

CREATE TABLE IF NOT EXISTS `finalists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artwork_id` int(10) unsigned NOT NULL,
  `artwork_category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `finalists`
--

INSERT INTO `finalists` (`id`, `artwork_id`, `artwork_category_id`, `user_id`, `created_at`, `updated_at`) VALUES
(59, 7, 2, 22, '2016-04-05 23:34:58', '0000-00-00 00:00:00'),
(61, 5, 2, 22, '2016-04-05 23:34:58', '0000-00-00 00:00:00'),
(62, 4, 2, 22, '2016-04-05 23:34:58', '0000-00-00 00:00:00'),
(65, 34, 1, 28, '2016-04-06 00:19:21', '0000-00-00 00:00:00'),
(67, 33, 2, 28, '2016-04-06 00:21:15', '0000-00-00 00:00:00'),
(68, 28, 2, 28, '2016-04-06 00:21:26', '0000-00-00 00:00:00'),
(69, 32, 1, 28, '2016-04-06 02:22:34', '0000-00-00 00:00:00'),
(70, 31, 1, 28, '2016-04-06 02:22:34', '0000-00-00 00:00:00'),
(73, 14, 1, 22, '2016-04-06 15:36:20', '0000-00-00 00:00:00'),
(74, 18, 1, 29, '2016-04-07 00:57:42', '0000-00-00 00:00:00'),
(75, 14, 1, 29, '2016-04-07 00:57:42', '0000-00-00 00:00:00'),
(76, 12, 1, 29, '2016-04-07 00:57:47', '0000-00-00 00:00:00'),
(77, 13, 2, 29, '2016-04-07 00:57:57', '0000-00-00 00:00:00'),
(78, 10, 2, 29, '2016-04-07 00:57:57', '0000-00-00 00:00:00'),
(79, 7, 2, 29, '2016-04-07 00:58:04', '0000-00-00 00:00:00'),
(85, 29, 1, 29, '2016-04-07 23:54:13', '0000-00-00 00:00:00'),
(87, 22, 1, 29, '2016-04-07 23:54:13', '0000-00-00 00:00:00'),
(88, 20, 1, 29, '2016-04-07 23:54:13', '0000-00-00 00:00:00'),
(89, 19, 1, 29, '2016-04-07 23:54:13', '0000-00-00 00:00:00'),
(90, 16, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(91, 11, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(92, 9, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(93, 8, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(94, 3, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(95, 2, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(96, 1, 1, 29, '2016-04-07 23:54:18', '0000-00-00 00:00:00'),
(109, 32, 1, 29, '2016-04-08 00:28:57', '0000-00-00 00:00:00'),
(111, 24, 1, 29, '2016-04-08 00:29:03', '0000-00-00 00:00:00'),
(112, 31, 1, 29, '2016-04-08 00:29:35', '0000-00-00 00:00:00'),
(113, 34, 1, 29, '2016-04-08 00:30:41', '0000-00-00 00:00:00'),
(114, 30, 1, 29, '2016-04-08 00:30:58', '0000-00-00 00:00:00'),
(115, 37, 2, 29, '2016-04-08 00:32:18', '0000-00-00 00:00:00'),
(116, 36, 2, 29, '2016-04-08 00:32:18', '0000-00-00 00:00:00'),
(117, 33, 2, 29, '2016-04-08 00:32:23', '0000-00-00 00:00:00'),
(118, 22, 1, 22, '2016-04-11 10:01:37', '0000-00-00 00:00:00'),
(125, 18, 1, 22, '2016-05-03 12:25:42', '0000-00-00 00:00:00'),
(127, 39, 1, 30, '2016-05-19 18:14:21', '0000-00-00 00:00:00'),
(128, 35, 1, 30, '2016-05-19 18:14:21', '0000-00-00 00:00:00'),
(129, 34, 1, 30, '2016-05-19 18:14:21', '0000-00-00 00:00:00'),
(130, 20, 1, 22, '2016-05-26 12:00:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(2) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'judge', 'List of judges than can vote photo');

-- --------------------------------------------------------

--
-- Table structure for table `groups_users`
--

CREATE TABLE IF NOT EXISTS `groups_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `groups_users`
--

INSERT INTO `groups_users` (`id`, `user_id`, `group_id`) VALUES
(20, 1, 1),
(24, 22, 2),
(30, 28, 2),
(31, 29, 2),
(32, 30, 2),
(33, 31, 2),
(34, 32, 2),
(39, 36, 2);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('f613d41f697ed4eaf1586bf5468ed626', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', 1464253171, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:5:"admin";s:8:"username";s:5:"admin";s:5:"email";s:15:"admin@admin.com";s:7:"user_id";s:1:"1";s:14:"old_last_login";s:10:"1464236070";}');

-- --------------------------------------------------------

--
-- Table structure for table `shortlists`
--

CREATE TABLE IF NOT EXISTS `shortlists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artwork_id` int(10) unsigned NOT NULL,
  `artwork_category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=198 ;

--
-- Dumping data for table `shortlists`
--

INSERT INTO `shortlists` (`id`, `artwork_id`, `artwork_category_id`, `user_id`, `created_at`, `updated_at`) VALUES
(110, 25, 2, 22, '2016-04-05 17:54:19', '0000-00-00 00:00:00'),
(111, 23, 2, 22, '2016-04-05 17:54:19', '0000-00-00 00:00:00'),
(116, 10, 2, 22, '2016-04-05 17:54:19', '0000-00-00 00:00:00'),
(117, 7, 2, 22, '2016-04-05 17:54:29', '0000-00-00 00:00:00'),
(118, 6, 2, 22, '2016-04-05 17:54:47', '0000-00-00 00:00:00'),
(120, 5, 2, 22, '2016-04-05 17:57:19', '0000-00-00 00:00:00'),
(121, 4, 2, 22, '2016-04-05 17:57:19', '0000-00-00 00:00:00'),
(128, 34, 1, 28, '2016-04-06 00:15:25', '0000-00-00 00:00:00'),
(129, 32, 1, 28, '2016-04-06 00:15:25', '0000-00-00 00:00:00'),
(130, 31, 1, 28, '2016-04-06 00:19:06', '0000-00-00 00:00:00'),
(131, 30, 1, 28, '2016-04-06 00:19:06', '0000-00-00 00:00:00'),
(132, 29, 1, 28, '2016-04-06 00:19:06', '0000-00-00 00:00:00'),
(133, 24, 1, 28, '2016-04-06 00:19:06', '0000-00-00 00:00:00'),
(134, 22, 1, 28, '2016-04-06 00:19:11', '0000-00-00 00:00:00'),
(136, 33, 2, 28, '2016-04-06 00:21:08', '0000-00-00 00:00:00'),
(137, 28, 2, 28, '2016-04-06 00:21:20', '0000-00-00 00:00:00'),
(139, 24, 1, 22, '2016-04-06 15:33:57', '0000-00-00 00:00:00'),
(141, 14, 1, 22, '2016-04-06 15:35:24', '0000-00-00 00:00:00'),
(143, 13, 2, 29, '2016-04-07 00:56:03', '0000-00-00 00:00:00'),
(144, 10, 2, 29, '2016-04-07 00:56:14', '0000-00-00 00:00:00'),
(145, 7, 2, 29, '2016-04-07 00:56:14', '0000-00-00 00:00:00'),
(146, 18, 1, 29, '2016-04-07 00:56:25', '0000-00-00 00:00:00'),
(147, 14, 1, 29, '2016-04-07 00:56:25', '0000-00-00 00:00:00'),
(148, 12, 1, 29, '2016-04-07 00:56:25', '0000-00-00 00:00:00'),
(149, 35, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(150, 34, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(151, 32, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(152, 31, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(153, 30, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(154, 29, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(155, 24, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(156, 22, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(157, 20, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(158, 19, 1, 29, '2016-04-07 23:54:02', '0000-00-00 00:00:00'),
(159, 16, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(160, 11, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(161, 9, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(162, 8, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(163, 3, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(164, 2, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(165, 1, 1, 29, '2016-04-07 23:54:07', '0000-00-00 00:00:00'),
(166, 37, 2, 29, '2016-04-08 00:32:02', '0000-00-00 00:00:00'),
(167, 36, 2, 29, '2016-04-08 00:32:02', '0000-00-00 00:00:00'),
(168, 33, 2, 29, '2016-04-08 00:32:02', '0000-00-00 00:00:00'),
(169, 28, 2, 29, '2016-04-08 00:32:02', '0000-00-00 00:00:00'),
(170, 22, 1, 22, '2016-04-11 10:01:25', '0000-00-00 00:00:00'),
(171, 20, 1, 22, '2016-04-11 10:01:25', '0000-00-00 00:00:00'),
(173, 18, 1, 22, '2016-04-11 10:01:25', '0000-00-00 00:00:00'),
(177, 9, 1, 22, '2016-04-11 10:01:25', '0000-00-00 00:00:00'),
(178, 8, 1, 22, '2016-04-11 10:01:26', '0000-00-00 00:00:00'),
(179, 3, 1, 22, '2016-04-11 10:01:26', '0000-00-00 00:00:00'),
(184, 11, 1, 22, '2016-05-03 16:50:32', '0000-00-00 00:00:00'),
(185, 2, 1, 22, '2016-05-03 16:50:32', '0000-00-00 00:00:00'),
(186, 1, 1, 22, '2016-05-03 16:50:32', '0000-00-00 00:00:00'),
(187, 38, 2, 22, '2016-05-04 09:43:29', '0000-00-00 00:00:00'),
(188, 37, 2, 22, '2016-05-04 09:43:29', '0000-00-00 00:00:00'),
(189, 36, 2, 22, '2016-05-04 09:43:29', '0000-00-00 00:00:00'),
(190, 39, 1, 30, '2016-05-19 18:10:18', '0000-00-00 00:00:00'),
(191, 35, 1, 30, '2016-05-19 18:10:18', '0000-00-00 00:00:00'),
(192, 34, 1, 30, '2016-05-19 18:10:18', '0000-00-00 00:00:00'),
(194, 31, 1, 30, '2016-05-19 18:10:18', '0000-00-00 00:00:00'),
(196, 37, 2, 30, '2016-05-19 18:15:23', '0000-00-00 00:00:00'),
(197, 39, 1, 22, '2016-05-26 12:01:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `company` varchar(20) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `email`, `first_name`, `last_name`, `phone`, `company`, `salt`, `password`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`) VALUES
(1, '127.0.0.1', 'admin', 'admin@admin.com', 'Muhammad', 'Faiz', NULL, NULL, '', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', NULL, NULL, NULL, 1268889823, 1464252454, 1),
(22, '127.0.0.1', 'muhdfaiz', '', 'Muhammad', 'Faiz', '11111111111', NULL, NULL, '$2y$08$tb0tvPa4Phv9gJIuFuU/d.by28tbZBeXa1onzWNKZ7n5KHDxGpnC.', NULL, NULL, NULL, NULL, 1459156334, 1464235104, 1),
(28, '127.0.0.1', 'paithalim', '', 'Faiz', 'Halim', '', NULL, NULL, '$2y$08$0w/fTpkIYgl/5tUtbYcXAeC2Q/bONm77vGVBMuvm7dI7ia/CsNTe2', NULL, NULL, NULL, NULL, 1459871056, 1459880541, 1),
(29, '127.0.0.1', 'cupin06', '', 'Muhammad', 'Firdaus', '0174862127', NULL, NULL, '$2y$08$eut5VcFT3ei.Wb4pXnpFu.9kbpNjT0NZ1RTyMtKU9pncpYkIlIOFe', NULL, NULL, NULL, NULL, 1459926265, 1460044338, 1),
(30, '127.0.0.1', 'nandosjudge4', '', 'judge', 'four', '', NULL, NULL, '$2y$08$AOVrvji9hNKgu6oHXvx9L.QJERMNPUVAJ.UZcDgrGZfnAarLNfESu', NULL, NULL, NULL, NULL, 1459997832, 1463652744, 1),
(31, '127.0.0.1', 'nandosjudge5', '', 'judge', 'five', '', NULL, NULL, '$2y$08$6TIndlRk4ySSIcDN9K2X3uu0lai0SASFF1MShjjKZ5fGrfbqWaEpa', NULL, NULL, NULL, NULL, 1459997857, NULL, 1),
(32, '127.0.0.1', 'nandosjudge6', '', 'judge', 'six', '0174862127', NULL, NULL, '$2y$08$XbjEMi9DeLgu8qN.VWb5n.xexEIKtyzono1bzhBz31sOyvQaKi2CC', NULL, NULL, NULL, NULL, 1459997884, NULL, 1),
(36, '127.0.0.1', 'vvvvv', '', 'Muhammad', 'Faiz', NULL, NULL, NULL, '$2y$08$jvtf8cVY0Z7Be5RGbT6CW.VCuyns7BCO711PXkHvKXtb5FItjTjpG', NULL, NULL, NULL, NULL, 1460086923, NULL, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups_users`
--
ALTER TABLE `groups_users`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
